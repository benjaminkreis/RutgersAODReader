#ifndef DisplacedAODReader_H
#define DisplacedAODReader_H

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseHandler.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseTreeReader.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseTreeWriter.h"
#include "RutgersAODReader/BaseAODReader/interface/BaseAODReader.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/EgammaCandidates/interface/Photon.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/PatCandidates/interface/PackedTriggerPrescales.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/MuonReco/interface/MuonPFIsolation.h"
#include "DataFormats/METReco/interface/HcalNoiseSummary.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include "DataFormats/JetReco/interface/CaloJet.h"
#include "MagneticField/Engine/interface/MagneticField.h"
#include "RecoVertex/KalmanVertexFit/interface/KalmanVertexFitter.h"
#include "RecoVertex/VertexTools/interface/VertexCompatibleWithBeam.h"
#include "RecoVertex/VertexTools/interface/VertexDistanceXY.h"
#include "TrackingTools/GeomPropagators/interface/Propagator.h"
#include "RecoVertex/ConfigurableVertexReco/interface/ConfigurableVertexReconstructor.h"
#include "DataFormats/GeometryVector/interface/GlobalPoint.h"
#include "TrackingTools/TransientTrack/interface/TransientTrack.h"
#include "PhysicsTools/RecoUtils/interface/CheckHitPattern.h"
#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "DataFormats/Candidate/interface/VertexCompositeCandidate.h"
#include "DataFormats/Candidate/interface/VertexCompositeCandidateFwd.h"
#include "DataFormats/RecoCandidate/interface/RecoChargedCandidate.h"
#include "DataFormats/V0Candidate/interface/V0Candidate.h"
#include "TrackingTools/IPTools/interface/IPTools.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"
#include "RecoVertex/KinematicFit/interface/KinematicParticleVertexFitter.h"
#include "RecoVertex/KinematicFit/interface/KinematicParticleFitter.h"
#include "RecoVertex/KinematicFitPrimitives/interface/KinematicParticle.h"
#include "RecoVertex/KinematicFitPrimitives/interface/KinematicParticleFactoryFromTransientTrack.h"
#include "RecoVertex/KinematicFitPrimitives/interface/RefCountedKinematicParticle.h"
#include "RecoTracker/DebugTools/interface/GetTrackTrajInfo.h"
#include <tuple>

class TClonesArray;

class DisplacedAODReader : public BaseAODReader {
public:
    DisplacedAODReader (const edm::ParameterSet&);
    virtual ~DisplacedAODReader();


protected:
    virtual void analyze(const edm::Event&, const edm::EventSetup&);
    virtual void makeProducts(const edm::Event&, const edm::EventSetup&);

    virtual void makeJets();
    virtual void makeCaloJets(const edm::EventSetup&);
    virtual void makeTracks(const edm::EventSetup&);
    virtual void makeVertices();
    virtual void makeBeamSpot();
    virtual void makeMET();
    virtual void makeHcalNoise();
    virtual void makeGenInfo();
    virtual void makeGenParticles();
    virtual void makeFilters();
    virtual void makePileupInfo();
    virtual void makeKshorts();

    virtual void setDetectorEtaPhi(SignatureObject*);

    void deltaVertex3D(GlobalPoint secVert, std::vector<reco::TransientTrack> tracks, double& dEta, double& dPhi, double& pt, double& m, double& energy);
    void deltaVertex2D(GlobalPoint secVert, std::vector<reco::TransientTrack> tracks, double& dPhi, double& pt, double& mediandPhi);
    std::pair<double,double> deltaVertexPt(GlobalPoint secVert, std::vector<reco::TransientTrack> tracks);
    double sumPTTransVertex(GlobalPoint secVert, std::vector<reco::TransientTrack> tracks);
    double deltaBeamspotPt(GlobalPoint secVert, std::vector<reco::TransientTrack> tracks);
    double sumPTTransBeamspot(GlobalPoint secVert, std::vector<reco::TransientTrack> tracks);
    void calculateAlphaMax(std::vector<reco::TransientTrack> tracks,std::vector<int>whichVertex, double& alphaMax, double& alphaMaxP, double& beta, double& alphaMax2, double& alphaMaxP2, double& beta2);
    std::vector<reco::TransientTrack> cleanTracks(std::vector<reco::TransientTrack>, GlobalPoint);    
    double trackAngle(reco::TransientTrack track,TrajectoryStateOnSurface tsosInnerHit);
    void trackEnergyAndMass(std::vector<TrajectoryStateOnSurface> tracks, double& energy, double& mass);
    double linearRadialMoment(reco::CaloJet calo_jet,std::vector<TrajectoryStateOnSurface> tsosList);

    std::tuple<double,double,double,double> getBoostedVariables(TLorentzVector, std::vector<TLorentzVector>);

    edm::Handle<HcalNoiseSummary> hcalHandle_;
    edm::EDGetTokenT<HcalNoiseSummary> hcalToken_;
    edm::InputTag hcalLabel_;

    edm::Handle<LHEEventProduct> genInfoHandle_;
    edm::EDGetTokenT<LHEEventProduct> genInfoToken_;
    edm::InputTag genInfoLabel_;

    edm::Handle<edm::View<reco::Vertex> > secondaryVertexHandle_;
    edm::EDGetTokenT<edm::View<reco::Vertex> > secondaryVertexToken_;
    edm::InputTag secondaryVertexLabel_;

    edm::Handle<edm::View<reco::CaloJet> > caloJetHandle_;
    edm::EDGetTokenT<edm::View<reco::CaloJet> > caloJetToken_;
    edm::InputTag caloJetLabel_;

    edm::Handle<bool> hIsoNoiseHandle_;
    edm::EDGetTokenT<bool> hIsoNoiseToken_;
    edm::InputTag hIsoNoiseLabel_;

    edm::Handle<bool> hNoiseHandle_;
    edm::EDGetTokenT<bool> hNoiseToken_;
    edm::InputTag hNoiseLabel_;

    edm::Handle<edm::View<PileupSummaryInfo> > pileupHandle_;
    edm::EDGetTokenT<edm::View<PileupSummaryInfo> > pileupToken_;
    edm::InputTag pileupLabel_;

    edm::Handle<reco::VertexCompositeCandidateCollection> ksHandle_;
    edm::EDGetTokenT<reco::VertexCompositeCandidateCollection> ksToken_;
    edm::InputTag ksLabel_;

    edm::Handle<double> rhoAllHandle_;
    edm::EDGetTokenT<double> rhoAllToken_;
    edm::InputTag rhoAllLabel_;

    edm::Handle<double> rhoNeutralHandle_;
    edm::EDGetTokenT<double> rhoNeutralToken_;
    edm::InputTag rhoNeutralLabel_;

    std::vector<int> trackToCaloJetMap_;
    std::vector<int> whichVertex_;
    double mPion_;

    bool processTracks_;

    ///edm::ESHandle<TransientTrackBuilder> trackBuilder_;
    const MagneticField* magneticField_;
    KalmanVertexFitter* vertexFitter_;

    VertexCompatibleWithBeam* vertexBeam_;
    VertexDistanceXY vertexDistanceXY_;

    ConfigurableVertexReconstructor* vtxfitter_;

    std::string thePropagatorName_;
    edm::ESHandle<Propagator> thePropagator_;

    CheckHitPattern checkHitPattern_;

    double cleanSignificanceCut_;
    double maxTrackToJetDeltaR_;
    double minTrackPtForDiTrack_;
    double minTrack3DSigForDiTrack_;

    double stupakR_;
    double stupakR2_;

    edm::ESHandle<TransientTrackBuilder> theB_;

};

#endif
