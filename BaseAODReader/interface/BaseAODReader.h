#ifndef BaseAODReader_H
#define BaseAODReader_H

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseHandler.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseTreeReader.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseTreeWriter.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureObjectFlat.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/EgammaCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "HLTrigger/HLTcore/interface/HLTPrescaleProvider.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "RecoEgamma/EgammaTools/interface/ConversionTools.h"
#include "DataFormats/EgammaCandidates/interface/Conversion.h"
#include "DataFormats/EgammaCandidates/interface/ConversionFwd.h"
#include "SimDataFormats/GeneratorProducts/interface/LHERunInfoProduct.h"
#include "JetMETCorrections/Modules/interface/JetResolution.h"
#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectionUncertainty.h"
#include "JetMETCorrections/Objects/interface/JetCorrectionsRecord.h"
#include "FWCore/Framework/interface/ESHandle.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "DataFormats/Common/interface/ValueMap.h"

#include "CondFormats/BTauObjects/interface/BTagCalibration.h"
#include "CondTools/BTau/interface/BTagCalibrationReader.h"
#include "FWCore/ParameterSet/interface/FileInPath.h"

class TClonesArray;
class TObjArray;

class BaseAODReader : public edm::EDAnalyzer,   public BaseTreeReader,   public BaseHandler, public BaseTreeWriter{
public:
    BaseAODReader (const edm::ParameterSet&);
    virtual ~BaseAODReader();

    using BaseHandler::setVariable;
    using BaseHandler::getVariable;
    using BaseHandler::analyzeEvent;

protected:
    virtual void beginJob() ;
    virtual void beginRun(edm::Run const &, edm::EventSetup const&);
    virtual void analyze(const edm::Event&, const edm::EventSetup&);
    virtual void endJob() ;
    void processParameters(const edm::VParameterSet);
    virtual void makeProducts();
    virtual void makeProducts(const edm::Event&, const edm::EventSetup&);
    virtual void finish();
    virtual void fillTree();
    virtual void initTree();

    virtual void processEventVariable(edm::ParameterSet);
    virtual void processObjectVariable(edm::ParameterSet);
    virtual void processObjectComparison(edm::ParameterSet);
    virtual void processSignatureTH(edm::ParameterSet);
    virtual void processProduct(edm::ParameterSet);
    virtual void processSignature(edm::ParameterSet);

    virtual void makeJets();
    virtual void makeElectrons();
    virtual void makePhotons();
    virtual void makeTaus();
    virtual void makeMuons();
    virtual void makeTracks();
    virtual void makeVertices();
    virtual void makeBeamSpot();
    virtual void makeTriggers(const edm::Event&, const edm::EventSetup&);
    virtual void makeGenParticles();
    virtual void makeMET();

    long event_;
    int run_;
    int lumi_;

    //TClonesArray* m_clonesarray;
    TObjArray* m_array;

    std::vector<std::string> m_variable_map_double_keys;
    std::vector<std::string> m_variable_map_long_keys;
    std::vector<std::string> m_variable_map_int_keys;
    std::vector<std::string> m_variable_map_TString_keys;
    std::vector<std::string> m_variable_map_bool_keys;

    std::vector<double> m_variable_map_double_values;
    std::vector<long> m_variable_map_long_values;
    std::vector<int> m_variable_map_int_values;
    std::vector<std::string> m_variable_map_TString_values;
    std::vector<bool> m_variable_map_bool_values;

    std::vector<std::string>* p_variable_map_double_keys;
    std::vector<std::string>* p_variable_map_long_keys;
    std::vector<std::string>* p_variable_map_int_keys;
    std::vector<std::string>* p_variable_map_TString_keys;
    std::vector<std::string>* p_variable_map_bool_keys;

    std::vector<double>* p_variable_map_double_values;
    std::vector<long>* p_variable_map_long_values;
    std::vector<int>* p_variable_map_int_values;
    std::vector<std::string>* p_variable_map_TString_values;
    std::vector<bool>* p_variable_map_bool_values;

    edm::Handle<edm::View<pat::Jet> > jetHandle_;
    edm::Handle<edm::View<pat::Electron> > electronHandle_;
    edm::Handle<edm::View<pat::Muon> > muonHandle_;
    edm::Handle<edm::View<pat::Photon> > photonHandle_;
    edm::Handle<edm::View<pat::Tau> > tauHandle_;
    edm::Handle<edm::View<pat::MET> > metHandle_;
    edm::Handle<edm::View<reco::Track> > trackHandle_;
    edm::Handle<edm::View<reco::Vertex> > vertexHandle_;
    edm::Handle<reco::BeamSpot> beamspotHandle_;
    edm::Handle<edm::TriggerResults> triggerHandle_;
    edm::Handle<reco::GenParticleCollection> genParticleHandle_;
    edm::Handle<trigger::TriggerEvent> triggerEventHandle_;

    edm::EDGetTokenT<edm::View<pat::Jet> > jetToken_;
    edm::EDGetTokenT<edm::View<pat::Electron> > electronToken_;
    edm::EDGetTokenT<edm::View<pat::Muon> > muonToken_;
    edm::EDGetTokenT<edm::View<pat::Photon> > photonToken_;
    edm::EDGetTokenT<edm::View<pat::Tau> > tauToken_;
    edm::EDGetTokenT<edm::View<pat::MET> > metToken_;
    edm::EDGetTokenT<edm::View<reco::Track> > trackToken_;
    edm::EDGetTokenT<edm::View<reco::Vertex> > vertexToken_;
    edm::EDGetTokenT<reco::BeamSpot> beamspotToken_;
    edm::EDGetTokenT<edm::TriggerResults> triggerToken_;
    edm::EDGetTokenT<reco::GenParticleCollection> genParticleToken_;
    edm::EDGetTokenT<trigger::TriggerEvent> triggerEventToken_;

    edm::InputTag jetLabel_;
    edm::InputTag electronLabel_;
    edm::InputTag muonLabel_;
    edm::InputTag tauLabel_;
    edm::InputTag photonLabel_;
    edm::InputTag metLabel_;
    edm::InputTag trackLabel_;
    edm::InputTag vertexLabel_;
    edm::InputTag triggerLabel_;
    edm::InputTag beamspotLabel_;
    edm::InputTag genParticleLabel_;
    edm::InputTag triggerEventLabel_;

    HLTConfigProvider hltConfig_;
    HLTPrescaleProvider hltPrescale_;
    bool writeAllEvents_;
    bool writeAllObjects_;
    std::string processName_;

    edm::InputTag LHERunInfoProductLabel_;
    edm::EDGetTokenT<LHERunInfoProduct> LHERunInfoProductToken_;
    edm::Handle<LHERunInfoProduct> LHERunInfoProductHandle_;
    
    bool getJECUncFromDB_;
    edm::FileInPath JECUncFileName_;
    edm::ESHandle<JetCorrectorParametersCollection> JetCorParColl;
    JetCorrectionUncertainty *jecUnc;
    JetCorrectorParameters* jetCorrParameters_;

    bool getJERSFfromGT_;
    edm::FileInPath JERSFfileName_;
    JME::JetResolutionScaleFactor res_sf;

    edm::Handle<edm::ValueMap<bool> > eleMediumIdMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleMediumIdMapToken_;
    edm::InputTag eleMediumIdMapLabel_;

    edm::Handle<edm::ValueMap<bool> > eleTightIdMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleTightIdMapToken_;
    edm::InputTag eleTightIdMapLabel_;

    edm::Handle<edm::ValueMap<float> > mvaValuesMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > mvaValuesMapToken_;
    edm::InputTag mvaValuesMapLabel_;

    edm::Handle<edm::ValueMap<int> > mvaCategoriesMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<int> > mvaCategoriesMapToken_;
    edm::InputTag mvaCategoriesMapLabel_;

    edm::Handle<edm::ValueMap<bool> > eleVetoIdMapCutBasedHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleVetoIdMapCutBasedToken_;
    edm::InputTag eleVetoIdMapCutBasedLabel_;

    edm::Handle<edm::ValueMap<bool> > eleLooseIdMapCutBasedHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleLooseIdMapCutBasedToken_;
    edm::InputTag eleLooseIdMapCutBasedLabel_;

    edm::Handle<edm::ValueMap<bool> > eleMediumIdMapCutBasedHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleMediumIdMapCutBasedToken_;
    edm::InputTag eleMediumIdMapCutBasedLabel_;

    edm::Handle<edm::ValueMap<bool> > eleTightIdMapCutBasedHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleTightIdMapCutBasedToken_;
    edm::InputTag eleTightIdMapCutBasedLabel_;

    edm::Handle<edm::ValueMap<bool> > eleHEEPIdMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleHEEPIdMapToken_;
    edm::InputTag eleHEEPIdMapLabel_;

    edm::Handle<edm::ValueMap<bool> > loose_id_decisions;
    edm::EDGetTokenT<edm::ValueMap<bool> > phoLooseIdMapToken_;
    edm::InputTag phoLooseIdLabel_;
    
    edm::Handle<edm::ValueMap<bool> > medium_id_decisions;
    edm::EDGetTokenT<edm::ValueMap<bool> > phoMediumIdMapToken_;
    edm::InputTag phoMediumIdLabel_;
    
    edm::Handle<edm::ValueMap<bool> > tight_id_decisions;
    edm::EDGetTokenT<edm::ValueMap<bool> > phoTightIdMapToken_;
    edm::InputTag phoTightIdLabel_;
    
    edm::Handle<edm::ValueMap<float> > phoChargedIsolationHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > phoChargedIsolationMapToken_;
    edm::InputTag phoChargedIsolationLabel_;
    
    edm::Handle<edm::ValueMap<float> > phoNeutralHadronIsolationHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > phoNeutralHadronIsolationMapToken_;
    edm::InputTag phoNeutralHadronIsolationLabel_;
    
    edm::Handle<edm::ValueMap<float> > phoPhotonIsolationHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > phoPhotonIsolationMapToken_;
    edm::InputTag phoPhotonIsolationLabel_;
    
    edm::Handle<edm::ValueMap<float> > phoWorstChargedIsolationHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > phoWorstChargedIsolationMapToken_;
    edm::InputTag phoWorstChargedIsolationLabel_;


    bool printLHERunInfo_;
};

namespace LHAPDF {
  void   initPDFSet(int nset, const std::string& filename, int member=0);
  int    numberPDF(int nset);
  void   usePDFMember(int nset, int member);
  double xfx(int nset, double x, double Q, int fl);
  double getXmin(int nset, int member);
  double getXmax(int nset, int member);
  double getQ2min(int nset, int member);
  double getQ2max(int nset, int member);
  void   extrapolate(bool extrapolate=true);
}

#endif
