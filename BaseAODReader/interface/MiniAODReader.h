#ifndef MiniAODReader_H
#define MiniAODReader_H

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseHandler.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseTreeReader.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseTreeWriter.h"
#include "RutgersAODReader/BaseAODReader/interface/BaseAODReader.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/EgammaCandidates/interface/Photon.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/PatCandidates/interface/PackedTriggerPrescales.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/MuonReco/interface/MuonPFIsolation.h"
#include "DataFormats/METReco/interface/HcalNoiseSummary.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "EgammaAnalysis/ElectronTools/interface/EGammaMvaEleEstimatorCSA14.h"

using namespace std;

class TClonesArray;

class MiniAODReader : public BaseAODReader {
public:
    MiniAODReader (const edm::ParameterSet&);
    virtual ~MiniAODReader();
    typedef math::XYZTLorentzVector LorentzVector;

protected:
    virtual void analyze(const edm::Event&, const edm::EventSetup&);
    virtual void makeProducts(const edm::Event&, const edm::EventSetup&);

    virtual void makeJets();
    virtual void makeElectrons();
    virtual void makePhotons();
    virtual void makeTaus();
    virtual void makeMuons();
    virtual void makeTracks();
    virtual void makePFCands();
    virtual void makeVertices();
    virtual void makeBeamSpot();
    virtual void makeTriggers(const edm::Event&, const edm::EventSetup&);
    virtual void makeFilters(const edm::Event&, const edm::EventSetup&);
    virtual void makeGenParticles();
    virtual void makeMET(TString inputTag = "");
    virtual void makeGenInfo();
    virtual void makeGenEventInfoProduct();
    virtual void makePDFsInternal();
    virtual void makePDFs();
    virtual void makePileupInfo();

    virtual void setIsolationVariablesForPackedCandidates(SignatureObject*,pat::PackedCandidate);

    virtual void setMiniIsolation(const reco::Candidate& pc,SignatureObject* obj, double ptThresh = 0.5, double vetoConeNeutralHadron = 0.01,double vetoConePhoton = 0.01, double vetoConeChargedHadron = 0.0001, double vetoConePU = 0.01);
    virtual void setPtRelAndRatio(const reco::Candidate& pc,SignatureObject* obj);

    virtual double linearRadialMoment(pat::Jet jet);

    virtual void makeSigObjTrack(SignatureObject*, pat::PackedCandidate);

    virtual void initializeBtagSFs();
    virtual void fillBtagSFs( SignatureObject* objjet, const pat::Jet& patjet, string btagger="");

    virtual bool ElectronCutBasedIDNoIsolation( const pat::Electron& ele, const reco::Vertex& PV, int IDno);

    edm::Handle<edm::View<pat::Photon> > patPhotonHandle_;
    edm::EDGetTokenT<edm::View<pat::Photon> > patPhotonToken_;
    edm::InputTag patPhotonLabel_;
    
    edm::Handle<edm::ValueMap<bool> > loose_id_decisions;
    edm::EDGetTokenT<edm::ValueMap<bool> > phoLooseIdMapToken_;
    edm::InputTag phoLooseIdLabel_;
    
    edm::Handle<edm::ValueMap<bool> > medium_id_decisions;
    edm::EDGetTokenT<edm::ValueMap<bool> > phoMediumIdMapToken_;
    edm::InputTag phoMediumIdLabel_;
    
    edm::Handle<edm::ValueMap<bool> > tight_id_decisions;
    edm::EDGetTokenT<edm::ValueMap<bool> > phoTightIdMapToken_;
    edm::InputTag phoTightIdLabel_;
    
    edm::Handle<edm::ValueMap<float> > phoChargedIsolationHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > phoChargedIsolationMapToken_;
    edm::InputTag phoChargedIsolationLabel_;
    
    edm::Handle<edm::ValueMap<float> > phoNeutralHadronIsolationHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > phoNeutralHadronIsolationMapToken_;
    edm::InputTag phoNeutralHadronIsolationLabel_;
    
    edm::Handle<edm::ValueMap<float> > phoPhotonIsolationHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > phoPhotonIsolationMapToken_;
    edm::InputTag phoPhotonIsolationLabel_;
    
    edm::Handle<edm::ValueMap<float> > phoWorstChargedIsolationHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > phoWorstChargedIsolationMapToken_;
    edm::InputTag phoWorstChargedIsolationLabel_;

    edm::Handle<pat::PackedCandidateCollection> pfCandidateHandle_;
    edm::EDGetTokenT<pat::PackedCandidateCollection> pfCandidateToken_;
    edm::InputTag pfCandidateLabel_;
    double pfCandMinPt_;

    edm::Handle<pat::PackedCandidateCollection> lostTrackHandle_;
    edm::EDGetTokenT<pat::PackedCandidateCollection> lostTrackToken_;
    edm::InputTag lostTrackLabel_;

    edm::Handle<pat::PackedTriggerPrescales> triggerPrescalesHandle_;
    edm::EDGetTokenT<pat::PackedTriggerPrescales> triggerPrescalesToken_;
    edm::InputTag triggerPrescalesLabel_;

    edm::Handle<edm::TriggerResults> filterHandle_;
    edm::EDGetTokenT<edm::TriggerResults> filterToken_;
    edm::InputTag filterLabel_;
    edm::EDGetTokenT<edm::TriggerResults> filterToken2_;
    edm::InputTag filterLabel2_;

    edm::Handle<pat::TriggerObjectStandAloneCollection> triggerObjectsHandle_;
    edm::EDGetTokenT<pat::TriggerObjectStandAloneCollection> triggerObjectsToken_;
    edm::InputTag triggerObjectsLabel_;
    vector<string> triggerObjectHLTnames;
    vector<string> triggerObjectFilterLabels;

    edm::Handle<edm::View<pat::PackedGenParticle> > packedGenHandle_;
    edm::EDGetTokenT<edm::View<pat::PackedGenParticle> > packedGenToken_;
    edm::InputTag packedGenLabel_;

    edm::Handle<HcalNoiseSummary> hcalHandle_;
    edm::EDGetTokenT<HcalNoiseSummary> hcalToken_;
    edm::InputTag hcalLabel_;
    edm::InputTag HBHENoiseFilterResult_;
    edm::InputTag HBHEIsoNoiseFilterResult_;
    edm::EDGetTokenT<bool> HBHENoiseFilterResultToken_;
    edm::EDGetTokenT<bool> HBHEIsoNoiseFilterResultToken_;

    edm::Handle<LHEEventProduct> genInfoHandle_;
    edm::EDGetTokenT<LHEEventProduct> genInfoToken_;
    edm::InputTag genInfoLabel_;

    // In BaseAODReader.h
    //edm::Handle<edm::View<pat::MET> > metHandle_;
    //edm::EDGetTokenT<edm::View<pat::MET>> metToken_;
    //edm::InputTag metLabel_;
    std::string metName_;
    edm::EDGetTokenT<edm::View<pat::MET>> metToken2_;
    edm::InputTag metLabel2_;
    std::string metName2_;

    edm::Handle<GenEventInfoProduct> genEventInfoProductHandle_;
    edm::EDGetTokenT<GenEventInfoProduct> genEventInfoProductToken_;
    edm::InputTag genEventInfoProductLabel_;

    edm::Handle<double> rhoAllHandle_;
    edm::EDGetTokenT<double> rhoAllToken_;
    edm::InputTag rhoAllLabel_;

    edm::Handle<double> rhoNeutralHandle_;
    edm::EDGetTokenT<double> rhoNeutralToken_;
    edm::InputTag rhoNeutralLabel_;

    edm::Handle<std::vector<PileupSummaryInfo> > pileupHandle_;
    edm::EDGetTokenT<std::vector<PileupSummaryInfo> > pileupToken_;
    edm::InputTag pileupLabel_;

    edm::Handle<edm::ValueMap<bool> > eleMediumIdMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleMediumIdMapToken_;
    edm::InputTag eleMediumIdMapLabel_;

    edm::Handle<edm::ValueMap<bool> > eleTightIdMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleTightIdMapToken_;
    edm::InputTag eleTightIdMapLabel_;

    edm::Handle<edm::ValueMap<float> > mvaValuesMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<float> > mvaValuesMapToken_;
    edm::InputTag mvaValuesMapLabel_;

    edm::Handle<edm::ValueMap<int> > mvaCategoriesMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<int> > mvaCategoriesMapToken_;
    edm::InputTag mvaCategoriesMapLabel_;

    edm::Handle<edm::ValueMap<bool> > eleVetoIdMapCutBasedHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleVetoIdMapCutBasedToken_;
    edm::InputTag eleVetoIdMapCutBasedLabel_;

    edm::Handle<edm::ValueMap<bool> > eleLooseIdMapCutBasedHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleLooseIdMapCutBasedToken_;
    edm::InputTag eleLooseIdMapCutBasedLabel_;

    edm::Handle<edm::ValueMap<bool> > eleMediumIdMapCutBasedHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleMediumIdMapCutBasedToken_;
    edm::InputTag eleMediumIdMapCutBasedLabel_;

    edm::Handle<edm::ValueMap<bool> > eleTightIdMapCutBasedHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleTightIdMapCutBasedToken_;
    edm::InputTag eleTightIdMapCutBasedLabel_;

    edm::Handle<edm::ValueMap<bool> > eleHEEPIdMapHandle_;
    edm::EDGetTokenT<edm::ValueMap<bool> > eleHEEPIdMapToken_;
    edm::InputTag eleHEEPIdMapLabel_;

    //EGammaMvaEleEstimatorCSA14* electronMVA_;
    double jetR_;

    vector<string> PDFSetNames;
    vector<vector<double> > PDFSetWeights;

    // Btag SFs: Supported Algorithms and Operating Points
    //   https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation76X
    BTagCalibration CSVv2calib;
    BTagCalibration cMVAv2calib;
    BTagCalibration JPcalib;
    BTagCalibrationReader CSVv2_LIGHT;
    BTagCalibrationReader CSVv2_BC;
    BTagCalibrationReader cMVAv2_LIGHT;
    BTagCalibrationReader cMVAv2_BC;
    BTagCalibrationReader JP_LIGHT;
    BTagCalibrationReader JP_BC;
    BTagEntry::OperatingPoint opl;
    BTagEntry::OperatingPoint opm;
    BTagEntry::OperatingPoint opt;
    vector<BTagEntry::OperatingPoint> opV;
    vector<string> sysTypeV;
    vector<BTagCalibrationReader> CSVv2;
    vector<BTagCalibrationReader> cMVAv2;
    vector<BTagCalibrationReader> JP;

    bool isData_;
};

#endif
