from CRABClient.UserUtilities import config
from CRABClient.UserUtilities import getUsernameFromSiteDB

# Select dataset to crab over
number = 0

# List of datasets
datasetnames = [
'/DoubleMuon/Run2015D-16Dec2015-v1/MINIAOD', #0
'/DoubleEG/Run2015D-16Dec2015-v1/MINIAOD', #1
'/MuonEG/Run2015D-16Dec2015-v1/MINIAOD', #2
'/SingleMuon/Run2015D-16Dec2015-v1/MINIAOD', #3
'/SingleElectron/Run2015D-16Dec2015-v1/MINIAOD' #4
]

# Please make sure these settings make sense:
# Storage path for output files
storagepath = '/store/user/'+getUsernameFromSiteDB()+'/2016/Data'

# cmsRun file
psetname = 'python/runMini_cfg.py'

# Output filename
OutputFilename = 'results.root'

# Storage site of output files
storageSite = 'T3_US_Rutgers'
#storageSite = 'T2_IN_TIFR'
#storageSite = 'T2_CH_CERN'

# White list sites
whiteList = ['']

# Black list sites
blackList = ['']


########## No modifications below this line are necessary ##########

dataset = filter(None, datasetnames[number].split('/'))

config = config()

import datetime
timestamp = datetime.datetime.now().strftime("_%Y%m%d_%H%M%S")

config.General.workArea = dataset[0]+timestamp
config.General.requestName = dataset[1]
config.General.transferOutputs = True
config.General.transferLogs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = psetname 
config.JobType.outputFiles = [OutputFilename]
config.JobType.pyCfgParams = ['outputFile='+OutputFilename,'isData=1']

config.Data.inputDataset = datasetnames[number]
config.Data.inputDBS = 'global'
config.Data.splitting = 'LumiBased'
config.Data.lumiMask = './Cert_13TeV_16Dec2015ReReco_Collisions15_25ns_JSON_v2.txt'
config.Data.unitsPerJob = 10
config.Data.ignoreLocality = True
config.Data.outLFNDirBase = storagepath
config.Data.publication = False
config.Data.outputDatasetTag = dataset[1]+'_'+dataset[2]

config.Site.storageSite = storageSite

if hasattr(config.Data, 'runRange'):
  config.General.requestName += ':' + config.Data.runRange

if not whiteList:
  config.Site.whitelist = whiteList

if not blackList:
  config.Site.blacklist = blackList
