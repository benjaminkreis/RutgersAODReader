from CRABClient.UserUtilities import config
from CRABClient.UserUtilities import getUsernameFromSiteDB

# Select dataset to crab over
number = 0

# List of datasets
datasetnames = [
'/WZJets_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8/RunIIFall15MiniAODv2-PU25nsData2015v1_76X_mcRun2_asymptotic_v12-v1/MINIAODSIM', #0
'/WZTo3LNu_TuneCUETP8M1_13TeV-powheg-pythia8/RunIIFall15MiniAODv2-PU25nsData2015v1_76X_mcRun2_asymptotic_v12-v1/MINIAODSIM', #1
'/ZZTo4L_13TeV-amcatnloFXFX-pythia8/RunIIFall15MiniAODv2-PU25nsData2015v1_76X_mcRun2_asymptotic_v12-v1/MINIAODSIM', #2
'/ZZTo4L_13TeV_powheg_pythia8/RunIIFall15MiniAODv2-PU25nsData2015v1_76X_mcRun2_asymptotic_v12-v1/MINIAODSIM', #3
'/TTJets_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8/RunIIFall15MiniAODv2-PU25nsData2015v1_76X_mcRun2_asymptotic_v12-v1/MINIAODSIM', #4
'/TTTo2L2Nu_13TeV-powheg/RunIIFall15MiniAODv2-PU25nsData2015v1_76X_mcRun2_asymptotic_v12-v1/MINIAODSIM', #5
'/DYJetsToLL_M-10to50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8/RunIIFall15MiniAODv2-PU25nsData2015v1_76X_mcRun2_asymptotic_v12-v1/MINIAODSIM', #6
'/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8/RunIIFall15MiniAODv2-PU25nsData2015v1_76X_mcRun2_asymptotic_v12-v1/MINIAODSIM' #7
]

# Please make sure these settings make sense:
# Storage path for output files
storagepath = '/store/user/'+getUsernameFromSiteDB()+'/2016/MC'

# cmsRun file
psetname = 'python/runMini_cfg.py'

# Output filename
OutputFilename = 'results.root'

# Storage site of output files
storageSite = 'T3_US_Rutgers'
#storageSite = 'T2_IN_TIFR'
#storageSite = 'T2_CH_CERN'


# White list sites
whiteList = ['']

# Black list sites
blackList = ['']


########## No modifications below this line are necessary ##########

dataset = filter(None, datasetnames[number].split('/'))

config = config()

import datetime
timestamp = datetime.datetime.now().strftime("_%Y%m%d_%H%M%S")

config.General.workArea = dataset[0]+timestamp
config.General.requestName = dataset[1]
config.General.transferOutputs = True
config.General.transferLogs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = psetname 
config.JobType.outputFiles = [OutputFilename]
config.JobType.pyCfgParams = ['outputFile='+OutputFilename,'isData=0']

config.Data.inputDataset = datasetnames[number]
config.Data.inputDBS = 'global'
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 1
config.Data.ignoreLocality = True
config.Data.outLFNDirBase = storagepath
config.Data.publication = False
config.Data.outputDatasetTag = dataset[1]+'_'+dataset[2]

config.Site.storageSite = storageSite

if not whiteList:
  config.Site.whitelist = whiteList

if not blackList:
  config.Site.blacklist = blackList

# For invalid ZZ sample
#config.Data.allowNonValidInputDataset = True
