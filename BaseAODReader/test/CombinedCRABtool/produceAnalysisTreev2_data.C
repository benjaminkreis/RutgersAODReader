#include "RutgersIAF/EventAnalyzer/interface/AnalysisTreeWriter.h"
#include "RutgersIAF/EventAnalyzer/interface/BaseHandler.h"
#include "RutgersIAF/EventAnalyzer/interface/FlatTreeReader.h"

#include "helperMiniAODv2.C"

void produceAnalysisTreev2_data(
	const char* ifname = "CondorInputSamples/DoubleMuon_Run2016B-PromptReco-v2_MINIAOD/results_1.root",
	const char* ofname = "/tmp/hsaka/results_EAtest.root",
	const char* json = "json/Cert_271036-274421_13TeV_PromptReco_Collisions16_JSON.txt",
	int mode = 0
) {

	// Check if the declated variables isMC and trig-mode make sense:
	assert(true);
	
	TChain* tree = new TChain("tree");
	TString input = ifname;
	bool manual = input.EndsWith(".root");
	if(!manual) {
		input += "/*.root";
	}
	tree->Add(input);
	
	FlatTreeReader* reader = new FlatTreeReader(tree);
	
	BaseHandler* handler = new BaseHandler(ofname,reader);
	
	//handler->setMode("theoryMinituples");
	bool theory = handler->getMode("theoryMinituples"); // the mode setting is also used inside the helper

	BaseTreeWriter* writer;
	if(theory) {
		writer = new TheoryTreeWriter(handler, "tree");
	} else {
		writer = new AnalysisTreeWriter(handler);
	}
	handler->setWriter(writer);

	//JSON filter
	handler->readGoodRunLumiFromJSON(TString(json));
	
	if(!(mode == 4 || mode == 5) && !theory) {
	  cout << "Setting fake modes ..." << endl;
	  handler->setWriter(new AnalysisTreeWriter(handler, "treeRfakeTracks") , "trackFakeCombination");
	  handler->setWriter(new AnalysisTreeWriter(handler, "treeRfakePhotons"), "photonFakeCombination");
	  //handler->setWriter(new AnalysisTreeWriter(handler, "treeRfakeTaus")   , "tauFakeCombination");//proxy method with taus is not ready yet
	}

	setupProducts(handler);
	setupVariables(handler, false);
	setupTriggers(handler, mode);
	handler->initSignatures();
	handler->eventLoop();
	//handler->eventLoop(1, 366232);
	handler->finishSignatures();
	
	cout<<"Done, exiting ...."<<endl;
}
