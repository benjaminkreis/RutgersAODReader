inputfile="results.root"
outputfile="histo.root"
json="Cert_271036-276811_13TeV_PromptReco_Collisions16_JSON.txt"
mode="3"
for i in "$@"
do
    case $i in
	inputFile=*)
	inputfile="${i#*=}"
	;;
	outputFile=*)
	outputfile="${i#*=}"
	;;
	jsonFile=*)
	json="${i#*=}"
	;;
	triggerMode=*)
	mode="${i#*=}"
    esac
done
pwd
echo "================= CMSRUN starting ===================="
cmsRun -j FrameworkJobReport.xml -p PSet.py
echo "================= CMSRUN finished ===================="
echo "================= EventAnalyzer Starting ===================="
root -q -b -l 'produceAnalysisTreev2_data.C("'"$inputfile"'","'"$outputfile"'","'"$json"'",'"$mode"')'
echo "================= EventAnalyzer Finished ===================="
