inputfile="results.root"
outputfile="histo.root"
for i in "$@"
do
    case $i in
	inputFile=*)
	inputfile="${i#*=}"
	;;
	outputFile=*)
	outputfile="${i#*=}"
    esac
done
pwd
echo "================= CMSRUN starting ===================="
cmsRun -j FrameworkJobReport.xml -p PSet.py
echo "================= CMSRUN finished ===================="
echo "================= EventAnalyzer Starting ===================="
root -q -b -l 'produceAnalysisTreev2_MC.C("'"$inputfile"'","'"$outputfile"'")'
echo "================= EventAnalyzer Finished ===================="
