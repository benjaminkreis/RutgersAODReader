from CRABClient.UserUtilities import config
from CRABClient.UserUtilities import getUsernameFromSiteDB

# Select dataset to crab over
number = 0

# List of datasets
datasetnames = [
'/WZTo3LNu_TuneCUETP8M1_13TeV-powheg-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM', #0
]

# Please make sure these settings make sense:
# Storage path for output files
storagepath = '/store/user/'+getUsernameFromSiteDB()+'/MC16_ML'

# cmsRun file
psetname = 'runMini_cfg.py'

# Output filename
ntupleOutputFilename = 'results.root'
histoOutputFilename = 'histo.root'

# Storage site of output files
storageSite = 'T3_US_Rutgers'
#storageSite = 'T2_IN_TIFR'
#storageSite = 'T2_CH_CERN'


# White list sites
whiteList = ['']

# Black list sites
blackList = ['']


########## No modifications below this line are necessary ##########

dataset = filter(None, datasetnames[number].split('/'))

config = config()

import datetime
timestamp = datetime.datetime.now().strftime("_%Y%m%d_%H%M%S")

config.General.workArea = dataset[0]+timestamp
config.General.requestName = dataset[1]
config.General.transferOutputs = True
config.General.transferLogs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = psetname 
config.JobType.maxMemoryMB = 3500
config.JobType.maxJobRuntimeMin = 2750
config.JobType.outputFiles = [histoOutputFilename]
config.JobType.inputFiles = ['produceAnalysisTreev2_MC.C','helperMiniAODv2.C',"helperMiniAOD_SetupObjectVariables.C","helperMiniAOD_SetupProductsCommon.C","helperMiniAOD_SetupProductsMatrix.C","helperMiniAOD_AddEventVariablesMatrix.C"]
config.JobType.scriptExe = 'crabScriptMC.sh'
config.JobType.pyCfgParams = ['outputFile='+ntupleOutputFilename,'isData=0']
config.JobType.scriptArgs = ['inputFile='+ntupleOutputFilename,'outputFile='+histoOutputFilename]

config.Data.inputDataset = datasetnames[number]
config.Data.inputDBS = 'global'
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 1
config.Data.totalUnits = 1
config.Data.ignoreLocality = True
config.Data.outLFNDirBase = storagepath
config.Data.publication = False
config.Data.outputDatasetTag = dataset[1]+'_'+dataset[2]

config.Site.storageSite = storageSite

if not whiteList:
  config.Site.whitelist = whiteList

if not blackList:
  config.Site.blacklist = blackList

# For invalid ZZ sample
#config.Data.allowNonValidInputDataset = True
