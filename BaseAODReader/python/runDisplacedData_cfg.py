import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing
from Configuration.StandardSequences.Eras import eras
process = cms.Process('RutgersAOD',eras.Run2_25ns)#for 25ns 13 TeV data
#process = cms.Process("RutgersAOD")
options = VarParsing.VarParsing ('analysis')

#set default arguments
options.inputFiles='root://cmsxrootd.fnal.gov//store/data/Run2015D/JetHT/AOD/16Dec2015-v1/00000/0A2C6696-AEAF-E511-8551-0026189438EB.root'
options.outputFile='test.root'
#options.inputFiles= '/store/relval/CMSSW_7_0_0/RelValProdTTbar_13/AODSIM/POSTLS170_V3-v2/00000/40D11F5C-EA98-E311-BE17-02163E00E964.root'
#options.inputFiles= 'file:/cms/thomassen/2012/Signal/StopRPV/store/aodsim/LLE122/StopRPV_8TeV_chunk3_stop950_bino800_LLE122_aodsim.root'
#options.maxEvents = 100 # -1 means all even
options.maxEvents = -1

# get and parse the command line arguments
options.parseArguments()

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(options.maxEvents) )

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(options.inputFiles),
)

## Geometry and Detector Conditions (needed for a few patTuple production steps)
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff')
from Configuration.AlCa.GlobalTag_condDBv2 import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '76X_dataRun2_v18')
process.load('Configuration.StandardSequences.MagneticField_38T_PostLS1_cff')

process.load("TrackingTools.MaterialEffects.MaterialPropagator_cfi")
process.load('PhysicsTools.PatAlgos.patSequences_cff')

#process.load("RutgersAODReader.BaseAODReader.displacedAOD_cfi")
#process.load("DisplacedDijet.DisplacedJetAnlzr.DJ_DiJetVertices_cfi")
process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(False),
)
process.options.allowUnscheduled = cms.untracked.bool( True )

from RutgersAODReader.BaseAODReader.displacedAOD_cfi import displacedAOD

process.displacedAOD = displacedAOD.clone()

process.displacedAOD.outFilename=options.outputFile
process.displacedAOD.JECUncFileName = cms.FileInPath("RutgersAODReader/BaseAODReader/data/Fall15_25nsV2_DATA_Uncertainty_AK4PFchs.txt")
process.displacedAOD.JERSFfileName = cms.FileInPath("RutgersAODReader/BaseAODReader/data/Summer15_25nsV6_MC_SF_AK4PFchs.txt")
process.displacedAOD.setupList = cms.VPSet(
    cms.PSet(type=cms.untracked.string("ObjectVariableMethod"),
             name=cms.string("PT"),
             methodName=cms.string("Pt"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValueInList"),
             name=cms.string("SaveAllOfType"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             values=cms.vstring("photon","vertex","beamspot","mc","met","electron","muon","trigger","filter","hcalnoise","kshort"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableInRange"),
             name=cms.string("PT5"),
             variableName=cms.string("PT"),
             variableType=cms.string("double"),
             low=cms.double(5),
             high=cms.double(1000000),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableInRange"),
             name=cms.string("PT10"),
             variableName=cms.string("PT"),
             variableType=cms.string("double"),
             low=cms.double(10),
             high=cms.double(1000000),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableInRange"),
             name=cms.string("PT15"),
             variableName=cms.string("PT"),
             variableType=cms.string("double"),
             low=cms.double(15),
             high=cms.double(1000000),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValueInList"),
             name=cms.string("isTrack"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             values=cms.vstring("losttrack","pftrack"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveTrack"),
             cutList=cms.vstring("isTrack","PT5"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValue"),
             name=cms.string("isMuon"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             value=cms.string("muon"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValue"),
             name=cms.string("isElectron"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             value=cms.string("electron"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValueInList"),
             name=cms.string("isJet"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             values=cms.vstring("jet","calojet"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValue"),
             name=cms.string("isTau"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             value=cms.string("tau"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveJet"),
             cutList=cms.vstring("isJet","PT15"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveTau"),
             cutList=cms.vstring("isTau","PT15"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveMuon"),
             cutList=cms.vstring("isMuon","PT5"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveElectron"),
             cutList=cms.vstring("isElectron","PT5"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("WRITEOBJECT"),
             cutList=cms.vstring("SaveJet","SaveTrack","SaveAllOfType","SaveMuon","SaveElectron","SaveTau"),
             doAnd=cms.bool(False),
             ),
    cms.PSet(type=cms.untracked.string("Signature"),
             name=cms.string("testSignature"),
             cutList=cms.vstring(),
             ),
)
process.load('CommonTools.RecoAlgos.HBHENoiseFilterResultProducer_cfi')

# for data:
from PhysicsTools.PatAlgos.tools.coreTools import runOnData
runOnData( process ,outputModules = [])

process.p = cms.Path(
    process.HBHENoiseFilterResultProducer * 
    #process.myV0Candidates *
    process.displacedAOD)
