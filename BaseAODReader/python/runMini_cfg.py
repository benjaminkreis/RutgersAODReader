import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("RutgersAOD")
options = VarParsing.VarParsing ('analysis')
options.register ('skipEvents', 0, VarParsing.VarParsing.multiplicity.singleton, VarParsing.VarParsing.varType.int, "no of skipped events")
options.register ('isData', 0, VarParsing.VarParsing.multiplicity.singleton, VarParsing.VarParsing.varType.int, "data flag")


# Set input/output files:
# Sample MC files:
#options.inputFiles = '/store/mc/RunIISpring16MiniAODv1/WZTo3LNu_TuneCUETP8M1_13TeV-powheg-pythia8/MINIAODSIM/PUSpring16_80X_mcRun2_asymptotic_2016_v3-v1/70000/000CB9A8-4311-E611-BEC7-0CC47A4C8EC8.root'
#options.inputFiles = '/store/mc/RunIISpring16MiniAODv2/ZJetsToNuNu_HT-600To800_13TeV-madgraph/MINIAODSIM/PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/00000/8A46DB9D-B522-E611-B9A5-003048943EBC.root'
#options.inputFiles = '/store/mc/RunIISpring16MiniAODv2/TTTo2L2Nu_13TeV-powheg/MINIAODSIM/PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0_ext1-v1/60000/DEAE3C4C-7D1B-E611-94B1-B083FED03632.root'
#
# Sample DATA files:
#options.inputFiles = '/store/data/Run2016B/SingleMuon/MINIAOD/PromptReco-v2/000/273/730/00000/7CAF5C02-B221-E611-86F9-02163E01393C.root'
#options.inputFiles = '/store/data/Run2016B/DoubleEG/MINIAOD/PromptReco-v2/000/273/728/00000/1A3E0D8F-C620-E611-9CF7-02163E01281D.root'
#options.inputFiles = '/store/data/Run2016D/SingleMuon/MINIAOD/PromptReco-v2/000/276/315/00000/ACA6971B-F544-E611-B5D9-02163E0135E8.root'
options.inputFiles='/store/data/Run2016G/SingleMuon/MINIAOD/PromptReco-v1/000/278/820/00000/0667AC34-2464-E611-84CE-02163E011979.root'
#options.inputFiles = '/store/data/Run2016D/SingleElectron/MINIAOD/PromptReco-v2/000/276/315/00000/6E900E43-0045-E611-AE79-02163E01468A.root'
#options.inputFiles = '/store/data/Run2016B/SingleMuon/MINIAOD/PromptReco-v2/000/275/291/00000/94BE9211-3037-E611-83A3-02163E011F23.root'
#options.inputFiles = '/store/data/Run2016B/SingleElectron/MINIAOD/PromptReco-v2/000/275/291/00000/504F6E04-1F37-E611-BCB5-02163E0135EA.root'
#
options.outputFile = 'results.root'
#options.maxEvents = 100 # -1 means all events
#options.skipEvents = 0 # default is 0.


# Get and parse the command line arguments
options.parseArguments()
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(options.maxEvents) )
process.source = cms.Source("PoolSource",
    fileNames  = cms.untracked.vstring(options.inputFiles),
    skipEvents = cms.untracked.uint32(options.skipEvents) # default is 0.
)


# Set data vs MC:
isDataFlag=False
if options.isData == 1 : 
    isDataFlag=True
    print "runMini_cfg.py: DATA settings will be used!"
if options.isData == 0 :
    print "runMini_cfg.py: MC settings will be used!"


# Load geometry and detector conditions (needed for a few patTuple production steps)
process.load("Configuration.StandardSequences.GeometryRecoDB_cff")
process.load('Configuration.StandardSequences.MagneticField_38T_cff')
process.load('Configuration.StandardSequences.Services_cff')
# Global tag info - Accessed on May-10-2016
#    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFrontierConditions?rev=568#Global_Tags_for_RunIISpring16DR8
#    https://twiki.cern.ch/twiki/bin/view/CMS/JECDataMC?rev=112#Jet_Energy_Corrections_in_Run2
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff')
process.GlobalTag.globaltag = '80X_mcRun2_asymptotic_2016_miniAODv2'
if isDataFlag : process.GlobalTag.globaltag = '80X_dataRun2_Prompt_ICHEP16JEC_v0'  # data GT updated on Aug 11, 2016


process.options = cms.untracked.PSet()
process.options.allowUnscheduled = cms.untracked.bool( True )

# -- DISABLED - not ready for 80X yet.
# ELECTRON energy smearing and scale correction 
# -------------------------------------------------------------------------------------------------------------------------------------------------
# https://twiki.cern.ch/twiki/bin/view/CMS/EGMSmearer: Produces a new collection of electrons with the smearing and scale corrections applied
# "selectedElectronsEGMSmearerSafe" cuts follow from: https://hypernews.cern.ch/HyperNews/CMS/get/egamma/1682/2/1/1/1.html
# -------------------------------------------------------------------------------------------------------------------------------------------------
#process.selectedElectronsEGMSmearerSafe = cms.EDFilter("PATElectronSelector", src = cms.InputTag("slimmedElectrons"), 
#                                                       cut = cms.string("pt>5 && abs(eta)<2.5") )
#process.load('EgammaAnalysis.ElectronTools.calibratedElectronsRun2_cfi')
#correctionType = "76XReReco"
#files = {"Prompt2015":"EgammaAnalysis/ElectronTools/data/74X_Prompt_2015",
#         "76XReReco" :"EgammaAnalysis/ElectronTools/data/76X_16DecRereco_2015"}
#process.calibratedPatElectrons.correctionFile = cms.string(files[correctionType])
#process.calibratedPatElectrons.electrons=cms.InputTag('selectedElectronsEGMSmearerSafe')
#process.calibratedPatElectrons.isMC = cms.bool(True)
#if isDataFlag : process.calibratedPatElectrons.isMC = cms.bool(False)
#process.RandomNumberGeneratorService = cms.Service("RandomNumberGeneratorService",
#        calibratedPatElectrons = cms.PSet(
#        initialSeed = cms.untracked.uint32(1),
#        engineName = cms.untracked.string('TRandom3')
#        ),
#)


# Adding PHOTON and ELECTRON IDs
# -------------------------------------------------------------------------------------------------------------------------------------------------
# Instructions are here (VID-based recipe):
#    https://twiki.cern.ch/twiki/bin/view/CMS/CutBasedElectronIdentificationRun2#Recipe74X
#    https://twiki.cern.ch/twiki/bin/view/CMS/MultivariateElectronIdentificationRun2#VID_based_recipe_provides_pass_f
# Examples are here:
#    https://github.com/ikrav/EgammaWork/blob/ntupler_and_VID_demos_7.4.12/ElectronNtupler/test/runElectrons_VID_MVA_Spring15_25ns_NonTrig_demo.py
#    https://github.com/ikrav/EgammaWork/blob/ntupler_and_VID_demos_7.4.12/ElectronNtupler/test/runElectrons_VID_MVA_Spring15_25ns_Trig_demo.py
#    https://github.com/ikrav/EgammaWork/blob/ntupler_and_VID_demos_7.4.12/ElectronNtupler/test/runElectrons_VID_CutBased_Spring15_25ns_demo.py
# -------------------------------------------------------------------------------------------------------------------------------------------------
# PHOTON IDs
from PhysicsTools.SelectorUtils.tools.vid_id_tools import *
dataFormat = DataFormat.MiniAOD
switchOnVIDPhotonIdProducer(process, dataFormat)
my_id_modules = ['RecoEgamma.PhotonIdentification.Identification.cutBasedPhotonID_Spring15_25ns_V1_cff',
                 'RecoEgamma.PhotonIdentification.Identification.mvaPhotonID_Spring15_25ns_nonTrig_V2p1_cff']
for idmod in my_id_modules:
    setupAllVIDIdsInModule(process,idmod,setupVIDPhotonSelection) 
#
# ELECTRON IDs
switchOnVIDElectronIdProducer(process, dataFormat)
# -------------------------------------------------------------------------------------------------------------------------------------------------
# Switch to calibrated electrons in switchOnVIDElectronIdProducer (with energy smearing and scale corrections):
#   https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/SelectorUtils/python/tools/vid_id_tools.py
#   https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/RecoEgamma/ElectronIdentification/python/egmGsfElectronIDs_cff.py
#       egmGsfElectronIDSequence = cms.Sequence( electronMVAValueMapProducer * egmGsfElectronIDs * electronRegressionValueMapProducer)
# -------------------------------------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------------------------------------
# For HEEP ID please see slide 13 here: https://indico.cern.ch/event/522084/contributions/2138101/attachments/1260278/1862267/ZPrime2016Status.pdf
# - HEEP ID broke in 76X
# - Temporary fix for 76X in (HEEP V6.1)
# - Permanent fix in for 81X
# - Working on a miniAODfix for 80X --- This doesnt seem to be available as of May-10-2016 (https://twiki.cern.ch/twiki/bin/view/CMS/HEEPElectronIdentificationRun2)
# -------------------------------------------------------------------------------------------------------------------------------------------------
# customization -begin : needed to use "calibratedPatElectrons" in the output.
#process.egmGsfElectronIDs.physicsObjectIDs = cms.VPSet()                                           # -- DISABLED
#process.egmGsfElectronIDs.physicsObjectSrc = cms.InputTag("calibratedPatElectrons")                # -- DISABLED
#dataFormatString = "MiniAOD"                                                                       # -- DISABLED
#process.electronMVAValueMapProducer.srcMiniAOD =  cms.InputTag("calibratedPatElectrons","")        # -- DISABLED
#process.electronRegressionValueMapProducer.srcMiniAOD =  cms.InputTag("calibratedPatElectrons","") # -- DISABLED
# customization -end
my_id_modules = ['RecoEgamma.ElectronIdentification.Identification.mvaElectronID_Spring15_25ns_nonTrig_V1_cff',
                 'RecoEgamma.ElectronIdentification.Identification.mvaElectronID_Spring15_25ns_Trig_V1_cff',
                 'RecoEgamma.ElectronIdentification.Identification.cutBasedElectronID_Spring15_25ns_V1_cff',
                 'RecoEgamma.ElectronIdentification.Identification.heepElectronID_HEEPV60_cff']
for idmod in my_id_modules:
    setupAllVIDIdsInModule(process,idmod,setupVIDElectronSelection)


# HBHE noise filter configuration
process.load('CommonTools.RecoAlgos.HBHENoiseFilterResultProducer_cfi')
process.HBHENoiseFilterResultProducer.minZeros = cms.int32(99999)
process.HBHENoiseFilterResultProducer.IgnoreTS4TS5ifJetInLowBVRegion=cms.bool(False) 
process.HBHENoiseFilterResultProducer.defaultDecision = cms.string("HBHENoiseFilterResultRun2Loose")


# https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatAlgos/python/patSequences_cff.py
process.load('PhysicsTools.PatAlgos.patSequences_cff')


# JetToolbox: Disabled & removed in 76X
#
#from JMEAnalysis.JetToolbox.jetToolbox_cff import jetToolbox
#jetToolbox( process, 'ak4', 'jetSequence', 'out', PUMethod='CHS', miniAOD=True, addPruning=True, addNsub=True, maxTau=3, addQGTagger=True,addPUJetID=True) 


# Re-apply JEC on default MiniAOD jets - via GT
# --------------------------------------------------------------------------------------- #
#    https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookJetEnergyCorrections#CorrPatJets
#    https://twiki.cern.ch/twiki/bin/view/CMS/IntroToJEC#Mandatory_Jet_Energy_Corrections
#    https://twiki.cern.ch/twiki/bin/view/CMS/JECDataMC#Jet_Energy_Corrections_in_Run2
# Available corrections:
#    https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatAlgos/plugins/JetCorrFactorsProducer.cc#L310
# Output jet collection: "patJetsReapplyJEC"
# --------------------------------------------------------------------------------------- #
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cff import updatedPatJetCorrFactors
process.patJetCorrFactorsReapplyJEC = updatedPatJetCorrFactors.clone(
    src = cms.InputTag("slimmedJets"),
    levels = ['L1FastJet', 'L2Relative', 'L3Absolute'],
    payload = 'AK4PFchs' ) 
if isDataFlag : process.patJetCorrFactorsReapplyJEC.levels = ['L1FastJet', 'L2Relative', 'L3Absolute', 'L2L3Residuals']
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cff import updatedPatJets
process.patJetsReapplyJEC = updatedPatJets.clone(
    jetSource = cms.InputTag("slimmedJets"),
    jetCorrFactorsSource = cms.VInputTag(cms.InputTag("patJetCorrFactorsReapplyJEC"))
    )
process.reapplyJEC = cms.Sequence( process.patJetCorrFactorsReapplyJEC + process.patJetsReapplyJEC )



# Load miniAOD, make isDataFlag dependent modifications:
process.load("RutgersAODReader.BaseAODReader.miniAOD_cfi")
if isDataFlag : process.miniAOD.JERSFfileName  = process.miniAOD.JERSFfileNameDATA
if isDataFlag : process.miniAOD.JECUncFileName = process.miniAOD.JECUncFileNameDATA
process.miniAOD.patMETInputTag                 = cms.InputTag("slimmedMETs","","PAT")#this is the out-of-the-box MET in miniAOD samples.
process.miniAOD.patMETInputTagName             = cms.string("slimmedMETsOLD")
if isDataFlag : process.miniAOD.patMETInputTag = cms.InputTag("slimmedMETs","","RECO")# Data miniAODs have process names "PAT"->"RECO"


# Corrected MET from MiniAOD (for JEC/GT updates)
# -------------------------------------------------------------------------------------------------------------------------------------------------
#    https://hypernews.cern.ch/HyperNews/CMS/get/met/437/1/1.html
#    https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatAlgos/test/corMETFromMiniAOD.py
# These modifications are still relevant for 76X (to be removed in the future)
#     https://twiki.cern.ch/twiki/bin/view/CMS/MissingETUncertaintyPrescription#Instructions_for_7_4_X_X_12
# -------------------------------------------------------------------------------------------------------------------------------------------------
#customization -begin
# See: https://hypernews.cern.ch/HyperNews/CMS/get/met/458/1.html
# Default parameters are: http://cmslxr.fnal.gov/lxr/source/PhysicsTools/PatAlgos/python/cleaningLayer1/tauCleaner_cfi.py?v=CMSSW_8_0_7
# Also made it somewhat consistent with: https://gitlab.com/Thomassen/RutgersIAF/blob/master/EventAnalyzer/test/helperMiniAOD_SetupProductsMatrix.C
process.cleanPatTaus.preselection = cms.string(
    ' tauID("decayModeFindingNewDMs") > 0.5 &'
    ' tauID("byVLooseIsolationMVArun2v1DBnewDMwLT") > 0.5 &'
    ' tauID("againstMuonTight3") > 0.5 &'
    ' tauID("againstElectronVLooseMVA6") > 0.5')
#customization -end
if process.miniAOD.getJECUncFromDB :
    from PhysicsTools.PatUtils.tools.runMETCorrectionsAndUncertainties import runMetCorAndUncFromMiniAOD 
    #runMetCorAndUncFromMiniAOD(process, isData=isDataFlag, jetCleaning="Full", electronColl="calibratedPatElectrons") # -- DISABLED
    #runMetCorAndUncFromMiniAOD(process, isData=isDataFlag, jetCleaning="LepClean") #only e-mu cleaning
    runMetCorAndUncFromMiniAOD(process, isData=isDataFlag, jetCleaning="Full") #e-mu-tau-photon cleaning
else :
    jecUncFileInput = str(process.miniAOD.JECUncFileName)[16:-2]
    from RutgersAODReader.BaseAODReader.tools.runMETCorrectionsAndUncertainties import runMetCorAndUncFromMiniAOD
    ##from PhysicsTools.PatUtils.tools.runMETCorrectionsAndUncertainties import runMetCorAndUncFromMiniAOD 
    #runMetCorAndUncFromMiniAOD(process, isData=isDataFlag, jetCleaning="Full", electronColl="calibratedPatElectrons", jecUncFile=jecUncFileInput) # -- DISABLED
    runMetCorAndUncFromMiniAOD(process, isData=isDataFlag, jetCleaning="Full", jecUncFile=jecUncFileInput) 
# -------------------------------------------------------------------------------------------------------------------------------------------------
# This tool "runMetCorAndUncFromMiniAOD" recomputes "patJetsReapplyJEC" - which we explicitly run above - internally as well. 
#    So all should be consistent for jets: 
#    https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatUtils/python/tools/runMETCorrectionsAndUncertainties.py#L1194
# Default overlap-cleaning setting is jetCleaning="LepClean" --> taus and photons are ignored.
#    https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatUtils/python/tools/runMETCorrectionsAndUncertainties.py#L1390
#    https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatAlgos/python/cleaningLayer1/tauCleaner_cfi.py
# Set jetCleaning to "Full" which uses cleanPaTTaus. This is not the exact offline selection, but it is good enough.
#    https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatAlgos/python/cleaningLayer1/tauCleaner_cfi.py
# Electron, Muon, Tau, Photon energy variations are hard-coded here:
#    https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatUtils/python/tools/runMETCorrectionsAndUncertainties.py#L645
# -------------------------------------------------------------------------------------------------------------------------------------------------


#Customize MiniAODReader object inputs to pick up corrected Electon, Jet, and MET collections:
process.miniAOD.isDataTag            = cms.bool(isDataFlag)
#process.miniAOD.patElectronsInputTag = cms.InputTag("calibratedPatElectrons") # -- DISABLED
process.miniAOD.patJetsInputTag      = cms.InputTag("patJetsReapplyJEC")
process.miniAOD.patMETInputTag2      = cms.InputTag("slimmedMETs","","RutgersAOD")
process.miniAOD.patMETInputTagName2  = cms.string("slimmedMETs")
#
process.miniAOD.writeAllEvents = True
process.miniAOD.outFilename    = options.outputFile
process.miniAOD.setupList = (
    cms.PSet(type=cms.untracked.string("ObjectVariableMethod"),
             name=cms.string("PT"),
             methodName=cms.string("Pt"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValueInList"),
             name=cms.string("SaveAllOfType"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             values=cms.vstring("photon","vertex","beamspot","mc","met","electron","muon","trigger","triggerobject","filter","pfcand"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableInRange"),
             name=cms.string("PT5"),
             variableName=cms.string("PT"),
             variableType=cms.string("double"),
             low=cms.double(5),
             high=cms.double(1000000),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableInRange"),
             name=cms.string("PT10"),
             variableName=cms.string("PT"),
             variableType=cms.string("double"),
             low=cms.double(10),
             high=cms.double(1000000),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableInRange"),
             name=cms.string("PT15"),
             variableName=cms.string("PT"),
             variableType=cms.string("double"),
             low=cms.double(15),
             high=cms.double(1000000),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValueInList"),
             name=cms.string("isTrack"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             values=cms.vstring("losttrack","pftrack"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveTrack"),
             cutList=cms.vstring("isTrack","PT5"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValue"),
             name=cms.string("isMuon"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             value=cms.string("muon"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValue"),
             name=cms.string("isElectron"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             value=cms.string("electron"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValue"),
             name=cms.string("isJet"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             value=cms.string("jet"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableValue"),
             name=cms.string("isTau"),
             variableName=cms.string("INPUTTYPE"),
             variableType=cms.string("TString"),
             value=cms.string("tau"),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveJet"),
             cutList=cms.vstring("isJet","PT15"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveTau"),
             cutList=cms.vstring("isTau","PT15"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveMuon"),
             cutList=cms.vstring("isMuon","PT5"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("SaveElectron"),
             cutList=cms.vstring("isElectron","PT5"),
             doAnd=cms.bool(True),
             ),
    cms.PSet(type=cms.untracked.string("ObjectVariableCombined"),
             name=cms.string("WRITEOBJECT"),
             cutList=cms.vstring("SaveJet","SaveTrack","SaveAllOfType","SaveMuon","SaveElectron","SaveTau"),
             doAnd=cms.bool(False),
             ),
    cms.PSet(type=cms.untracked.string("Signature"),
             name=cms.string("testSignature"),
             cutList=cms.vstring(),
             ),
)

process.p = cms.Path(
    #process.calibratedPatElectrons * # -- DISABLED
    process.egmGsfElectronIDSequence*
    process.egmPhotonIDSequence* 
    process.miniAOD
)
