import FWCore.ParameterSet.Config as cms

displacedAOD = cms.EDAnalyzer(
  "DisplacedDijetAODReader",
  vertexFitterConfig = cms.PSet(
        finder = cms.string('avf'),
        sigmacut = cms.double(10.),
        Tini = cms.double(256.),
        ratio = cms.double(0.25),
        ),
  patMuonsInputTag = cms.InputTag("selectedPatMuons"),
  patElectronsInputTag = cms.InputTag("selectedPatElectrons"),
  patTausInputTag = cms.InputTag("selectedPatTaus"),
  patPhotonsInputTag = cms.InputTag("selectedPatPhotons"),
  patJetsInputTag = cms.InputTag('selectedPatJets'),
  dijetInputTag = cms.InputTag('djdijetvertices',"DisplacedDijets","RutgersAOD"),
  caloJetInputTag = cms.InputTag('ak4CaloJets'),
  patMETInputTag = cms.InputTag('patMETs'),
  trackInputTag = cms.InputTag('generalTracks'),
  vtxInputTag = cms.InputTag("offlinePrimaryVertices"),
  beamSpotInputTag = cms.InputTag('offlineBeamSpot'),
  triggerInputTag = cms.InputTag("TriggerResults","","HLT"),
  triggerEventInputTag = cms.InputTag("hltTriggerSummaryAOD","","HLT"),
  genParticleInputTag = cms.InputTag('genParticles'),
  secondaryVertexInputTag = cms.InputTag('inclusiveSecondaryVertices'),
  hcalNoiseInputTag = cms.InputTag('hcalnoise'),
  kshorts = cms.InputTag('generalV0Candidates','Kshort'),
  processName = cms.string('HLT'),
  outFilename=cms.untracked.string("output.root"),
  treeName=cms.untracked.string("tree"),
  writeAllEvents=cms.bool(True),
  printLHERunInfo     = cms.bool(False),
  genInfoTag             = cms.InputTag("externalLHEProducer"),
  getJECUncFromDB = cms.bool(False), #FROM DATABASE/GT
  JECUncFileName  =  cms.FileInPath("RutgersAODReader/BaseAODReader/data/Fall15_25nsV2_MC_Uncertainty_AK4PFchs.txt"),
  getJERSFfromGT = cms.bool(False), #Not Ready for 76X yet
  JERSFfileName = cms.FileInPath("RutgersAODReader/BaseAODReader/data/Summer15_25nsV6_MC_SF_AK4PFchs.txt"),

  cutList=cms.VPSet(),
)

myV0Candidates = cms.EDProducer("V0Producer",
                                     
    # InputTag that tells which TrackCollection to use for vertexing
    trackRecoAlgorithm = cms.InputTag('generalTracks'),

    # These bools decide whether or not to reconstruct
    #  specific V0 particles
    doKShorts = cms.bool(True),
    doLambdas = cms.bool(False),

    # Recommend leaving this one as is.
    vertexFitter = cms.bool(True),

    # set to true, uses tracks refit by the KVF for V0Candidate kinematics
    #  NOTE: useSmoothing is automatically set to FALSE
    #  if using the AdaptiveVertexFitter (which is NOT recommended)
    useSmoothing = cms.bool(True),
                                     
    # Select tracks using TrackBase::TrackQuality.
    # Select ALL tracks by leaving this vstring empty, which
    #   is equivalent to using 'loose'
    #trackQualities = cms.vstring('highPurity', 'goodIterative'),
    trackQualities = cms.vstring('highPurity'),
                                     
    # The next parameters are cut values.
    # All distances are in cm, all energies in GeV, as usual.

    # --Track quality/compatibility cuts--
    #   Normalized track Chi2 <
    tkChi2Cut = cms.double(50.0),
    tkPtCut = cms.double(0.35),
    #   Number of valid hits on track >=
    tkNHitsCut = cms.int32(0),
    #   Track impact parameter significance >
    impactParameterSigCut = cms.double(0.),
    # We calculate the PCA of the tracks quickly in RPhi, extrapolating
    # the z position as well, before vertexing.  Used in the following 2 cuts:
    #   m_pipi calculated at PCA of tracks <
    mPiPiCut = cms.double(0.6),
    #   PCA distance between tracks <
    tkDCACut = cms.double(1.),
   # Track impact parameter significance >
   tkIPSigXYCut = cms.double(2.),
   tkIPSigZCut = cms.double(-1.),
   tkIPSigCut = cms.double(-1.),

    # --V0 Vertex cuts--
    #   Vertex chi2 < 
    vtxChi2Cut = cms.double(10.0),
   # XY decay distance significance >
   vtxDecaySigXYCut = cms.double(10.),
   # XYZ decay distance significance >
   vtxDecaySigXYZCut = cms.double(-1.),
   vtxDecayRSigCut = cms.double(-1.),
   # -- miscellaneous cuts --
   # check if either track has a hit radially inside the vertex position minus this number times the sigma of the vertex fit
   # note: Set this to -1 to disable this cut, which MUST be done if you want to run V0Producer on the AOD track collection!
   # cos(angleXY) between x and p of V0 candidate >
   cosThetaXYCut = cms.double(0.9998),
   v0CosThetaCut = cms.double(0.9998),
   # cos(angleXYZ) between x and p of V0 candidate >
   cosThetaXYZCut = cms.double(-2.),

    #   Lambda collinearity cut
    #   (UNUSED)
    collinearityCut = cms.double(0.02),
    #   Vertex radius cut >
    #   (UNUSED)
    rVtxCut = cms.double(0.0),
    #   V0 decay length from primary cut >
    #   (UNUSED)
    lVtxCut = cms.double(0.0),
    #   Radial vertex significance >
    vtxSignificance2DCut = cms.double(0.0),
    #   3D vertex significance using primary vertex
    #   (UNUSED)
    vtxSignificance3DCut = cms.double(0.0),
    #   V0 mass window, Candidate mass must be within these values of
    #     the PDG mass to be stored in the collection
    kShortMassCut = cms.double(0.07),
    lambdaMassCut = cms.double(0.05),
    #   Mass window cut using normalized mass (mass / massError)
    #   (UNUSED)
    kShortNormalizedMassCut = cms.double(0.0),
    lambdaNormalizedMassCut = cms.double(0.0),
    # We check if either track has a hit inside (radially) the vertex position
    #  minus this number times the sigma of the vertex fit
    #  NOTE: Set this to -1 to disable this cut, which MUST be done
    #  if you want to run V0Producer on the AOD track collection!
    innerHitPosCut = cms.double(-1.),

    useRefTracks = cms.bool(True),
)
