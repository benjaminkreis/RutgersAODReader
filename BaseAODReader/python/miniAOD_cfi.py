import FWCore.ParameterSet.Config as cms

miniAOD = cms.EDAnalyzer("MiniAODReader",
  isDataTag = cms.bool(False),

  patMuonsInputTag     = cms.InputTag("slimmedMuons"),
  patElectronsInputTag = cms.InputTag("slimmedElectrons"),
  patTausInputTag      = cms.InputTag("slimmedTaus"),
  patPhotonsInputTag   = cms.InputTag("slimmedPhotons"),
  patJetsInputTag      = cms.InputTag('slimmedJets'),
  patMETInputTag       = cms.InputTag('slimmedMETs'),
  patMETInputTagName   = cms.string("slimmedMETs"),
  patMETInputTag2      = cms.InputTag('slimmedMETsNoHF'), # No longer present in MiniAODv2 76X samples
  patMETInputTagName2  = cms.string("slimmedMETsNoHF"),   # No longer present in MiniAODv2 76X samples
  trackInputTag        = cms.InputTag('lostTracks'),
  lostTrackInputTag    = cms.InputTag('lostTracks'),
  pfCandidateInputTag  = cms.InputTag('packedPFCandidates'),
  vtxInputTag          = cms.InputTag("offlineSlimmedPrimaryVertices"),
  beamSpotInputTag     = cms.InputTag('offlineBeamSpot'),
  triggerInputTag      = cms.InputTag("TriggerResults","","HLT"),

  pfCandMinPt = cms.double(1.5), #warning, setting this too low (<1) causes a large increase in output file size! Used in MiniAODReader::makePFCands().

  filter              = cms.InputTag("TriggerResults","","PAT"),
  hcalNoiseInputTag   = cms.InputTag('hcalnoise'),

  printLHERunInfo     = cms.bool(False), #Controls LHERunInfoProduct printout

  getJERSFfromGT = cms.bool(False), #data is not ready yet in 2016
  JERSFfileName     = cms.FileInPath("RutgersAODReader/BaseAODReader/data/Spring16_25nsV1_MC_SF_AK4PFchs.txt"),  #for MC
  JERSFfileNameDATA = cms.FileInPath("RutgersAODReader/BaseAODReader/data/Fall15_25nsV2_DATA_SF_AK4PFchs.txt"),#for DATA -- this needs to be updated!!
  # Text files are in: https://github.com/cms-jet/JRDatabase/tree/master/textFiles
  
  getJECUncFromDB = cms.bool(True), 
  JECUncFileName     =  cms.FileInPath("RutgersAODReader/BaseAODReader/data/Spring16_25nsV2_MC_Uncertainty_AK4PFchs.txt"),  #for MC 
  JECUncFileNameDATA =  cms.FileInPath("RutgersAODReader/BaseAODReader/data/Spring16_25nsV2_DATA_Uncertainty_AK4PFchs.txt"),#for DATA
  # Database (DB) object is specified by the GT
  # Text files are in: https://github.com/cms-jet/JECDatabase/tree/master/textFiles
  # Check here for the most up to date MC GT: https://twiki.cern.ch/twiki/bin/view/CMS/JECDataMC#Jet_Energy_Corrections_in_Run2
  # Use-case example in: https://twiki.cern.ch/twiki/bin/view/CMS/JECUncertaintySources#2015_JEC
  
  genParticleInputTag       = cms.InputTag('prunedGenParticles'),
  packedGenParticleInputTag = cms.InputTag('packedGenParticles'),
  
  electronMVAweights =  cms.vstring(
  "RutgersAODReader/BaseAODReader/data/EIDmva_EB1_5_oldNonTrigSpring15_ConvVarCwoBoolean_TMVA412_FullStatLowPt_PairNegWeightsGlobal_BDT.weights.xml",
  "RutgersAODReader/BaseAODReader/data/EIDmva_EB2_5_oldNonTrigSpring15_ConvVarCwoBoolean_TMVA412_FullStatLowPt_PairNegWeightsGlobal_BDT.weights.xml",
  "RutgersAODReader/BaseAODReader/data/EIDmva_EE_5_oldNonTrigSpring15_ConvVarCwoBoolean_TMVA412_FullStatLowPt_PairNegWeightsGlobal_BDT.weights.xml",
  "RutgersAODReader/BaseAODReader/data/EIDmva_EB1_10_oldNonTrigSpring15_ConvVarCwoBoolean_TMVA412_FullStatLowPt_PairNegWeightsGlobal_BDT.weights.xml",
  "RutgersAODReader/BaseAODReader/data/EIDmva_EB2_10_oldNonTrigSpring15_ConvVarCwoBoolean_TMVA412_FullStatLowPt_PairNegWeightsGlobal_BDT.weights.xml",
  "RutgersAODReader/BaseAODReader/data/EIDmva_EE_10_oldNonTrigSpring15_ConvVarCwoBoolean_TMVA412_FullStatLowPt_PairNegWeightsGlobal_BDT.weights.xml",
  ),

  eleMediumIdMap   = cms.InputTag("egmGsfElectronIDs:mvaEleID-Spring15-25ns-nonTrig-V1-wp90"),
  eleTightIdMap    = cms.InputTag("egmGsfElectronIDs:mvaEleID-Spring15-25ns-nonTrig-V1-wp80"),
  mvaValuesMap     = cms.InputTag("electronMVAValueMapProducer:ElectronMVAEstimatorRun2Spring15NonTrig25nsV1Values"),
  mvaCategoriesMap = cms.InputTag("electronMVAValueMapProducer:ElectronMVAEstimatorRun2Spring15NonTrig25nsV1Categories"),

  eleVetoIdMapCutBased   = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Spring15-25ns-V1-standalone-veto"),
  eleLooseIdMapCutBased  = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Spring15-25ns-V1-standalone-loose"),
  eleMediumIdMapCutBased = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Spring15-25ns-V1-standalone-medium"),
  eleTightIdMapCutBased  = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Spring15-25ns-V1-standalone-tight"),
  eleHEEPIdMap           = cms.InputTag("egmGsfElectronIDs:heepElectronID-HEEPV60"),

  phoLooseIdMap  = cms.InputTag("egmPhotonIDs:cutBasedPhotonID-Spring15-25ns-V1-standalone-loose"),
  phoMediumIdMap = cms.InputTag("egmPhotonIDs:cutBasedPhotonID-Spring15-25ns-V1-standalone-medium"),
  phoTightIdMap  = cms.InputTag("egmPhotonIDs:cutBasedPhotonID-Spring15-25ns-V1-standalone-tight"),
  phoChargedIsolationMap       = cms.InputTag("photonIDValueMapProducer", "phoChargedIsolation"),
  phoNeutralHadronIsolationMap = cms.InputTag("photonIDValueMapProducer", "phoNeutralHadronIsolation"),
  phoPhotonIsolationMap        = cms.InputTag("photonIDValueMapProducer", "phoPhotonIsolation"),
  phoWorstChargedIsolationMap  = cms.InputTag("photonIDValueMapProducer", "phoWorstChargedIsolation"),

  genEventInfoProductTag = cms.InputTag("generator"),
  genInfoTag             = cms.InputTag("externalLHEProducer"),
  # ----------------------------------------------------------------------------------------------------------------------------------------------
  # PDF weights for Madgraph and Poweheg (LHE) samples are available via "externalLHEProducer" and are pre-stored in the miniAOD samples.
  #   Powheg   NLO RunIIFall15MiniAODv2 76X samples use: NNPDF30_nlo_as_0118    (ex/ ttHToTT_M125_13TeV_powheg_pythia8)
  #   Madgraph NLO RunIIFall15MiniAODv2 76X samples use: NNPDF30_nlo_nf_5_pdfas (ex/ TTJets_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8)
  #   Madgraph  LO RunIIFall15MiniAODv2 76X samples use: NNPDF30_lo_as_0130     (ex/ TTJets_HT-1200to2500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8)
  # ---------------------------------------------------------------------------------------------------------------------------------------------
  # PDF weights for Pythia8 samples (non LHE), for ex/ LQ samples, should be accessed externally via LHAPDF.
  # Samples generated with Pythia8 Tune CUETP8M1 use NNPDF2.3LO PDF sets.
  # In particular, /LQLQTo*_TuneCUETP8M1_13TeV_pythia8/RunIIFall15MiniAODv2*_76X_*/MINIAODSIM use NNPDF23_lo_as_0130_qed.
  # The names of these external PDF sets need to be specified in PDFSets parameter below.
  # All sets are listed here: http://lhapdf.hepforge.org/pdfsets.html
  # For a given CMSSW release, the list directory is available under: 
  #   scram tool info lhapdf ->
  #   LHAPDF_DATA_PATH=/cvmfs/cms.cern.ch/slc6_amd64_gcc493/external/lhapdf/6.1.5-kpegke3/share/LHAPDF/
  # ---------------------------------------------------------------------------------------------------------------------------------------------
  #PDFSets = cms.vstring("NNPDF30_nlo_nf_5_pdfas.LHgrid"),
  #PDFSets = cms.vstring("NNPDF30_lo_as_0130.LHgrid"),
  #PDFSets = cms.vstring("NNPDF23_lo_as_0130_qed.LHgrid","NNPDF23_lo_as_0119_qed.LHgrid","NNPDF30_lo_as_0130.LHgrid","NNPDF30_lo_as_0118.LHgrid"),
  PDFSets = cms.vstring("NNPDF23_lo_as_0130_qed.LHgrid","NNPDF23_lo_as_0119_qed.LHgrid"),

  outFilename     = cms.untracked.string("output.root"),
  treeName        = cms.untracked.string("tree"),
  writeAllEvents  = cms.bool(True),
  writeAllObjects = cms.bool(False),
  setupList       = cms.VPSet(),

  processName = cms.string('HLT'),
  triggerEventInputTag     = cms.InputTag("TriggerEvent"),
  triggerPrescalesInputTag = cms.InputTag("patTrigger"),                         
  triggerObjectHLT = cms.vstring( ), #not used at the moment, see earlier commits for examples.
  triggerObjectFilter = cms.vstring(
  # SingleMuon PD
  'hltL3fL1sMu22Or25L1f0L2f10QL3Filtered50Q',              #HLT_Mu50_v3,v4
  'hltL3fL1sMu16orMu25L1f0L2f10QL3Filtered50Q',            #HLT_Mu50_v2
  'hltL3fL1sMu25f0TkFiltered50Q',                          #HLT_TkMu50_v1,v2,v3
  'hltL3fL1sMu1lqL1f0L2f10L3Filtered17TkIsoFiltered0p4',   #HLT_Mu17_TrkIsoVVL_v2,v3
  'hltL3fL1sMu5L1f0L2f5L3Filtered8TkIsoFiltered0p4',       #HLT_Mu8_TrkIsoVVL_v3,v4
  'hltL3crIsoL1sMu20L1f0L2f10QL3f22QL3trkIsoFiltered0p09', #HLT_IsoMu22_v2,v3
  'hltL3fL1sMu20L1f0Tkf22QL3trkIsoFiltered0p09',           #HLT_IsoTkMu22_v2,v3,v4
  'hltL3fL1sMu20L1f0Tkf22QL3trkIsoFiltered0p09',           #HLT_IsoTkMu22_v2,v3,v4
  #hltL3fL1sMu10lqL1f0L2f10L3Filtered17 #HLT_Mu17_v3 (add?)
  #hltL3fL1sMu5L1f0L2f5L3Filtered8      #HLT_Mu8_v4 (add?)
   #
   # DoubleMu PD
  'hltDiMuonGlb17Glb8RelTrkIsoFiltered0p4',              #HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_v2,v3,v4  (also good for pre-DZ step of the filter below)
  'hltDiMuonGlb17Glb8RelTrkIsoFiltered0p4DzFiltered0p2', #HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_v2,v3
  'hltDiMuonGlb17Trk8RelTrkIsoFiltered0p4',              #HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_v2,v3   (also good for pre-DZ step of the filter below)
  'hltDiMuonGlb17Trk8RelTrkIsoFiltered0p4DzFiltered0p2', #HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_DZ_v2,v3,v4
  'hltDiMuonGlb17Glb8DzFiltered0p2SameSign',             #HLT_Mu17_Mu8_SameSign_DZ_v1,v2,v3
  'hltDiMuonGlb17Glb8DzFiltered0p2',                     #HLT_Mu17_Mu8_SameSign_DZ_v1,v2,v3  (DZ filter)
  'hltL3pfL1sDoubleMu114L1f0L2pf0L3PreFiltered8',        #HLT_Mu17_Mu8_SameSign_DZ_v1,v2,v3  (pre DZ filter)
  'hltL3fL1sDoubleMu114L1f0L2f10OneMuL3Filtered17',      #HLT_Mu17_Mu8_SameSign_DZ_v1,v2,v3  (pre DZ filter)
  'hltDiMuonGlb30Trk11DzFiltered0p2',                    #HLT_Mu30_TkMu11_v2,v3
  'hltDiMuonGlbFiltered30TrkFiltered11',                 #HLT_Mu30_TkMu11_v2,v3 (pre DZ filter)
   #
   # SingleEle PD
  'hltEle12CaloIdLTrackIdLIsoVLTrackIsoFilter',   #HLT_Ele12_CaloIdL_TrackIdL_IsoVL_v3,v4,v5,v6
  'hltEle17CaloIdLTrackIdLIsoVLTrackIsoFilter',   #HLT_Ele17_CaloIdL_TrackIdL_IsoVL_v2,v3,v4,v5
  'hltEle23CaloIdLTrackIdLIsoVLTrackIsoFilter',   #HLT_Ele23_CaloIdL_TrackIdL_IsoVL_v3,v4,v5,v6
  'hltEle27noerWPLooseGsfTrackIsoFilter',         #HLT_Ele27_WPLoose_Gsf_v1,v2,v3,v4
  'hltEle27WPTightGsfTrackIsoFilter',             #HLT_Ele27_WPTight_Gsf_v1,v2,v3,v4
   #
   # DoubleEle PD
  'hltEle23Ele12CaloIdLTrackIdLIsoVLDZFilter',             #HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ_v3,v4,v5,v6
  'hltEle23Ele12CaloIdLTrackIdLIsoVLTrackIsoLeg1Filter',   #HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ_v3,v4,v5,v6 (same as above, before DZ cut)
  'hltEle23Ele12CaloIdLTrackIdLIsoVLTrackIsoLeg2Filter',   #HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ_v3,v4,v5,v6 (same as above, before DZ cut)
   #
   # MuonEG PD (pairs of filters correspond to muon and electron legs separately)
  'hltMu23TrkIsoVVLEle12CaloIdLTrackIdLIsoVLMuonlegL3IsoFiltered23', 'hltMu23TrkIsoVVLEle12CaloIdLTrackIdLIsoVLElectronlegTrackIsoFilter', #HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_v3,v4,v5,v6
  'hltMu23TrkIsoVVLEle8CaloIdLTrackIdLIsoVLMuonlegL3IsoFiltered23',  'hltMu23TrkIsoVVLEle8CaloIdLTrackIdLIsoVLElectronlegTrackIsoFilter',  #HLT_Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL_v1,v2,v3,v4
  'hltMu8TrkIsoVVLEle23CaloIdLTrackIdLIsoVLMuonlegL3IsoFiltered8',   'hltMu8TrkIsoVVLEle23CaloIdLTrackIdLIsoVLElectronlegTrackIsoFilter',  #HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_v3,v4,v5,v6
   #
   # Not used for now:
  'hltDiMu9Ele9CaloIdLTrackIdLMuonlegL3Filtered9',                   'hltDiMu9Ele9CaloIdLTrackIdLElectronlegDphiFilter',  #HLT_DiMu9_Ele9_CaloIdL_TrackIdL_v
  'hltL3fL1sMu22orMu25orMu20EG15orMu5EG20L1f0L2f10QL3Filtered30Q',   'hltEle30CaloIdLGsfTrkIdVLDPhiUnseededFilter',       #HLT_Mu30_Ele30_CaloIdL_GsfTrkIdVL_v
  'hltMu8DiEle12CaloIdLTrackIdLMuonlegL3Filtered8',                  'hltMu8DiEle12CaloIdLTrackIdLElectronlegDphiFilter'  #HLT_Mu8_DiEle12_CaloIdL_TrackIdL_v
  )
)
