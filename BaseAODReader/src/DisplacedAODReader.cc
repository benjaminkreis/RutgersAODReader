#include "FWCore/Framework/interface/EventSetup.h"
#include "RutgersAODReader/BaseAODReader/interface/DisplacedAODReader.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureObject.h"
#include "DataFormats/Math/interface/Point3D.h"
#include "MagneticField/Records/interface/IdealMagneticFieldRecord.h"
#include "DataFormats/GeometryCommonDetAlgo/interface/GlobalError.h"
#include "RecoVertex/VertexPrimitives/interface/ConvertToFromReco.h"
#include "RecoVertex/VertexPrimitives/interface/ConvertError.h"
#include "TrackingTools/GeomPropagators/interface/StateOnTrackerBound.h"
#include "TrackingTools/TrajectoryState/interface/FreeTrajectoryState.h"
#include "TrackingTools/TrajectoryState/interface/TrajectoryStateOnSurface.h"
#include "TrackingTools/TrajectoryState/interface/TrajectoryStateTransform.h"
#include "TrackingTools/Records/interface/TrackingComponentsRecord.h"
#include "RecoVertex/VertexPrimitives/interface/TransientVertex.h"
#include <TTree.h>
#include <TFile.h>
#include <TVector3.h>
#include <TMatrixDSym.h>
#include <TMatrixDSymEigen.h>
#include <assert.h>
#include <math.h>
#include <algorithm>

using namespace std;
using namespace edm;

DisplacedAODReader::DisplacedAODReader(const ParameterSet& pset) : BaseAODReader(pset)
{
  vertexFitter_  = new KalmanVertexFitter();
  vertexBeam_ = new VertexCompatibleWithBeam(vertexDistanceXY_,100);
  maxTrackToJetDeltaR_ = 0.4;
  if(pset.exists("maxTrackToJetDeltaR"))maxTrackToJetDeltaR_ = pset.getParameter<double>("maxTrackToJetDeltaR");

  cleanSignificanceCut_ = 3.0;
  if(pset.exists("cleanSignificanceCut"))cleanSignificanceCut_ = pset.getParameter<double>("cleanSignificanceCut");

  minTrackPtForDiTrack_ = 1.0;
  if(pset.exists("minTrackPtForDiTrack"))minTrackPtForDiTrack_ = pset.getParameter<double>("minTrackPtForDiTrack");

  minTrack3DSigForDiTrack_ = 3.0;
  if(pset.exists("minTrack3DSigForDiTrack"))minTrack3DSigForDiTrack_ = pset.getParameter<double>("minTrack3DSigForDiTrack");

  genParticleLabel_ = pset.getParameter<InputTag>("genParticleInputTag");
  genParticleToken_ = consumes<reco::GenParticleCollection>(genParticleLabel_);

  hcalLabel_ = pset.getParameter<InputTag>("hcalNoiseInputTag");
  hcalToken_ = consumes<HcalNoiseSummary>(hcalLabel_);

  genInfoLabel_ = InputTag("source");
  if(pset.exists("genInfo"))genInfoLabel_ = pset.getParameter<InputTag>("genInfo");
  genInfoToken_ = consumes<LHEEventProduct>(genInfoLabel_);

  secondaryVertexLabel_ = pset.getParameter<InputTag>("secondaryVertexInputTag");
  secondaryVertexToken_ = consumes<edm::View<reco::Vertex> >(secondaryVertexLabel_);

  caloJetLabel_ = pset.getParameter<InputTag>("caloJetInputTag");
  caloJetToken_ = consumes<edm::View<reco::CaloJet> >(caloJetLabel_);

  thePropagatorName_ = "PropagatorWithMaterial";
  if(pset.exists("propagatorName"))thePropagatorName_ = pset.getParameter<std::string>("propagatorName");

  vtxfitter_ = new ConfigurableVertexReconstructor(pset.getParameter<edm::ParameterSet>("vertexFitterConfig"));

  hIsoNoiseLabel_ = InputTag("HBHENoiseFilterResultProducer", "HBHEIsoNoiseFilterResult");
  if(pset.exists("hIsoNoise"))hIsoNoiseLabel_ = pset.getParameter<InputTag>("hIsoNoise");
  hIsoNoiseToken_ = consumes<bool>(hIsoNoiseLabel_);

  hNoiseLabel_ = InputTag("HBHENoiseFilterResultProducer", "HBHENoiseFilterResult");
  if(pset.exists("hNoise"))hNoiseLabel_ = pset.getParameter<InputTag>("hNoise");
  hNoiseToken_ = consumes<bool>(hNoiseLabel_);

  pileupLabel_ = InputTag("addPileupInfo");
  if(pset.exists("pileup"))pileupLabel_ = pset.getParameter<InputTag>("pileup");
  pileupToken_ = consumes<View<PileupSummaryInfo> >(pileupLabel_);

  ksLabel_ = pset.getParameter<InputTag>("kshorts");
  ksToken_ = consumes<reco::VertexCompositeCandidateCollection>(ksLabel_);

  rhoAllLabel_ = InputTag("fixedGridRhoFastjetAll");
  if(pset.exists("rhoAll"))rhoAllLabel_ = pset.getParameter<InputTag>("rhoAll");
  rhoAllToken_ = consumes<double>(rhoAllLabel_);

  rhoNeutralLabel_ = InputTag("fixedGridRhoFastjetCentralNeutral");
  if(pset.exists("rhoNeutral"))rhoNeutralLabel_ = pset.getParameter<InputTag>("rhoNeutral");
  rhoNeutralToken_ = consumes<double>(rhoNeutralLabel_);

  mPion_ = 0.134966;

  processTracks_ = false;
  if(pset.exists("processTracks"))processTracks_ = pset.getParameter<bool>("processTracks");

}

DisplacedAODReader::~DisplacedAODReader()
{


}

bool greater_than_Energy(const TLorentzVector &v1, const TLorentzVector &v2){
  return (v1.E() > v2.E());
}

void DisplacedAODReader::analyze(const Event& event, const EventSetup& eventsetup)
{

  m_currentEntry++;

  clearProducts();
 
  event_ = (long)event.id().event();
  run_ = (int)event.id().run();
  lumi_ = (int)event.luminosityBlock();
  stupakR_ = 0;
  stupakR2_ = 0;

  //eventsetup.get<TransientTrackRecord>().get("TransientTrackBuilder",trackBuilder_);

  edm::ESHandle<MagneticField> magneticField;
  eventsetup.get<IdealMagneticFieldRecord>().get(magneticField);
  magneticField_ = &*magneticField;

  eventsetup.get<TrackingComponentsRecord>().get(thePropagatorName_,thePropagator_);

  eventsetup.get<TransientTrackRecord>().get("TransientTrackBuilder",theB_);

  makeProducts(event,eventsetup);

  m_products = m_productmap;

  prepareEvent();
  setVariable("EVENT",event_);
  setVariable("RUN",run_);
  setVariable("LUMI",lumi_);
  setVariable("stupakR",stupakR_);
  setVariable("stupakR2",stupakR2_);

  if(event.getByToken(rhoAllToken_,rhoAllHandle_))
    setVariable("rhoAll",(*rhoAllHandle_));

  if(event.getByToken(rhoNeutralToken_,rhoNeutralHandle_))
    setVariable("rhoNeutral",(*rhoNeutralHandle_));

  if(event.getByToken(genInfoToken_,genInfoHandle_))
    makeGenInfo();

  if(event.getByToken(pileupToken_,pileupHandle_))
    makePileupInfo();

  analyzeEvent();

  bool writeEvent = false;
  bool isSet = getVariable("WRITEEVENT",writeEvent);
  writeEvent &= isSet;

  if(writeAllEvents_ || writeEvent)fillTree();

}

void DisplacedAODReader::makeProducts(const Event& event, const EventSetup& eventsetup)
{
  if(event.getByToken(vertexToken_,vertexHandle_) && event.getByToken(secondaryVertexToken_,secondaryVertexHandle_))
    makeVertices();
  else
    assert(0);

  if(event.getByToken(beamspotToken_, beamspotHandle_))
    makeBeamSpot();
  else
    assert(0);

  if(event.getByToken(hIsoNoiseToken_, hIsoNoiseHandle_) && event.getByToken(hNoiseToken_,hNoiseHandle_))
    makeFilters();

  if(event.getByToken(trackToken_,trackHandle_) && event.getByToken(jetToken_,jetHandle_))
    makeJets();

  if(event.getByToken(trackToken_,trackHandle_) && event.getByToken(caloJetToken_,caloJetHandle_))
    makeCaloJets(eventsetup);

  if(event.getByToken(electronToken_,electronHandle_))
    makeElectrons();

  if(event.getByToken(muonToken_,muonHandle_))
    makeMuons();

  if(event.getByToken(photonToken_,photonHandle_)){
    makePhotons();
  }

  //if(event.getByToken(tauToken_,tauHandle_))
  //  makeTaus();

  if(event.getByToken(metToken_,metHandle_))
    makeMET();

  if(processTracks_ && event.getByToken(trackToken_,trackHandle_))
    makeTracks(eventsetup);

  if(event.getByToken(ksToken_,ksHandle_))
    makeKshorts();

  if(event.getByToken(triggerToken_, triggerHandle_) && event.getByToken(triggerEventToken_, triggerEventHandle_)) 
    makeTriggers(event,eventsetup);

  if(event.getByToken(genParticleToken_, genParticleHandle_))
  //if(event.getByToken(packedGenToken_, packedGenHandle_))
    makeGenParticles();

  if(event.getByToken(hcalToken_,hcalHandle_))
    makeHcalNoise();

}

void DisplacedAODReader::makeCaloJets(const edm::EventSetup& iSetup)
{

  vertexBeam_->setBeamSpot(*beamspotHandle_);
  trackToCaloJetMap_.clear();
  trackToCaloJetMap_ = vector<int>(trackHandle_->size(),-1);
  whichVertex_.clear();
  whichVertex_ = vector<int>(trackHandle_->size(),-1);
  for(int i = 0; i < (int)trackHandle_->size(); i++){
    double maxWeight = 0;
    int jj = -1;
    reco::TrackBaseRef tref(trackHandle_,i);
    for(int j = 0; j < (int)vertexHandle_->size();j++){
      if(vertexHandle_->at(j).trackWeight(tref) > maxWeight){
        maxWeight = vertexHandle_->at(j).trackWeight(tref);
        jj = j;
      }
    }
    whichVertex_[i] = jj;
  }

  StateOnTrackerBound stateOnTracker(thePropagator_.product());

  double stupakR = 0;
  double stupakR2 = 0;
  double totRpT = 0;
  double totRpT2 = 0;

  vector<SignatureObject*> jets;
  for(int i = 0; i < (int)caloJetHandle_->size(); i++){
    float sumIP = 0;
    float sumIPPt = 0;
    float sumIPSig = 0;
    float sumIPLogSig = 0;
    float IVFScore = 0; 
    int nTracksIPlt0p05 = 0;
    int nTracksIPSiggt10 = 0;
    int nTracksIPSiglt5 = 0;
    
    const reco::CaloJet& calo_jet = caloJetHandle_->at(i);
    TVector3 jetVec;
    jetVec.SetPtEtaPhi(calo_jet.detectorP4().Pt(),calo_jet.detectorP4().Eta(),calo_jet.detectorP4().phi());

    SignatureObject* jet = new SignatureObject(calo_jet.detectorP4().Px(),calo_jet.detectorP4().Py(),calo_jet.detectorP4().Pz(),calo_jet.detectorP4().E());

    jet->setVariable("INPUTTYPE",TString("calojet"));
    jet->setVariable("jetArea",calo_jet.jetArea());
    jet->setVariable("pileup",calo_jet.pileup());
    jet->setVariable("maxEInEmTowers",calo_jet.maxEInEmTowers());
    jet->setVariable("maxEInHadTowers",calo_jet.maxEInHadTowers());
    jet->setVariable("energyFractionHadronic",calo_jet.energyFractionHadronic());
    jet->setVariable("emEnergyFraction",calo_jet.emEnergyFraction());
    jet->setVariable("hadEnergyInHB",calo_jet.hadEnergyInHB());
    jet->setVariable("hadEnergyInHO",calo_jet.hadEnergyInHO());
    jet->setVariable("hadEnergyInHE",calo_jet.hadEnergyInHE());
    jet->setVariable("hadEnergyInHF",calo_jet.hadEnergyInHF());
    jet->setVariable("emEnergyInEB",calo_jet.emEnergyInEB());
    jet->setVariable("emEnergyInEE",calo_jet.emEnergyInEE());
    jet->setVariable("emEnergyInHF",calo_jet.emEnergyInHF());
    jet->setVariable("nConstituents",calo_jet.nConstituents());
    jet->setVariable("jetIndex",i);
    //jet->setVariable("n90",calo_jet.n90());
    //jet->setVariable("n60",calo_jet.n60());

    map<reco::TransientTrack,reco::TrackBaseRef> refMap;
    vector<reco::TransientTrack> transientTracks;
    vector<TrajectoryStateOnSurface> tsosList;
    vector<float> tracksIPLogSig;
    vector<float> tracksIPLog10Sig;
    vector<float> trackAngles;
    vector<int> vertexVector;
    double totalTrackAngle = 0;
    double totalTrackPt = 0;
    double totalTrackAnglePt = 0;
    double minR = 10000;
    double minPt = 0;

    TLorentzVector sumVector(0,0,0,0);
    vector<TLorentzVector> trackVectors;

    int nMissingInner = 0;
    int nMissingOuter = 0;


    for(int j = 0; j < (int)trackHandle_->size(); j++){
      reco::TrackBaseRef tref(trackHandle_,j);
      if (tref->pt() < minTrackPtForDiTrack_)continue;
      if (!tref->quality(reco::TrackBase::highPurity)) continue;
      FreeTrajectoryState fts = trajectoryStateTransform::initialFreeState(trackHandle_->at(j),magneticField_);
      TrajectoryStateOnSurface outer = stateOnTracker(fts);
      if(!outer.isValid())continue;
      GlobalPoint outerPos = outer.globalPosition();
      TVector3 trackPos(outerPos.x(),outerPos.y(),outerPos.z());
      double drt = trackPos.DeltaR(jetVec);
      if(drt > maxTrackToJetDeltaR_)continue;
      if(trackToCaloJetMap_[j] < 0)trackToCaloJetMap_[j] = i;
      if(drt < minR){
        minR = drt;
        minPt = tref->pt();
      }
      reco::TransientTrack tt(trackHandle_->at(j),magneticField_);
      if(!tt.isValid())continue;
      transientTracks.push_back(tt);
      vertexVector.push_back(whichVertex_[j]);

      TLorentzVector p(tref->px(),tref->py(),tref->pz(),sqrt(pow(tref->px(),2) + pow(tref->py(),2) + pow(tref->pz(),2) + pow(mPion_,2)));
      trackVectors.push_back(p);
      sumVector += p;

      nMissingInner += tref->hitPattern().numberOfLostTrackerHits(reco::HitPattern::MISSING_INNER_HITS);
      nMissingOuter += tref->hitPattern().numberOfLostTrackerHits(reco::HitPattern::MISSING_OUTER_HITS);
      
      static GetTrackTrajInfo getTrackTrajInfo; 
      vector<GetTrackTrajInfo::Result> trajInfo = getTrackTrajInfo.analyze(iSetup, (*tref));
      if ( trajInfo.size() > 0 && trajInfo[0].valid) {
        const TrajectoryStateOnSurface& tsosInnerHit = trajInfo[0].detTSOS;
        double ta = fabs(trackAngle(tt,tsosInnerHit));
        totalTrackAngle += ta;
        totalTrackAnglePt += ta*tref->pt();
        totalTrackPt += tref->pt();
        trackAngles.push_back(log10(ta));
        tsosList.push_back(tsosInnerHit);
      }

      double dxy = fabs(tref->dxy(*beamspotHandle_));
      double dxyerr = tref->dxyError();
      double dxySig = 0;
      if (dxyerr > 0)dxySig = dxy/dxyerr;

      sumIP += dxy;
      sumIPPt += dxy * trackHandle_->at(j).pt();
      sumIPSig += dxySig;
      sumIPLogSig += log(dxySig);
      tracksIPLogSig.push_back(log(dxySig));
      tracksIPLog10Sig.push_back(log10(dxySig));
      IVFScore += 1.0/trackPos.DeltaR(jetVec);
      nTracksIPlt0p05 += dxy < 0.05 ? 1 : 0;
      nTracksIPSiggt10 += dxySig > 10.0 ? 1 : 0;
      nTracksIPSiglt5 += dxySig < 5.0 ? 1 : 0;
    }
    sort(tracksIPLogSig.begin(), tracksIPLogSig.end());
    sort(tracksIPLog10Sig.begin(), tracksIPLog10Sig.end());
    sort(trackAngles.begin(), trackAngles.end());

    stupakR += minR*minPt;
    stupakR2 += minR*minPt*minPt;
    totRpT += minPt;
    totRpT2 += minPt*minPt;

    jet->setVariable("MISSINGINNER",nMissingInner);
    jet->setVariable("MISSINGOUTER",nMissingOuter);
    jet->setVariable("AVGMISSINGINNER",double(nMissingInner)/double(transientTracks.size()));
    jet->setVariable("AVGMISSINGOUTER",double(nMissingOuter)/double(transientTracks.size()));

    jet->setVariable("stupakPt",minPt);
    jet->setVariable("stupakR",minR);

    jet->setVariable("sumIP",sumIP);
    jet->setVariable("sumIPpTWeight",sumIPPt);
    if(sumIP > 0)jet->setVariable("sumIPpTNorm",sumIPPt/sumIP);
    jet->setVariable("sumIPSig",sumIPSig);
    jet->setVariable("sumIPLogSig",sumIPLogSig);
    jet->setVariable("nMatchedTracks",(int)transientTracks.size());
    
    if(tracksIPLogSig.size() == 0){
      //do nothing
    }else if((tracksIPLogSig.size()%2 == 0)){
      jet->setVariable("medianIPLogSig",(tracksIPLogSig.at(tracksIPLogSig.size()/2-1)+tracksIPLogSig.at((tracksIPLogSig.size()/2)))/2);
    }else{
      jet->setVariable("medianIPLogSig",tracksIPLogSig.at((tracksIPLogSig.size()-1)/2));
    }
    if(tracksIPLog10Sig.size() == 0){
      //do nothing
    }else if((tracksIPLog10Sig.size()%2 == 0)){
      jet->setVariable("medianIPLog10Sig",(tracksIPLog10Sig.at(tracksIPLog10Sig.size()/2-1)+tracksIPLog10Sig.at((tracksIPLog10Sig.size()/2)))/2);
    }else{
      jet->setVariable("medianIPLog10Sig",tracksIPLog10Sig.at((tracksIPLog10Sig.size()-1)/2));
    }
    if(trackAngles.size() == 0){
      //do nothing
    }else if(trackAngles.size() % 2 == 0){
      jet->setVariable("medianLog10TrackAngle",trackAngles.at(trackAngles.size()/2 - 1));
    }else{
      jet->setVariable("medianLog10TrackAngle",trackAngles.at((trackAngles.size() - 1)/2));
    }

    auto vars1 = getBoostedVariables(sumVector, trackVectors);

    jet->setVariable("assocAplanarity",get<0>(vars1));
    jet->setVariable("assocSphericity",get<1>(vars1));
    jet->setVariable("assocThrustMajor",get<2>(vars1));
    jet->setVariable("assocThrustMinor",get<3>(vars1));

    jet->setVariable("totalTrackPx",sumVector.Px());
    jet->setVariable("totalTrackPy",sumVector.Py());
    jet->setVariable("totalTrackPz",sumVector.Pz());
    jet->setVariable("totalTrackE",sumVector.E());

    jet->setVariable("totalTrackAngle",totalTrackAngle);
    jet->setVariable("totalTrackPt",totalTrackPt);
    jet->setVariable("totalTrackAnglePt",totalTrackAnglePt);

    jet->setVariable("IVFScore",IVFScore);
    jet->setVariable("nTracksIPlt0p05",nTracksIPlt0p05);
    jet->setVariable("nTracksIPSiggt10",nTracksIPSiggt10);
    jet->setVariable("nTracksIPSiglt5",nTracksIPSiglt5);

    double tenergy = 0, tmass = 0;
    trackEnergyAndMass(tsosList,tenergy,tmass);
    jet->setVariable("trackEnergy",tenergy);
    jet->setVariable("trackMass",tmass);

    jet->setVariable("linearRadialMoment",linearRadialMoment(calo_jet,tsosList));

    double associatedTrackPt = 0;

    double alphaMax,alphaMaxPrime,beta,alphaMax2,alphaMaxPrime2,beta2;
    calculateAlphaMax(transientTracks,vertexVector,alphaMax,alphaMaxPrime,beta,alphaMax2,alphaMaxPrime2,beta2);
    jet->setVariable("alphaMax",alphaMax);
    jet->setVariable("alphaMaxPrime",alphaMaxPrime);
    jet->setVariable("beta",beta);
    jet->setVariable("alphaMax2",alphaMax2);
    jet->setVariable("alphaMaxPrime2",alphaMaxPrime2);
    jet->setVariable("beta2",beta2);
    

    if(transientTracks.size() > 1){
      CachingVertex<5> vert = vertexFitter_->vertex(transientTracks);

      if(vert.isValid()){
        GlobalPoint vertPos = vert.position();
        GlobalError vertErr = vert.error();
          
        float vertBeamXY = vertexBeam_->distanceToBeam(reco::Vertex(RecoVertex::convertPos(vertPos),RecoVertex::convertError(vertErr)));

        jet->setVariable("refitVertexTotalChiSquared",vert.totalChiSquared());
        jet->setVariable("refitVertexDegreesOfFreedom",vert.degreesOfFreedom());
        if(vert.degreesOfFreedom() > 0)jet->setVariable("refitVertexChi2NDoF",vert.totalChiSquared()/vert.degreesOfFreedom());
        jet->setVariable("refitVertexDistanceToBeam",vertBeamXY);
        double rerr = vertErr.rerr(vertPos);
        jet->setVariable("refitVertexTransverseError",rerr);
        if(rerr > 0)jet->setVariable("refitVertexTransverseSig",vertBeamXY/rerr);
        pair<double,double> dvp = deltaVertexPt(vertPos,transientTracks);
        jet->setVariable("refitVertexDeltaEta",dvp.first);
        jet->setVariable("refitVertexDeltaPhi",dvp.second);
      }

      vector<TransientVertex> avfVerts = vtxfitter_->vertices(transientTracks);
      if(avfVerts.size() > 0 && avfVerts[0].isValid()){
        GlobalPoint vertPos = avfVerts[0].position();
        GlobalError vertErr = avfVerts[0].positionError();

        jet->setVariable("avfVx",vertPos.x());
        jet->setVariable("avfVy",vertPos.y());
        jet->setVariable("avfVz",vertPos.z());

        float vertBeamXY = vertexBeam_->distanceToBeam(reco::Vertex(RecoVertex::convertPos(vertPos),RecoVertex::convertError(vertErr)));

        jet->setVariable("avfVertexTotalChiSquared",avfVerts[0].totalChiSquared());
        jet->setVariable("avfVertexDegreesOfFreedom",avfVerts[0].degreesOfFreedom());
        if(avfVerts[0].degreesOfFreedom() > 0)jet->setVariable("avfVertexChi2NDoF",avfVerts[0].totalChiSquared()/avfVerts[0].degreesOfFreedom());
        jet->setVariable("avfVertexDistanceToBeam",vertBeamXY);
        double rerr = vertErr.rerr(vertPos);
        jet->setVariable("avfVertexTransverseError",rerr);
        if(rerr > 0)jet->setVariable("avfVertexTransverseSig",vertBeamXY/rerr);
        /*
        pair<double,double> dvp = deltaVertexPt(vertPos,avfVerts[0].refittedTracks());
        jet->setVariable("avfVertexDeltaEta",dvp.first);
        jet->setVariable("avfVertexDeltaPhi",dvp.second);
        jet->setVariable("avfVertexRecoilPt",sumPTTransVertex(vertPos,avfVerts[0].refittedTracks()));
        jet->setVariable("avfBeamSpotDeltaPhi",deltaBeamspotPt(vertPos,avfVerts[0].refittedTracks()));
        jet->setVariable("avfBeamSpotRecoilPt",sumPTTransBeamspot(vertPos,avfVerts[0].refittedTracks()));
        */

        vector<reco::TransientTrack> cleanTrackColl = cleanTracks(avfVerts[0].refittedTracks(),vertPos);

        double d3deta = 0, d3dphi = 0, d3dpt = 0, d3m = 0, d3en;
        deltaVertex3D(vertPos, cleanTrackColl,d3deta,d3dphi,d3dpt,d3m,d3en);
        jet->setVariable("avfVertexDeltaEta",d3deta);
        jet->setVariable("avfVertexDeltaPhi",d3dphi);
        jet->setVariable("avfVertexRecoilPt",d3dpt);
        jet->setVariable("avfVertexTrackMass",d3m);
        jet->setVariable("avfVertexTrackEnergy",d3en);
        double d2dphi = 0,d2dpt = 0,d2dphiMed=1e-6;
        deltaVertex2D(vertPos,cleanTrackColl,d2dphi,d2dpt,d2dphiMed);
        jet->setVariable("avfBeamSpotDeltaPhi",d2dphi);
        jet->setVariable("avfBeamSpotRecoilPt",d2dpt);
        jet->setVariable("avfBeamSpotMedianDeltaPhi",d2dphiMed);
        jet->setVariable("avfBeamSpotLog10MedianDeltaPhi",log10(d2dphiMed));
        jet->setVariable("nCleanMatchedTracks",(int)cleanTrackColl.size());

        int nHitsInFront = 0;
        int nMissingAfter = 0;
        for(int j = 0; j < (int)cleanTrackColl.size(); j++){
          CheckHitPattern::Result res = checkHitPattern_.analyze(iSetup,cleanTrackColl[j].track(),avfVerts[0].vertexState(),false);
          nHitsInFront += res.hitsInFrontOfVert;
          nMissingAfter += res.missHitsAfterVert;
        }
        jet->setVariable("sumHitsInFrontOfVert",nHitsInFront);
        jet->setVariable("sumMissHitsAfterVert",nMissingAfter);
        jet->setVariable("hitsInFrontOfVertPerTrack",double(nHitsInFront)/double(transientTracks.size()));
        jet->setVariable("missHitsAfterVertPerTrack",double(nMissingAfter)/double(transientTracks.size()));

        jet->setVariable("avfDistToPV",
                 sqrt(pow((vertexHandle_->at(0).x() - avfVerts[0].position().x())/vertexHandle_->at(0).x(),2)
                     +pow((vertexHandle_->at(0).y() - avfVerts[0].position().y())/vertexHandle_->at(0).y(),2)
               +pow((vertexHandle_->at(0).z() - avfVerts[0].position().z())/vertexHandle_->at(0).z(),2)))
              ;
        if(vertexHandle_->size() > 0)jet->setVariable("avfVertexDeltaZtoPV",vertexHandle_->at(0).z() - avfVerts[0].position().z());
        if(vertexHandle_->size() > 1)jet->setVariable("avfVertexDeltaZtoPV2",vertexHandle_->at(1).z() - avfVerts[0].position().z());


        TLorentzVector associatedTrackNetP(0,0,0,0);
        vector<TLorentzVector> associatedTrack4Vecs;

        for(int j = 0; j < (int)trackHandle_->size(); j++){
          reco::TrackBaseRef tref(trackHandle_,j);
          if (tref->pt() < minTrackPtForDiTrack_)continue;
          if (!tref->quality(reco::TrackBase::highPurity)) continue;
          reco::TransientTrack tt(trackHandle_->at(j),magneticField_);
          if(!tt.isValid())continue;
          
          if(tt.trajectoryStateClosestToPoint(vertPos).perigeeError().transverseImpactParameterError() > 0 && tt.trajectoryStateClosestToPoint(vertPos).perigeeParameters().transverseImpactParameter() / tt.trajectoryStateClosestToPoint(vertPos).perigeeError().transverseImpactParameterError() > cleanSignificanceCut_)continue;
          
          associatedTrackPt += tref->pt();
          
          TLorentzVector p(tref->px(),tref->py(),tref->pz(),sqrt(pow(tref->px(),2) + pow(tref->py(),2) + pow(tref->pz(),2) + pow(mPion_,2)));
          associatedTrack4Vecs.push_back(p);
          associatedTrackNetP += p;
        }
        
        //////////////////////////////
        ///add rest frame variables///
        //////////////////////////////
        if(associatedTrack4Vecs.size() != 0){
	  auto vars = getBoostedVariables(associatedTrackNetP,associatedTrack4Vecs);
	  jet->setVariable("avfAssocAplanarity",get<0>(vars));
	  jet->setVariable("avfAssocSphericity",get<1>(vars));
	  jet->setVariable("avfAssocThrustMajor",get<2>(vars));
	  jet->setVariable("avfAssocThrustMinor",get<3>(vars));
        }

      }
      if(associatedTrackPt == 0)associatedTrackPt = totalTrackPt;
      jet->setVariable("associatedTrackPt",associatedTrackPt);

    }
    jets.push_back(jet);
  }
  sort(jets.begin(),jets.end(),SignatureObjectComparison);
  reverse(jets.begin(),jets.end());

  stupakR_ = stupakR/totRpT;
  stupakR2_ = stupakR2/totRpT2;

  m_productmap["CALOJETS"] = jets;

}

void DisplacedAODReader::makeJets()
{

  vector<SignatureObject*> jets;
  int nV = vertexHandle_->size() + secondaryVertexHandle_->size();
  for(int i = 0; i < (int)jetHandle_->size(); i++){
    const pat::Jet& pat_jet = jetHandle_->at(i);

    reco::TrackRefVector tracks = pat_jet.associatedTracks();
    float trackSumPt = 0;
    vector<float> vertexPt(nV,0);
    vector<int> verticesUsed;
    for(int j = 0; j < (int) tracks.size(); j++){
      trackSumPt += (*tracks[j]).pt();
      float minDist = 100000;
      int minV = -1;
      for(int k = 0; k < (int)vertexHandle_->size(); k++){
  float dxy = (*tracks[j]).dxy(vertexHandle_->at(k).position());
  float dz = (*tracks[j]).dz(vertexHandle_->at(k).position());
  float dist = pow(dxy,2)+pow(dz,2);
  if(dist < minDist){
    minDist = dist;
    minV = k;
  }
      }
      for(int k = 0; k < (int)secondaryVertexHandle_->size();k++){
  float dxy = (*tracks[j]).dxy(secondaryVertexHandle_->at(k).position());
  float dz = (*tracks[j]).dz(secondaryVertexHandle_->at(k).position());
  float dist = pow(dxy,2)+pow(dz,2);
  if(dist < minDist){
    minDist = dist;
    minV = vertexHandle_->size() + k;
  }
      }
      if(minV >= 0){
  vertexPt[minV] += (*tracks[j]).pt();
  if(find(verticesUsed.begin(),verticesUsed.end(),minV) == verticesUsed.end())verticesUsed.push_back(minV);
      }
    }

    float maxPt = 0;
    float maxV = -1;
    for(int j = 0; j < (int)verticesUsed.size(); j++){
      if(vertexPt[verticesUsed[j]] > maxPt){
  maxPt = vertexPt[verticesUsed[j]];
  maxV = verticesUsed[j];
      }
    }
    SignatureObject* jet = new SignatureObject(pat_jet.px(),pat_jet.py(),pat_jet.pz(),pat_jet.energy());
    jet->setVariable("INPUTTYPE",TString("jet"));
    jet->setVariable("partonFlavour",pat_jet.partonFlavour());
    //jet->setVariable("hadronFlavour",pat_jet.hadronFlavour());
    jet->setVariable("jecFactorUncorrected",pat_jet.jecFactor("Uncorrected"));
    jet->setVariable("jetBProbabilityBJetTags",pat_jet.bDiscriminator("jetBProbabilityBJetTags"));
    jet->setVariable("jetProbabilityBJetTags",pat_jet.bDiscriminator("jetProbabilityBJetTags"));
    jet->setVariable("trackCountingHighPurBJetTags",pat_jet.bDiscriminator("trackCountingHighPurBJetTags"));
    jet->setVariable("trackCountingHighEffBJetTags",pat_jet.bDiscriminator("trackCountingHighEffBJetTags"));
    jet->setVariable("simpleSecondaryVertexHighEffBJetTags",pat_jet.bDiscriminator("simpleSecondaryVertexHighEffBJetTags"));
    jet->setVariable("simpleSecondaryVertexHighPurBJetTags",pat_jet.bDiscriminator("simpleSecondaryVertexHighPurBJetTags"));
    jet->setVariable("pfCombinedSecondaryVertexBJetTags",pat_jet.bDiscriminator("pfCombinedSecondaryVertexBJetTags"));
    jet->setVariable("combinedInclusiveSecondaryVertexV2BJetTags",pat_jet.bDiscriminator("combinedInclusiveSecondaryVertexV2BJetTags"));
    //jet->setVariable("pileupJetIdfullDiscriminant",pat_jet.userFloat("pileupJetId:fullDiscriminant"));
    //jet->setVariable("vtxMass",pat_jet.userFloat("vtxMass"));
    //jet->setVariable("vtxNtracks",pat_jet.userFloat("vtxNtracks"));
    //jet->setVariable("vtx3DVal",pat_jet.userFloat("vtx3DVal"));
    //jet->setVariable("vtx3DSig",pat_jet.userFloat("vtx3DSig"));
    jet->setVariable("isPFJet",pat_jet.isPFJet());
    jet->setVariable("isBasicJet",pat_jet.isBasicJet());
    jet->setVariable("muonMultiplicity",pat_jet.muonMultiplicity());
    jet->setVariable("chargedMultiplicity",pat_jet.chargedMultiplicity());
    jet->setVariable("chargedEmEnergy",pat_jet.chargedEmEnergy());
    jet->setVariable("neutralEmEnergy",pat_jet.neutralEmEnergy());
    jet->setVariable("chargedHadronEnergy",pat_jet.chargedHadronEnergy());
    jet->setVariable("neutralHadronEnergy",pat_jet.neutralHadronEnergy());
    jet->setVariable("chargedHadronEnergyFraction",pat_jet.chargedHadronEnergyFraction());
    jet->setVariable("neutralHadronEnergyFraction",pat_jet.neutralHadronEnergyFraction());
    jet->setVariable("chargedEmEnergyFraction",pat_jet.chargedEmEnergyFraction());
    jet->setVariable("neutralEmEnergyFraction",pat_jet.neutralEmEnergyFraction());
    jet->setVariable("chargedHadronMultiplicity",pat_jet.chargedHadronMultiplicity());
    jet->setVariable("neutralHadronMultiplicity",pat_jet.neutralHadronMultiplicity());
    jet->setVariable("photonMultiplicity",pat_jet.photonMultiplicity());
    jet->setVariable("electronMultiplicity",pat_jet.electronMultiplicity());
    jet->setVariable("trackPtSum",(double)trackSumPt);
    jet->setVariable("largestVertexPt",maxPt);
    jet->setVariable("largestVertexPtFraction",trackSumPt > 0 ? maxPt / trackSumPt : 0.0);
    jet->setVariable("fromSecondary",maxV >= vertexHandle_->size() ? true : false);
    if(maxV >= 0){
      math::XYZPoint vpos;
      if(maxV < vertexHandle_->size()){
  vpos = vertexHandle_->at(maxV).position();
      }else{
  vpos = secondaryVertexHandle_->at(maxV-vertexHandle_->size()).position();
      }
      jet->setVariable("vx",vpos.x());
      jet->setVariable("vy",vpos.y());
      jet->setVariable("vz",vpos.z());
      jet->setVariable("v0",vpos.rho());
    }

    jets.push_back(jet);
  }

  sort(jets.begin(),jets.end(),SignatureObjectComparison);
  reverse(jets.begin(),jets.end());

  m_productmap["PFJETS"] = jets;


}


void DisplacedAODReader::makeTracks(const edm::EventSetup& iSetup)
{
  vector<SignatureObject*> tracks;

  for(int j = 0; j < (int)trackHandle_->size(); j++){
    reco::TrackBaseRef tref(trackHandle_,j);
    double energy = sqrt(pow(tref->px(),2) + pow(tref->py(),2) + pow(tref->pz(),2) + pow(mPion_,2));
    if (tref->pt() < minTrackPtForDiTrack_)continue;
    if (!tref->quality(reco::TrackBase::highPurity)) continue;
    reco::TransientTrack tt(trackHandle_->at(j),magneticField_);
    if(!tt.isValid())continue;

    SignatureObject* track = new SignatureObject(tref->px(),tref->py(),tref->pz(),energy);
    
    track->setVariable("nMissingInner",tref->hitPattern().numberOfLostTrackerHits(reco::HitPattern::MISSING_INNER_HITS));
    track->setVariable("nMissingOuter",tref->hitPattern().numberOfLostTrackerHits(reco::HitPattern::MISSING_OUTER_HITS));
    
    static GetTrackTrajInfo getTrackTrajInfo; 
    vector<GetTrackTrajInfo::Result> trajInfo = getTrackTrajInfo.analyze(iSetup, (*tref));
    if ( trajInfo.size() > 0 && trajInfo[0].valid) {
      const TrajectoryStateOnSurface& tsosInnerHit = trajInfo[0].detTSOS;
      double ta = fabs(trackAngle(tt,tsosInnerHit));
      track->setVariable("trackAngle",ta);
      track->setVariable("Log10trackAngle",log10(ta));
    }
    
    double dxy = fabs(tref->dxy(*beamspotHandle_));
    double dxyerr = tref->dxyError();
    double dxySig = 0;
    if (dxyerr > 0)dxySig = dxy/dxyerr;

    track->setVariable("dxy",dxy);
    track->setVariable("dxyError",dxyerr);
    track->setVariable("dxySig",dxySig);
    track->setVariable("Log10SumIPSig",log10(dxySig));

    track->setVariable("whichVertex",whichVertex_[j]);
    track->setVariable("whichJet",trackToCaloJetMap_[j]);

    track->setVariable("INPUTTYPE",TString("track"));

    track->setVariable("charge",tref->charge());

    tracks.push_back(track);
  }
  

  sort(tracks.begin(),tracks.end(),SignatureObjectComparison);
  reverse(tracks.begin(),tracks.end());

  m_productmap["ALLTRACKS"] = tracks;

}

void DisplacedAODReader::makeKshorts()
{


  vector<SignatureObject*> kshorts;

  reco::Vertex  pv = vertexHandle_->at(0);

  for(size_t i=0;i< ksHandle_->size();i++){
    const reco::VertexCompositeCandidate & Kscand = ksHandle_->at(i);

    if (Kscand.numberOfDaughters()!=2) continue;
    //if (Kscand.vertexNormalizedChi2()>7) continue;
    
    
     // track quality cuts
    reco::TrackRef trk1 = (*(dynamic_cast<const reco::RecoChargedCandidate *> (Kscand.daughter(0))) ).track();
    reco::TrackRef trk2 = (*(dynamic_cast<const reco::RecoChargedCandidate *> (Kscand.daughter(1))) ).track();
    
    if (trk1->pt() < minTrackPtForDiTrack_ or trk2->pt() < minTrackPtForDiTrack_) continue;
    if (!trk1->quality(reco::TrackBase::highPurity)) continue;
    if (!trk2->quality(reco::TrackBase::highPurity)) continue;

    const GlobalVector v0Momentum(Kscand.px(),Kscand.py(),Kscand.pz());
    // track 3D impact parameter sig
    reco::TransientTrack t_trk1 = theB_->build(trk1);
    reco::TransientTrack t_trk2 = theB_->build(trk2);
    Measurement1D ip3d1 = IPTools::signedImpactParameter3D(t_trk1,v0Momentum,pv).second;
    Measurement1D ip3d2 = IPTools::signedImpactParameter3D(t_trk2,v0Momentum,pv).second;
    Measurement1D ip2d1 = IPTools::signedTransverseImpactParameter(t_trk1,v0Momentum,pv).second;
    Measurement1D ip2d2 = IPTools::signedTransverseImpactParameter(t_trk2,v0Momentum,pv).second;
    // displacement cut
    if ((ip3d1.significance() < minTrack3DSigForDiTrack_) || (ip3d2.significance() < minTrack3DSigForDiTrack_)) continue;
    
    
    // recompute lxy wrt PV
    ROOT::Math::SVector<double,3> v2D(Kscand.vx() - pv.x(),Kscand.vy()-pv.y(),0);
    ROOT::Math::SVector<double,3> v3D(Kscand.vx() - pv.x(),Kscand.vy()-pv.y(),Kscand.vz()-pv.z());
    double lxy = ROOT::Math::Mag(v2D);
    double lxyz = ROOT::Math::Mag(v3D);
    reco::Candidate::CovarianceMatrix matrix = Kscand.vertexCovariance() + pv.covariance();
    float errlxy = sqrt(ROOT::Math::Similarity(matrix,v2D))/lxy;
    float errlxyz = sqrt(ROOT::Math::Similarity(matrix,v3D))/lxyz;
    float lxysig = lxy/errlxy;
    float lxyzsig = lxyz/errlxyz;
    
    // displacement cut
    //if(lxyzsig<5) continue;
    
    //collinearity
    float reducedlxyz= ( 
      Kscand.px()*(Kscand.vx()-pv.x())
      +Kscand.py()*(Kscand.vy()-pv.y())
      +Kscand.pz()*(Kscand.vz()-pv.z())            
       )/(Kscand.p());
    float colin=reducedlxyz/lxyz; 
    
    //Kinematic fit
    KinematicParticleFactoryFromTransientTrack pFactory;
    std::vector<RefCountedKinematicParticle> v0Particles;
    
    float piMass=0.13957018;
    float piSigma=piMass*1.e-6;
    v0Particles.push_back(pFactory.particle(t_trk1, piMass, t_trk1.chi2(), t_trk1.ndof(), piSigma));
    v0Particles.push_back(pFactory.particle(t_trk2, piMass, t_trk2.chi2(), t_trk2.ndof(), piSigma));
    
    KinematicParticleVertexFitter fitter;                  
    RefCountedKinematicTree v0VertexFitTree=fitter.fit(v0Particles);
    if (v0VertexFitTree->isEmpty()) continue;
    RefCountedKinematicParticle v0FitKinematicParticle = v0VertexFitTree->currentParticle();     
    RefCountedKinematicVertex v0FitKinematicVertex = v0VertexFitTree->currentDecayVertex();
    
    KinematicState theCurrentKinematicState = v0FitKinematicParticle->currentState();
    FreeTrajectoryState theV0FTS = theCurrentKinematicState.freeTrajectoryState();
    //Neutral v0 candidate transient tracks
    reco::TransientTrack v0TT = (*theB_).build(theV0FTS);
    
    // v0 IP
    Measurement1D v0IP3d = IPTools::signedImpactParameter3D(v0TT, v0Momentum, pv).second;
    Measurement1D v0IP2d = IPTools::signedTransverseImpactParameter(v0TT, v0Momentum, pv).second;
    
    // collinearity cut
    //if(fabs(v0IP3d.significance())>3) continue;
    SignatureObject* kshort = new SignatureObject(0,0,0,0);
    kshort->SetPtEtaPhiM(Kscand.pt(),Kscand.eta(),Kscand.phi(),Kscand.mass());
    kshort->setVariable("vertexNormalizedChi2",Kscand.vertexNormalizedChi2());
    kshort->setVariable("lxy",lxy);
    kshort->setVariable("lxyz",lxyz);
    kshort->setVariable("lxysig",lxysig);
    kshort->setVariable("lxyzsig",lxyzsig);
    kshort->setVariable("ctau",lxyz*0.497614/Kscand.p());
    kshort->setVariable("trk1Pt",trk1->pt());
    kshort->setVariable("trk2Pt",trk2->pt());
    kshort->setVariable("trk1IP3d",ip3d1.value());
    kshort->setVariable("trk1IP2d",ip2d1.value());
    kshort->setVariable("trk1IP3dSig",ip3d1.significance());
    kshort->setVariable("trk1IP2dSig",ip2d1.significance());
    kshort->setVariable("trk2IP3d",ip3d2.value());
    kshort->setVariable("trk2IP2d",ip2d2.value());
    kshort->setVariable("trk2IP3dSig",ip3d2.significance());
    kshort->setVariable("trk2IP2dSig",ip2d2.significance());
    kshort->setVariable("collinearity",colin);
    kshort->setVariable("ksIP3d",v0IP3d.value());
    kshort->setVariable("ksIP3dSig",v0IP3d.significance());
    kshort->setVariable("ksIP2d",v0IP2d.value());
    kshort->setVariable("ksIP2dSig",v0IP2d.significance());
    kshort->setVariable("INPUTTYPE",TString("kshort"));
    kshorts.push_back(kshort);
  }
  m_productmap["ALLKSHORTS"] = kshorts;
}


void DisplacedAODReader::makeVertices()
{
  vector<SignatureObject*> vertices;
  for(int i = 0; i < (int)vertexHandle_->size(); i++){
    const reco::Vertex& pat_vertex = vertexHandle_->at(i);
    SignatureObject* vertex = new SignatureObject(0,0,0,0);
    vertex->setVariable("x",pat_vertex.x());
    vertex->setVariable("y",pat_vertex.y());
    vertex->setVariable("z",pat_vertex.z());
    vertex->setVariable("isValid",pat_vertex.isValid());
    vertex->setVariable("isFake",pat_vertex.isFake());
    vertex->setVariable("tracksSize",int(pat_vertex.tracksSize()));
    vertex->setVariable("normalizedChi2",pat_vertex.normalizedChi2());
    vertex->setVariable("chi2",pat_vertex.chi2());
    vertex->setVariable("ndof",pat_vertex.ndof());
    vertex->setVariable("rho",pat_vertex.position().rho());
    vertex->setVariable("INPUTTYPE",TString("vertex"));
    vertices.push_back(vertex);
  }

  m_productmap["ALLVERTICES"] = vertices;
}

void DisplacedAODReader::makeBeamSpot()
{
  vector<SignatureObject*> beamspots;
  const reco::BeamSpot& pat_beamspot = (*beamspotHandle_);
  SignatureObject* beamspot = new SignatureObject(0,0,0,0);
  beamspot->setVariable("x0",pat_beamspot.x0());
  beamspot->setVariable("y0",pat_beamspot.y0());
  beamspot->setVariable("z0",pat_beamspot.z0());
  beamspot->setVariable("x0Error",pat_beamspot.x0Error());
  beamspot->setVariable("y0Error",pat_beamspot.y0Error());
  beamspot->setVariable("z0Error",pat_beamspot.z0Error());
  beamspot->setVariable("INPUTTYPE",TString("beamspot"));
  beamspots.push_back(beamspot);

  m_productmap["ALLBEAMSPOTS"] = beamspots;

}

void DisplacedAODReader::makeFilters()
{
  vector<SignatureObject*> filters;

  SignatureObject* filter = new SignatureObject(0,0,0,0);
  filter->setVariable("filterName", TString("HBHENoiseFilter"));
  filter->setVariable("wasrun", true);
  filter->setVariable("accept", *hNoiseHandle_);
  filter->setVariable("INPUTTYPE", TString("filter"));
  filters.push_back(filter);
    
  filter = new SignatureObject(0,0,0,0);
  filter->setVariable("filterName", TString("HBHEIsoNoiseFilter"));
  filter->setVariable("wasrun", true);
  filter->setVariable("accept", *hIsoNoiseHandle_);
  filter->setVariable("INPUTTYPE", TString("filter"));
  filters.push_back(filter);

  m_productmap["ALLFILTERS"] = filters;
}

void DisplacedAODReader::makeMET()
{
  vector<SignatureObject*> mets;
  const pat::MET& pat_met = metHandle_->at(0);
  SignatureObject* met = new SignatureObject(pat_met.px(),pat_met.py(),0,pat_met.pt());
  //met->setVariable("corEx",pat_met.corEx());
  //met->setVariable("corEy",pat_met.corEy());
  //met->setVariable("corSumEt",pat_met.corSumEt());
  met->setVariable("mEtSig",pat_met.mEtSig());
  met->setVariable("sumEt",pat_met.sumEt());
  met->setVariable("significance",pat_met.significance());
  met->setVariable("e_longitudinal",pat_met.e_longitudinal());
  //met->setVariable("uncorrectedPt",pat_met.uncorrectedPt());
  //met->setVariable("uncorrectedPhi",pat_met.uncorrectedPhi());
  met->setVariable("isCaloMET",pat_met.isCaloMET());
  met->setVariable("isPFMET",pat_met.isPFMET());
  met->setVariable("isRecoMET",pat_met.isRecoMET());
  met->setVariable("NeutralEMFraction",pat_met.NeutralEMFraction());
  met->setVariable("NeutralHadEtFraction",pat_met.NeutralHadEtFraction());
  met->setVariable("ChargedEMEtFraction",pat_met.ChargedEMEtFraction());
  met->setVariable("ChargedHadEtFraction",pat_met.ChargedHadEtFraction());
  met->setVariable("INPUTTYPE",TString("met"));
  mets.push_back(met);

  // Obtain and store a few variants based on a few uncertainties, see DataFormats/PatCandidates/interface/MET.h and https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2015#ETmiss
  static const std::map<pat::MET::METCorrectionLevel, TString> levels = {
    {pat::MET::Raw, "Raw"}
    , {pat::MET::Type1, "Type1"}
  };
  static const std::map<pat::MET::METUncertainty, TString> uncertainties = {
    {pat::MET::JetResUp, "JetResUp"}
    , {pat::MET::JetResDown, "JetResDown"}
    , {pat::MET::JetEnUp, "JetEnUp"}
    , {pat::MET::JetEnDown, "JetEnDown"}
    , {pat::MET::MuonEnUp, "MuonEnUp"}
    , {pat::MET::MuonEnDown, "MuonEnDown"}
    , {pat::MET::ElectronEnUp, "ElectronEnUp"}
    , {pat::MET::ElectronEnDown, "ElectronEnDown"}
    , {pat::MET::TauEnUp, "TauEnUp"}
    , {pat::MET::TauEnDown, "TauEnDown"}
    , {pat::MET::UnclusteredEnUp, "UnclusteredEnUp"}
    , {pat::MET::UnclusteredEnDown, "UnclusteredEnDown"}
    , {pat::MET::PhotonEnUp, "PhotonEnUp"}
    , {pat::MET::PhotonEnDown, "PhotonEnDown"}
    , {pat::MET::NoShift, "NoShift"}
    /* // The following are not really uncertainties (causing exceptions) -- just book-keeping inside DataFormats/PatCandidates/src/MET.cc
       , {pat::MET::METUncertaintySize, "METUncertaintySize"}
       , {pat::MET::JetResUpSmear, "JetResUpSmear"}
       , {pat::MET::JetResDownSmear, "JetResDownSmear"}
       , {pat::MET::METFullUncertaintySize, "METFullUncertaintySize"}
    */
  };

/*  
  for(auto &level : levels) {
    for(auto &uncertainty : uncertainties) {
      met = new SignatureObject(pat_met.shiftedPx(uncertainty.first, level.first), pat_met.shiftedPy(uncertainty.first, level.first), 0, pat_met.shiftedPt(uncertainty.first, level.first));
      met->setVariable("level", level.second);
      met->setVariable("uncertainty", uncertainty.second);
      met->setVariable("INPUTTYPE", TString("met"));
      mets.push_back(met);
    }
  }
  */
  m_productmap["ALLMET"].insert(m_productmap["ALLMET"].end(), mets.begin(), mets.end());

}

void DisplacedAODReader::makeHcalNoise()
{
  SignatureObject* hcalnoise = new SignatureObject(0,0,0,0);
  hcalnoise->setVariable("noiseFilterStatus",hcalHandle_->noiseFilterStatus());
  hcalnoise->setVariable("noiseType",hcalHandle_->noiseType());
  hcalnoise->setVariable("numIsolatedNoiseChannels",hcalHandle_->numIsolatedNoiseChannels());
  hcalnoise->setVariable("isolatedNoiseSumE",hcalHandle_->isolatedNoiseSumE());
  hcalnoise->setVariable("isolatedNoiseSumEt",hcalHandle_->isolatedNoiseSumEt());
  hcalnoise->setVariable("INPUTTYPE",TString("hcalnoise"));

  vector<SignatureObject*> hcals;
  hcals.push_back(hcalnoise);
  m_productmap["HCALNOISE"] = hcals;
}

void DisplacedAODReader::makeGenInfo()
{
  lhef::HEPEUP genInfo = genInfoHandle_->hepeup();
  setVariable("SUBPROCESSID",genInfo.IDPRUP);
  setVariable("XWGTUP",genInfo.XWGTUP);
  setVariable("PDFW1",genInfo.XPDWUP.first);
  setVariable("PDFW2",genInfo.XPDWUP.second);
  setVariable("SCALUP",genInfo.SCALUP);
}
void DisplacedAODReader::makeGenParticles()
{
  map<reco::GenParticleRef,int> motherMap;

  vector<SignatureObject*> genParticles;
  for(int i = 0; i < (int)genParticleHandle_->size(); i++){
    const reco::GenParticle& reco_genpart = genParticleHandle_->at(i);
    reco::GenParticleRef ref(genParticleHandle_,i);
    SignatureObject* part = new SignatureObject(reco_genpart.px(),reco_genpart.py(),reco_genpart.pz(),reco_genpart.energy());
    motherMap[ref] = i;
    part->setVariable("index",i);
    part->setVariable("INPUTTYPE",TString("mc"));
    part->setVariable("collisionId",reco_genpart.collisionId());
    part->setVariable("charge",reco_genpart.charge());
    part->setVariable("vx",reco_genpart.vx());
    part->setVariable("vy",reco_genpart.vy());
    part->setVariable("vz",reco_genpart.vz());
    part->setVariable("pdgId",reco_genpart.pdgId());
    part->setVariable("status",reco_genpart.status());
    part->setVariable("numberOfMothers",(int)reco_genpart.numberOfMothers());
    if(reco_genpart.numberOfMothers() > 0){
      part->setVariable("motherpdgId",reco_genpart.mother(0)->pdgId());
      part->setVariable("motherstatus",reco_genpart.mother(0)->status());
      if(motherMap.find(reco_genpart.motherRef()) != motherMap.end()){
  part->setVariable("motherIndex",motherMap[reco_genpart.motherRef()]);
      }
    }
    setDetectorEtaPhi(part);
    genParticles.push_back(part);
  }
  m_productmap["ALLMC"] = genParticles;
}
void DisplacedAODReader::setDetectorEtaPhi(SignatureObject* obj)
{
  double sintheta = sin(obj->Theta());
  double costheta = cos(obj->Theta());
  double sinphi = sin(obj->Phi());
  double cosphi = cos(obj->Phi());
  double vx=0,vy=0,vz=0;
  bool vx_isSet =  obj->getVariable("vx",vx);
  bool vy_isSet =  obj->getVariable("vy",vy);
  bool vz_isSet =  obj->getVariable("vz",vz);

  if(!vx_isSet || !vy_isSet || !vz_isSet)return;

  double ecalRadius = 129;
  double ecalZ = 314;

  double pzp = costheta == 0 ? 1e10 : (ecalZ - vz)/costheta;
  double pzm = costheta == 0 ? 1e10 : (-ecalZ - vz)/costheta;
  float a = sintheta*sintheta;
  float b = 2*sintheta*(vx*cosphi + vy*sinphi);
  float c = vx*vx + vy*vy - ecalRadius*ecalRadius;
  double prp = 1e10;
  double prm = 1e10;
  if(a > 0){
    prp = (-b + sqrt(b*b-4*a*c))/(2*a);
    prm = (-b - sqrt(b*b-4*a*c))/(2*a);
  }
  if(prm < 0)prm = 1e10;
  if(prp < 0)prp = 1e10;
  if(pzp < 0)pzp = 1e10;
  if(pzm < 0)pzm = 1e10;

  double minP = std::min(prp,std::min(prm,std::min(pzp,pzm)));
  if(minP > 1e9)return;
  TVector3 endCoord(vx+minP*sintheta*cosphi,vy+minP*sintheta*sinphi,vz+minP*costheta);
  if(endCoord.Pt() > 1e-6){
    obj->setVariable("etaOnECAL",endCoord.Eta());
    obj->setVariable("phiOnECAL",endCoord.Phi());
  }

}

void DisplacedAODReader::deltaVertex3D(GlobalPoint secVert, std::vector<reco::TransientTrack> tracks, double& dEta, double& dPhi, double& pt, double& m, double& energy)
{
  TVector3 pv(vertexHandle_->at(0).x(),vertexHandle_->at(0).y(),vertexHandle_->at(0).z());
  TVector3 sv(secVert.x(),secVert.y(),secVert.z());
  TVector3 diff = (sv-pv);
  TVector3 trackPt(0,0,0);
  TLorentzVector trackP4(0,0,0,0);
  for(int i = 0; i < (int)tracks.size(); i++){
    TVector3 tt;
    tt.SetPtEtaPhi(tracks[i].trajectoryStateClosestToPoint(secVert).momentum().transverse(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().eta(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().phi());
    trackPt += tt;
    trackP4 += TLorentzVector(tt,tracks[i].trajectoryStateClosestToPoint(secVert).momentum().mag());
  }
  dEta = diff.Eta()-trackPt.Eta();
  dPhi = diff.DeltaPhi(trackPt);
  pt = (trackPt - ((trackPt * diff)/(diff * diff) * diff)).Mag();
  m = trackP4.M();
  energy = trackP4.E();
}

void DisplacedAODReader::deltaVertex2D(GlobalPoint secVert, std::vector<reco::TransientTrack> tracks, double& dPhi, double& pt, double& mediandPhi)
{
  const reco::BeamSpot& pat_beamspot = (*beamspotHandle_);
  TVector2 bmspot(pat_beamspot.x0(),pat_beamspot.y0());
  TVector2 sv(secVert.x(),secVert.y());
  TVector2 diff = (sv-bmspot);
  TVector2 trackPt(0,0);
  vector<double> trackAngles;
  for(int i = 0; i < (int)tracks.size(); i++){
    TVector2 tt;
    tt.SetMagPhi(tracks[i].trajectoryStateClosestToPoint(secVert).momentum().transverse(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().phi());
    trackPt += tt;
    trackAngles.push_back(fabs(diff.DeltaPhi(tt)));
  }
  sort(trackAngles.begin(), trackAngles.end());

  if(trackAngles.size() == 0){
    //do nothing
  }else if((trackAngles.size()%2 == 0)){
    mediandPhi = trackAngles.at(trackAngles.size()/2-1);
  }else{
    mediandPhi = trackAngles.at((trackAngles.size()-1)/2);
  }

  dPhi = diff.DeltaPhi(trackPt);
  pt = (trackPt - ((trackPt * diff)/(diff * diff) * diff)).Mod();

}

pair<double,double> DisplacedAODReader::deltaVertexPt(GlobalPoint secVert, vector<reco::TransientTrack> tracks){
  TVector3 pv(vertexHandle_->at(0).x(),vertexHandle_->at(0).y(),vertexHandle_->at(0).z());
  TVector3 sv(secVert.x(),secVert.y(),secVert.z());
  TVector3 diff = (sv-pv);
  TVector3 trackPt(0,0,0);
  for(int i = 0; i < (int)tracks.size(); i++){
    TVector3 tt;
    tt.SetPtEtaPhi(tracks[i].trajectoryStateClosestToPoint(secVert).momentum().transverse(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().eta(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().phi());
    trackPt += tt;
  }
  return make_pair(diff.Eta()-trackPt.Eta(),diff.DeltaPhi(trackPt));
}


double DisplacedAODReader::sumPTTransVertex(GlobalPoint secVert, vector<reco::TransientTrack> tracks){
  TVector3 pv(vertexHandle_->at(0).x(),vertexHandle_->at(0).y(),vertexHandle_->at(0).z());
  TVector3 sv(secVert.x(),secVert.y(),secVert.z());
  TVector3 diff = (sv-pv);
  TVector3 trackPt(0,0,0);
  for(int i = 0; i < (int)tracks.size(); i++){
    TVector3 tt;
    tt.SetPtEtaPhi(tracks[i].trajectoryStateClosestToPoint(secVert).momentum().transverse(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().eta(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().phi());
    trackPt += tt;
  }
  return (trackPt - ((trackPt * diff)/(diff * diff) * diff)).Mag();
}

double DisplacedAODReader::deltaBeamspotPt(GlobalPoint secVert, vector<reco::TransientTrack> tracks){
  const reco::BeamSpot& pat_beamspot = (*beamspotHandle_);
  TVector2 bmspot(pat_beamspot.x0(),pat_beamspot.y0());
  TVector2 sv(secVert.x(),secVert.y());
  TVector2 diff = (sv-bmspot);
  TVector2 trackPt(0,0);
  for(int i = 0; i < (int)tracks.size(); i++){
    TVector2 tt;
    tt.SetMagPhi(tracks[i].trajectoryStateClosestToPoint(secVert).momentum().transverse(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().phi());
    trackPt += tt;
  }
  return diff.DeltaPhi(trackPt);
}

double DisplacedAODReader::sumPTTransBeamspot(GlobalPoint secVert, vector<reco::TransientTrack> tracks){
  const reco::BeamSpot& pat_beamspot = (*beamspotHandle_);
  TVector2 bmspot(pat_beamspot.x0(),pat_beamspot.y0());
  TVector2 sv(secVert.x(),secVert.y());
  TVector2 diff = (sv-bmspot);
  TVector2 trackPt(0,0);
  for(int i = 0; i < (int)tracks.size(); i++){
    TVector2 tt;
    tt.SetMagPhi(tracks[i].trajectoryStateClosestToPoint(secVert).momentum().transverse(),tracks[i].trajectoryStateClosestToPoint(secVert).momentum().phi());
    trackPt += tt;
  }
  return (trackPt - ((trackPt * diff)/(diff * diff) * diff)).Mod();
}

vector<reco::TransientTrack> DisplacedAODReader::cleanTracks(vector<reco::TransientTrack> tracks, GlobalPoint vertPos)
{
  vector<reco::TransientTrack> cleanTracks;
  for(int i = 0; i < (int)tracks.size(); i++){
    if(tracks[i].trajectoryStateClosestToPoint(vertPos).perigeeError().transverseImpactParameterError() > 0 && tracks[i].trajectoryStateClosestToPoint(vertPos).perigeeParameters().transverseImpactParameter() / tracks[i].trajectoryStateClosestToPoint(vertPos).perigeeError().transverseImpactParameterError() > cleanSignificanceCut_)continue;
    cleanTracks.push_back(tracks[i]);
  }
  return cleanTracks;
}

void DisplacedAODReader::calculateAlphaMax(vector<reco::TransientTrack>tracks,vector<int> whichVertex, double& aMax, double& aMaxP, double& beta, double& aMax2, double& aMaxP2, double& beta2)
{
  double total = 0;
  double total2 = 0;
  double promptTotal = 0;
  double promptTotal2 = 0;
  vector<double> alphas(vertexHandle_->size(),0);
  vector<double> alphas2(vertexHandle_->size(),0);
  for(int i = 0; i < (int)tracks.size(); i++){
    double pt = tracks[i].initialFreeState().momentum().transverse();
    total += pt;
    total2 += pt*pt;
    if(whichVertex[i] < 0)continue;
    promptTotal += pt;
    promptTotal2 += pt*pt;
    alphas[whichVertex[i]] += pt;
    alphas2[whichVertex[i]] += pt*pt;
  }
  double alphaMax = 0;
  double alphaMax2 = 0;
  double apMax =0;
  double apMax2 = 0;
  beta = 1.0 - promptTotal/total;
  beta2 = 1.0 - promptTotal2 / total2;
  for(int i = 0; i < (int)alphas.size(); i++){
    if(alphas[i] > alphaMax) alphaMax = alphas[i];
    if(alphas2[i] > alphaMax2) alphaMax2 = alphas2[i];
    double ap = alphas[i] / (alphas[i] + beta);
    double ap2 = alphas2[i] / (alphas2[i] + beta2);
    if(ap > apMax) apMax = ap;
    if(ap2 > apMax2) apMax2 = ap2;
  }
  aMax = alphaMax / total;
  aMax2 = alphaMax2 / total2;
  aMaxP = apMax;
  aMaxP2 = apMax2;
  return;
}

void DisplacedAODReader::makePileupInfo()
{
  vector<SignatureObject*> puverts;
  if(pileupHandle_->size() < 1)return;
  setVariable("PU_NumInteractions",pileupHandle_->at(0).getPU_NumInteractions());
  //setVariable("BunchCrossing",pileupHandle_->at(0).getBunchCrossing());
  //setVariable("BunchSpacing",pileupHandle_->at(0).getBunchSpacing());
  setVariable("TrueNumInteractions",pileupHandle_->at(0).getTrueNumInteractions());
  for(int i = 0; i < (int)pileupHandle_->size(); i++){
    SignatureObject* puv = new SignatureObject(0,0,0,0);
    puv->setVariable("INPUTTYPE",TString("PUVERTEX"));
    puv->setVariable("BunchCrossing",pileupHandle_->at(i).getBunchCrossing());
    vector<float> zpos =   pileupHandle_->at(i).getPU_zpositions();
    vector<float> sumpt_low =   pileupHandle_->at(i).getPU_sumpT_lowpT();
    vector<float> sumpt_high =   pileupHandle_->at(i).getPU_sumpT_highpT();
    vector<float> instLumi = pileupHandle_->at(i).getPU_instLumi();
    vector<int> ntrks_lowpT = pileupHandle_->at(i).getPU_ntrks_lowpT();
    vector<int> ntrks_highpT = pileupHandle_->at(i).getPU_ntrks_highpT();
    vector<edm::EventID> eventIDs = pileupHandle_->at(i).getPU_EventID();
    if(zpos.size() > 0)puv->setVariable("z",zpos[0]);
    if(sumpt_low.size() > 0)puv->setVariable("sumpT_lowpT",sumpt_low[0]);
    if(sumpt_high.size() > 0)puv->setVariable("sumpT_highpT",sumpt_high[0]);
    if(instLumi.size() > 0)puv->setVariable("instLumi",instLumi[0]);
    if(ntrks_lowpT.size() > 0)puv->setVariable("ntrks_lowpT",ntrks_lowpT[0]);
    if(ntrks_highpT.size() > 0)puv->setVariable("ntrks_highpT",ntrks_highpT[0]);
    if(eventIDs.size() > 0){
      puv->setVariable("run",int(eventIDs[0].run()));
      puv->setVariable("luminosityBlock",int(eventIDs[0].luminosityBlock()));
      puv->setVariable("event",int(eventIDs[0].event()));
    }
    puverts.push_back(puv);    
  }
  m_productmap["ALLPUVERTS"] = puverts;

}


double DisplacedAODReader::trackAngle(reco::TransientTrack track, TrajectoryStateOnSurface tsosInnerHit)
{
  const reco::BeamSpot& pat_beamspot = (*beamspotHandle_);
  TVector2 bmspot(pat_beamspot.x0(),pat_beamspot.y0());
  GlobalPoint   innerPos  = tsosInnerHit.globalPosition();
  GlobalVector innerMom = tsosInnerHit.globalMomentum();
  TVector2 sv(innerPos.x(),innerPos.y());
  TVector2 diff = (sv-bmspot);
  //cout<<"bs x: "<<bmspot.X()<<" y: "<<bmspot.Y()<<endl;
  //cout<<" sv x: "<<sv.X()<<" y: "<<sv.Y()<<endl;
  //cout<<" diff phi: "<<diff.Phi()<<endl;
  TVector2 momentum(innerMom.x(),innerMom.y());
  //cout<<" p x: "<<momentum.X()<<" y: "<<momentum.Y()<<endl;
  //cout<<" p phi: "<<momentum.Phi()<<endl;
  //cout<<" dPhi: "<<diff.DeltaPhi(momentum)<<endl;
  return diff.DeltaPhi(momentum);
}

void DisplacedAODReader::trackEnergyAndMass(std::vector<TrajectoryStateOnSurface> tsosList, double& energy, double& mass)
{
  TLorentzVector trackP4(0,0,0,0);
  for(int i = 0; i < (int)tsosList.size(); i++){
    GlobalVector innerMom = tsosList[0].globalMomentum();
    TLorentzVector tt(innerMom.x(),innerMom.y(),innerMom.z(),innerMom.mag());
    trackP4 += tt;
  }
  mass = trackP4.M();
  energy = trackP4.E();
}

double DisplacedAODReader::linearRadialMoment(reco::CaloJet calo_jet,std::vector<TrajectoryStateOnSurface> tsosList)
{
  double lrm = 0;
  TLorentzVector jetpt(calo_jet.detectorP4().Px(),calo_jet.detectorP4().Py(),calo_jet.detectorP4().Pz(),calo_jet.detectorP4().E());
  for(int i = 0; i < (int) tsosList.size(); i++){
    GlobalVector innerMom = tsosList[0].globalMomentum();
    TLorentzVector tt(innerMom.x(),innerMom.y(),innerMom.z(),innerMom.mag());
    float deltar = jetpt.DeltaR(tt);
    lrm += deltar * tt.Pt()/jetpt.Pt();
  }
  return lrm;
}

tuple<double,double,double,double> DisplacedAODReader::getBoostedVariables(TLorentzVector associatedTrackNetP, vector<TLorentzVector> associatedTrack4Vecs)
{
  TVector3 boostVec = -associatedTrackNetP.BoostVector();
  
  for(auto &track : associatedTrack4Vecs){
    track.Boost(boostVec);
  }
  
  ///sphericity + aplanarity///
  TMatrixDSym sph(3);
  for(int i = 0; i < 3; i++){
    for(int j = i; j < 3; j++){
      for(auto &vec : associatedTrack4Vecs){
	sph(i,j) += vec[i]*vec[j];
	sph(j,i) = sph(i,j);
      }
    }
  }
  TMatrixDSymEigen* sphMat = new TMatrixDSymEigen(sph);
  TVectorD eValues = sphMat->GetEigenValues();
  double L1 = eValues.Max();
  double L3 = eValues.Min();
  double L2 = eValues.Sum() - L1 - L3;
  
  
  ///Thrust///
  double thrust_major = -1;
  double thrust_minor = -1;
  
  if(associatedTrack4Vecs.size() < 4)   return make_tuple(3.*L3/2./eValues.Sum(),3./2.*(L2 + L3)/eValues.Sum(),thrust_major,thrust_minor);
;
  
  TVector3 thrust(0,0,0);
  
  int agree = 0;
  int disagree = 0;
  
  TVector3 n_0[20];
  short add0[20] = { 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1,-1,-1,-1,-1,-1,-1,-1,-1 };
  short add1[20] = { 0, 1, 0, 0, 1, 1, 1, 1,-1,-1,-1,-1, 1, 1, 1, 1,-1,-1,-1,-1 };
  short add2[20] = { 0, 0, 1, 0, 1, 1,-1,-1, 1, 1,-1,-1, 1, 1,-1,-1, 1, 1,-1,-1 };
  short add3[20] = { 0, 0, 0, 1, 1,-1, 1,-1, 1,-1, 1,-1, 1,-1, 1,-1, 1,-1, 1,-1 };
  
  sort(associatedTrack4Vecs.begin(),associatedTrack4Vecs.end(),greater_than_Energy);
  
  
  int n_tests = 0;
  int max_tests = min<int>(20, associatedTrack4Vecs.size());
  do {
    n_0[n_tests]=TVector3(0,0,0);
    n_0[n_tests] +=
      add0[n_tests] * associatedTrack4Vecs[0].Vect() +
      add1[n_tests] * associatedTrack4Vecs[1].Vect() +
      add2[n_tests] * associatedTrack4Vecs[2].Vect() +
      add3[n_tests] * associatedTrack4Vecs[3].Vect()
      ;
    
    if (n_0[n_tests].Mag() > 0) n_0[n_tests] *= 1/n_0[n_tests].Mag();
    
    for(int loop = 0; loop < 10; loop++){
      TVector3 n_1(0,0,0);
      for(auto &track : associatedTrack4Vecs){
	if(track.Px()*n_0[n_tests].Px() + track.Py()*n_0[n_tests].Py() + track.Pz()*n_0[n_tests].Pz() > 0){
	  n_1 += track.Vect();
	}else{
	  n_1 -= track.Vect();
	}
      }
      if(n_1.Mag() > 0) n_1 *= 1/n_1.Mag();
      if((n_0[n_tests] == n_1) || (-n_0[n_tests] == n_1)) break;
      n_0[n_tests] = n_1;
    }
    
    if (n_tests > 0 && (n_0[0] == n_0[n_tests] || n_0[0] == -n_0[n_tests])) agree++;
    if (n_tests > 0 &&  n_0[0] != n_0[n_tests] && n_0[0] != -n_0[n_tests])  disagree++;
    
  }while ((disagree > 0 || agree < 4) && ++n_tests < max_tests);
  
  n_tests = 0;        
  double denominator = 0;
  double numerator_t = 0;
  double numerator_m = 0;
  do {
    denominator = 0;
    numerator_t = 0;
    numerator_m = 0;        
    for(auto &track : associatedTrack4Vecs){      
      numerator_t += fabs((track.Vect()).Dot(n_0[n_tests]));
      numerator_m += ((track.Vect()).Cross(n_0[n_tests])).Mag();
      denominator += (track.Vect()).Mag();
    }
    
    if(denominator < 1e-20) break;
    
    if (numerator_t / denominator > thrust_major) {
      thrust_major = numerator_t / denominator;
      thrust_minor = numerator_m / denominator;
      thrust=n_0[n_tests];
    }
  }while (disagree > 0 && ++n_tests < max_tests);
  
  if(denominator < 1e-20){
    thrust_major = -1;
    thrust_minor = -1;
  }

  return make_tuple(3.*L3/2./eValues.Sum(),3./2.*(L2 + L3)/eValues.Sum(),thrust_major,thrust_minor);
  
}

DEFINE_FWK_MODULE(DisplacedAODReader);
