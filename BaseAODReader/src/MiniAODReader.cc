#include "FWCore/Framework/interface/EventSetup.h"
#include "RutgersAODReader/BaseAODReader/interface/MiniAODReader.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureObject.h"
#include "DataFormats/Provenance/interface/EventID.h"
#include "DataFormats/Provenance/interface/RunLumiEventNumber.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Math/interface/angle.h"
#include <TTree.h>
#include <TFile.h>
#include <assert.h>

using namespace std;
using namespace edm;

MiniAODReader::MiniAODReader(const ParameterSet& pset) : BaseAODReader(pset)
{
  isData_  = pset.getParameter<bool>("isDataTag");

  patPhotonLabel_ = pset.getParameter<InputTag>("patPhotonsInputTag");
  patPhotonToken_ = consumes<edm::View<pat::Photon> >(patPhotonLabel_);
  
  phoLooseIdLabel_ = pset.getParameter<edm::InputTag>("phoLooseIdMap");
  phoLooseIdMapToken_ = consumes<edm::ValueMap<bool> >(phoLooseIdLabel_);
  
  phoMediumIdLabel_ = pset.getParameter<edm::InputTag>("phoMediumIdMap");
  phoMediumIdMapToken_ = consumes<edm::ValueMap<bool> >(phoMediumIdLabel_);
  
  phoTightIdLabel_ = pset.getParameter<edm::InputTag>("phoTightIdMap");
  phoTightIdMapToken_ = consumes<edm::ValueMap<bool> >(phoTightIdLabel_);
  
  phoChargedIsolationLabel_ = pset.getParameter<edm::InputTag>("phoChargedIsolationMap");
  phoChargedIsolationMapToken_ = consumes<edm::ValueMap<float> >(phoChargedIsolationLabel_);
  
  phoNeutralHadronIsolationLabel_ = pset.getParameter<edm::InputTag>("phoNeutralHadronIsolationMap");
  phoNeutralHadronIsolationMapToken_ = consumes<edm::ValueMap<float> >(phoNeutralHadronIsolationLabel_);
  
  phoPhotonIsolationLabel_ = pset.getParameter<edm::InputTag>("phoPhotonIsolationMap");
  phoPhotonIsolationMapToken_ = consumes<edm::ValueMap<float> >(phoPhotonIsolationLabel_);
  
  phoWorstChargedIsolationLabel_ = pset.getParameter<edm::InputTag>("phoWorstChargedIsolationMap");
  phoWorstChargedIsolationMapToken_ = consumes<edm::ValueMap<float> >(phoWorstChargedIsolationLabel_);

  lostTrackLabel_ = pset.getParameter<InputTag>("lostTrackInputTag");
  lostTrackToken_ = consumes<pat::PackedCandidateCollection>(lostTrackLabel_);

  pfCandidateLabel_ = pset.getParameter<InputTag>("pfCandidateInputTag");
  pfCandidateToken_ = consumes<pat::PackedCandidateCollection>(pfCandidateLabel_);
  pfCandMinPt_ = pset.getParameter<double>("pfCandMinPt");

  genParticleLabel_ = pset.getParameter<InputTag>("genParticleInputTag");
  genParticleToken_ = consumes<reco::GenParticleCollection>(genParticleLabel_);

  packedGenLabel_ = pset.getParameter<InputTag>("packedGenParticleInputTag");
  packedGenToken_ = consumes<edm::View<pat::PackedGenParticle> >(packedGenLabel_);

  triggerPrescalesLabel_ = pset.getParameter<InputTag>("triggerPrescalesInputTag");
  triggerPrescalesToken_ = consumes<pat::PackedTriggerPrescales>(triggerPrescalesLabel_);

  hcalLabel_ = pset.getParameter<InputTag>("hcalNoiseInputTag");
  hcalToken_ = consumes<HcalNoiseSummary>(hcalLabel_);
  HBHENoiseFilterResult_         = InputTag("HBHENoiseFilterResultProducer","HBHENoiseFilterResult");
  HBHEIsoNoiseFilterResult_      = InputTag("HBHENoiseFilterResultProducer","HBHEIsoNoiseFilterResult");
  HBHENoiseFilterResultToken_    = consumes<bool>(HBHENoiseFilterResult_);
  HBHEIsoNoiseFilterResultToken_ = consumes<bool>(HBHEIsoNoiseFilterResult_);

  genInfoLabel_ = pset.getParameter<InputTag>("genInfoTag");
  genInfoToken_ = consumes<LHEEventProduct>(genInfoLabel_);

  genEventInfoProductLabel_ = pset.getParameter<InputTag>("genEventInfoProductTag");
  genEventInfoProductToken_ = consumes<GenEventInfoProduct>(genEventInfoProductLabel_);

  rhoAllLabel_ = InputTag("fixedGridRhoFastjetAll");
  if(pset.exists("rhoAll"))rhoAllLabel_ = pset.getParameter<InputTag>("rhoAll");
  rhoAllToken_ = consumes<double>(rhoAllLabel_);

  rhoNeutralLabel_ = InputTag("fixedGridRhoFastjetCentralNeutral");
  if(pset.exists("rhoNeutral"))rhoNeutralLabel_ = pset.getParameter<InputTag>("rhoNeutral");
  rhoNeutralToken_ = consumes<double>(rhoNeutralLabel_);

  pileupLabel_ = InputTag("slimmedAddPileupInfo");
  if(pset.exists("pileup"))pileupLabel_ = pset.getParameter<InputTag>("pileup");
  pileupToken_ = consumes<vector<PileupSummaryInfo> >(pileupLabel_);

  filterLabel_ = InputTag("TriggerResults","","PAT");
  if(pset.exists("filter"))filterLabel_ = pset.getParameter<InputTag>("filter");
  filterToken_ = consumes<edm::TriggerResults>(filterLabel_);

  filterLabel2_ = InputTag("TriggerResults","","RECO");
  filterToken2_ = consumes<edm::TriggerResults>(filterLabel2_);
  
  triggerObjectsLabel_ = InputTag("selectedPatTrigger");
  triggerObjectsToken_ = consumes<pat::TriggerObjectStandAloneCollection>(triggerObjectsLabel_);
  if(pset.exists("triggerObjectHLT")   ) triggerObjectHLTnames     = pset.getParameter<vector<string> >("triggerObjectHLT");
  if(pset.exists("triggerObjectFilter")) triggerObjectFilterLabels = pset.getParameter<vector<string> >("triggerObjectFilter");

  metLabel_ = pset.getParameter<InputTag>("patMETInputTag");
  metName_  = pset.getParameter<std::string>("patMETInputTagName");
  metToken_ = consumes<edm::View<pat::MET> >(metLabel_);

  metLabel2_ = pset.getParameter<InputTag>("patMETInputTag2");
  metName2_  = pset.getParameter<std::string>("patMETInputTagName2");
  metToken2_ = consumes<edm::View<pat::MET> >(metLabel2_);

  vector<string> weightsfnames;
  if(pset.exists("electronMVAweights")) weightsfnames = pset.getParameter<vector<string> >("electronMVAweights");

  vector<string> weightsfiles;
  for(int i = 0; i < (int)weightsfnames.size(); i++){
    weightsfiles.push_back(edm::FileInPath(weightsfnames[i]).fullPath());
  }

  // Eleceton ID: MVA
  eleMediumIdMapLabel_   = pset.getParameter<InputTag>("eleMediumIdMap");
  eleTightIdMapLabel_    = pset.getParameter<InputTag>("eleTightIdMap");
  mvaValuesMapLabel_     = pset.getParameter<InputTag>("mvaValuesMap");
  mvaCategoriesMapLabel_ = pset.getParameter<InputTag>("mvaCategoriesMap");
  eleMediumIdMapToken_   = consumes<edm::ValueMap<bool> >(eleMediumIdMapLabel_);
  eleTightIdMapToken_    = consumes<edm::ValueMap<bool> >(eleTightIdMapLabel_);
  mvaValuesMapToken_     = consumes<edm::ValueMap<float> >(mvaValuesMapLabel_);
  mvaCategoriesMapToken_ = consumes<edm::ValueMap<int> >(mvaCategoriesMapLabel_);

  // Electron ID: Cut Based
  eleVetoIdMapCutBasedLabel_   = pset.getParameter<InputTag>("eleVetoIdMapCutBased");
  eleLooseIdMapCutBasedLabel_  = pset.getParameter<InputTag>("eleLooseIdMapCutBased");
  eleMediumIdMapCutBasedLabel_ = pset.getParameter<InputTag>("eleMediumIdMapCutBased");
  eleTightIdMapCutBasedLabel_  = pset.getParameter<InputTag>("eleTightIdMapCutBased");
  eleVetoIdMapCutBasedToken_   = consumes<edm::ValueMap<bool> >(eleVetoIdMapCutBasedLabel_);
  eleLooseIdMapCutBasedToken_  = consumes<edm::ValueMap<bool> >(eleLooseIdMapCutBasedLabel_);
  eleMediumIdMapCutBasedToken_ = consumes<edm::ValueMap<bool> >(eleMediumIdMapCutBasedLabel_);
  eleTightIdMapCutBasedToken_  = consumes<edm::ValueMap<bool> >(eleTightIdMapCutBasedLabel_);

  // Electron ID: HEEP
  eleHEEPIdMapLabel_ = pset.getParameter<InputTag>("eleHEEPIdMap");
  eleHEEPIdMapToken_ = consumes<edm::ValueMap<bool> >(eleHEEPIdMapLabel_);

  jetR_ = 0.4;
  if(pset.exists("jetR"))jetR_ = pset.getParameter<double>("jetR");

  //Initialize PDF sets:
  if(pset.exists("PDFSets")) PDFSetNames = pset.getParameter<vector<string> >("PDFSets");
  for(unsigned int ipdf=1; ipdf<=PDFSetNames.size(); ++ipdf){
    cout<<"\nINFO: External PDF set no: "<<ipdf<<" :: "<<PDFSetNames.at(ipdf-1)<<"\n"<<endl;
    LHAPDF::initPDFSet( ipdf, PDFSetNames.at(ipdf-1) );
  }

  // Btag Scale Factors and Uncertainties - initialization:
  initializeBtagSFs();

}

MiniAODReader::~MiniAODReader()
{


}

void MiniAODReader::analyze(const Event& event, const EventSetup& eventsetup)
{

  m_currentEntry++;

  clearProducts();
 
  event_ = (long)event.id().event();
  run_ = (int)event.id().run();
  lumi_ = (int)event.luminosityBlock();

  makeProducts(event,eventsetup);

  m_products = m_productmap;

  prepareEvent();
  setVariable("EVENT",event_);
  setVariable("RUN",run_);
  setVariable("LUMI",lumi_);
  if(event.getByToken(rhoAllToken_,rhoAllHandle_))
    setVariable("rhoAll",(*rhoAllHandle_));

  if(event.getByToken(rhoNeutralToken_,rhoNeutralHandle_))
    setVariable("rhoNeutral",(*rhoNeutralHandle_));

  if(event.getByToken(genInfoToken_,genInfoHandle_)){
    makeGenInfo();
    makePDFsInternal();
  }
  
  if(event.getByToken(genEventInfoProductToken_,genEventInfoProductHandle_)){
    makeGenEventInfoProduct();
    makePDFs();
  }
  
  if(event.getByToken(pileupToken_,pileupHandle_))
    makePileupInfo();

  analyzeEvent();

  bool writeEvent = false;
  bool isSet = getVariable("WRITEEVENT",writeEvent);
  writeEvent &= isSet;

  if(writeAllEvents_ || writeEvent)fillTree();

}

void MiniAODReader::makeProducts(const Event& event, const EventSetup& eventsetup)
{

  if(event.getByToken(vertexToken_,vertexHandle_))
    makeVertices();
  else
    assert(0);

  if(event.getByToken(beamspotToken_, beamspotHandle_))
    makeBeamSpot();
  else
    assert(0);

  if(event.getByToken(jetToken_,jetHandle_))
    makeJets();

  if(event.getByToken(lostTrackToken_,lostTrackHandle_) && event.getByToken(pfCandidateToken_,pfCandidateHandle_))
    makeTracks();

  if(event.getByToken(pfCandidateToken_,pfCandidateHandle_))
    makePFCands();

  if(event.getByToken( electronToken_,               electronHandle_               ) &&
     event.getByToken( eleMediumIdMapToken_,         eleMediumIdMapHandle_         ) &&
     event.getByToken( eleTightIdMapToken_,          eleTightIdMapHandle_          ) &&
     event.getByToken( mvaValuesMapToken_,           mvaValuesMapHandle_           ) &&
     event.getByToken( mvaCategoriesMapToken_,       mvaCategoriesMapHandle_       ) &&
     event.getByToken( eleVetoIdMapCutBasedToken_,   eleVetoIdMapCutBasedHandle_   ) &&
     event.getByToken( eleLooseIdMapCutBasedToken_,  eleLooseIdMapCutBasedHandle_  ) &&
     event.getByToken( eleMediumIdMapCutBasedToken_, eleMediumIdMapCutBasedHandle_ ) &&
     event.getByToken( eleTightIdMapCutBasedToken_,  eleTightIdMapCutBasedHandle_  ) &&
     event.getByToken( eleHEEPIdMapToken_,           eleHEEPIdMapHandle_           )  )
    makeElectrons();

  if(event.getByToken(muonToken_,muonHandle_))
    makeMuons();

  if(event.getByToken(patPhotonToken_,patPhotonHandle_) && event.getByToken(phoLooseIdMapToken_ ,loose_id_decisions) && event.getByToken(phoMediumIdMapToken_ ,medium_id_decisions) && event.getByToken(phoTightIdMapToken_ ,tight_id_decisions) && event.getByToken(phoChargedIsolationMapToken_ ,phoChargedIsolationHandle_) && event.getByToken(phoNeutralHadronIsolationMapToken_ ,phoNeutralHadronIsolationHandle_) && event.getByToken(phoPhotonIsolationMapToken_ ,phoPhotonIsolationHandle_) && event.getByToken(phoWorstChargedIsolationMapToken_ ,phoWorstChargedIsolationHandle_))
    makePhotons();

  if(event.getByToken(tauToken_,tauHandle_))
    makeTaus();

  m_productmap["ALLMET"] = vector<SignatureObject*>();
  if(event.getByToken(metToken_,metHandle_))
    makeMET(metName_);
  if(event.getByToken(metToken2_,metHandle_))
    makeMET(metName2_);

  if(event.getByToken(triggerToken_, triggerHandle_) && event.getByToken(triggerPrescalesToken_, triggerPrescalesHandle_) && event.getByToken(triggerObjectsToken_, triggerObjectsHandle_))
    makeTriggers(event,eventsetup);

  if((event.getByToken(filterToken_, filterHandle_) || event.getByToken(filterToken2_, filterHandle_)) && event.getByToken(triggerPrescalesToken_,triggerPrescalesHandle_)) {
    makeFilters(event,eventsetup);
  } else {
    cerr << "Warning: not running makeFilters() -- could not gather necessary inputs" << endl;
  }

  if(event.getByToken(genParticleToken_, genParticleHandle_))
  //if(event.getByToken(packedGenToken_, packedGenHandle_))
    makeGenParticles();

}

void MiniAODReader::makeJets()
{
  bool isdebug = false;

  vector<SignatureObject*> jets;
  for(int i = 0; i < (int)jetHandle_->size(); i++){
    const pat::Jet& pat_jet = jetHandle_->at(i);
    SignatureObject* jet = new SignatureObject(pat_jet.px(),pat_jet.py(),pat_jet.pz(),pat_jet.energy());
    jet->setVariable("INPUTTYPE",TString("jet"));
    // Jet flavor parameters for btag scale factors: https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideBTagMCTools#Jet_flavour_in_PAT
    jet->setVariable("partonFlavour",pat_jet.partonFlavour());
    jet->setVariable("hadronFlavour",pat_jet.hadronFlavour()); //used for btagging SF in Run2
    int genPartonPdgId = 0; //initiated to an invalid pdgId
    if(pat_jet.genParton()!=0) genPartonPdgId = pat_jet.genParton()->pdgId();
    jet->setVariable("genPartonPdgId",genPartonPdgId);
    // ----
    jet->setVariable("jecFactorUncorrected",pat_jet.jecFactor("Uncorrected"));
    jet->setVariable("jetBProbabilityBJetTags",pat_jet.bDiscriminator("jetBProbabilityBJetTags"));
    jet->setVariable("jetProbabilityBJetTags",pat_jet.bDiscriminator("jetProbabilityBJetTags"));
    jet->setVariable("trackCountingHighPurBJetTags",pat_jet.bDiscriminator("trackCountingHighPurBJetTags"));
    jet->setVariable("trackCountingHighEffBJetTags",pat_jet.bDiscriminator("trackCountingHighEffBJetTags"));
    jet->setVariable("simpleSecondaryVertexHighEffBJetTags",pat_jet.bDiscriminator("simpleSecondaryVertexHighEffBJetTags"));
    jet->setVariable("simpleSecondaryVertexHighPurBJetTags",pat_jet.bDiscriminator("simpleSecondaryVertexHighPurBJetTags"));
    jet->setVariable("pfCombinedSecondaryVertexBJetTags",pat_jet.bDiscriminator("pfCombinedSecondaryVertexBJetTags"));
    jet->setVariable("combinedInclusiveSecondaryVertexV2BJetTags",pat_jet.bDiscriminator("combinedInclusiveSecondaryVertexV2BJetTags"));
    jet->setVariable("pfCombinedInclusiveSecondaryVertexV2BJetTags",pat_jet.bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags"));
    jet->setVariable("pfJetBProbabilityBJetTags",pat_jet.bDiscriminator("pfJetBProbabilityBJetTags"));
    jet->setVariable("pfJetProbabilityBJetTags",pat_jet.bDiscriminator("pfJetProbabilityBJetTags"));
    jet->setVariable("pfTrackCountingHighPurBJetTags",pat_jet.bDiscriminator("pfTrackCountingHighPurBJetTags"));
    jet->setVariable("pfTrackCountingHighEffBJetTags",pat_jet.bDiscriminator("pfTrackCountingHighEffBJetTags"));
    jet->setVariable("pfSimpleSecondaryVertexHighEffBJetTags",pat_jet.bDiscriminator("pfSimpleSecondaryVertexHighEffBJetTags"));
    jet->setVariable("pfSimpleSecondaryVertexHighPurBJetTags",pat_jet.bDiscriminator("pfSimpleSecondaryVertexHighPurBJetTags"));
    jet->setVariable("pfCombinedSecondaryVertexV2BJetTags",pat_jet.bDiscriminator("pfCombinedSecondaryVertexV2BJetTags"));
    jet->setVariable("pfCombinedSecondaryVertexSoftLeptonBJetTags",pat_jet.bDiscriminator("pfCombinedSecondaryVertexSoftLeptonBJetTags"));
    jet->setVariable("pfCombinedMVABJetTags",pat_jet.bDiscriminator("pfCombinedMVABJetTags"));
    jet->setVariable("pfCombinedMVAV2BJetTags",pat_jet.bDiscriminator("pfCombinedMVAV2BJetTags"));
    jet->setVariable("pileupJetIdfullDiscriminant",pat_jet.userFloat("pileupJetId:fullDiscriminant"));
    jet->setVariable("vtxMass",pat_jet.userFloat("vtxMass"));
    jet->setVariable("vtxNtracks",pat_jet.userFloat("vtxNtracks"));
    jet->setVariable("vtx3DVal",pat_jet.userFloat("vtx3DVal"));
    jet->setVariable("vtx3DSig",pat_jet.userFloat("vtx3DSig"));
    jet->setVariable("isPFJet",pat_jet.isPFJet());
    jet->setVariable("isBasicJet",pat_jet.isBasicJet());
    jet->setVariable("muonMultiplicity",pat_jet.muonMultiplicity());
    jet->setVariable("neutralMultiplicity",pat_jet.neutralMultiplicity());
    jet->setVariable("chargedMultiplicity",pat_jet.chargedMultiplicity());
    jet->setVariable("chargedEmEnergy",pat_jet.chargedEmEnergy());
    jet->setVariable("neutralEmEnergy",pat_jet.neutralEmEnergy());
    jet->setVariable("chargedHadronEnergy",pat_jet.chargedHadronEnergy());
    jet->setVariable("neutralHadronEnergy",pat_jet.neutralHadronEnergy());
    jet->setVariable("chargedHadronEnergyFraction",pat_jet.chargedHadronEnergyFraction());
    jet->setVariable("neutralHadronEnergyFraction",pat_jet.neutralHadronEnergyFraction());
    jet->setVariable("chargedEmEnergyFraction",pat_jet.chargedEmEnergyFraction());
    jet->setVariable("neutralEmEnergyFraction",pat_jet.neutralEmEnergyFraction());
    jet->setVariable("chargedHadronMultiplicity",pat_jet.chargedHadronMultiplicity());
    jet->setVariable("neutralHadronMultiplicity",pat_jet.neutralHadronMultiplicity());
    jet->setVariable("muonEnergyFraction",pat_jet.muonEnergyFraction());
    jet->setVariable("photonMultiplicity",pat_jet.photonMultiplicity());
    jet->setVariable("electronMultiplicity",pat_jet.electronMultiplicity());
    //jet->setVariable("numberOfConstituents",pat_jet.chargedMultiplicity() + pat_jet.neutralHadronMultiplicity()+pat_jet.photonMultiplicity());
    jet->setVariable("numberOfConstituents",pat_jet.chargedMultiplicity() + pat_jet.neutralMultiplicity());

    // JetToolBox specific variables
    //jet->setVariable("fullDiscriminant",pat_jet.userFloat("AK4PFCHSpileupJetIdEvaluator:fullDiscriminant"));
    //jet->setVariable("cutbasedId",pat_jet.userFloat("AK4PFCHSpileupJetIdEvaluator:cutbasedId"));
    //jet->setVariable("fullId",pat_jet.userFloat("AK4PFCHSpileupJetIdEvaluator:fullId"));
    //jet->setVariable("qgLikelihood",pat_jet.userFloat("QGTaggerAK4PFCHS:qgLikelihood"));
    //jet->setVariable("tau1",pat_jet.userFloat("NjettinessAK4CHS:tau1"));
    //jet->setVariable("tau2",pat_jet.userFloat("NjettinessAK4CHS:tau2"));
    //jet->setVariable("tau3",pat_jet.userFloat("NjettinessAK4CHS:tau3"));

    jet->setVariable("linearRadialMoment",linearRadialMoment(pat_jet));

    // Following  https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookJetEnergyResolution
    float sf = res_sf.getScaleFactor({{JME::Binning::JetEta, pat_jet.eta()}});
    float sf_up = res_sf.getScaleFactor({{JME::Binning::JetEta, pat_jet.eta()}}, Variation::UP);
    float sf_down = res_sf.getScaleFactor({{JME::Binning::JetEta, pat_jet.eta()}}, Variation::DOWN);
    jet->setVariable("JERsf",sf);
    jet->setVariable("JERsfUp",sf_up);
    jet->setVariable("JERsfDown",sf_down);
    
    jecUnc->setJetEta(pat_jet.eta());
    jecUnc->setJetPt(pat_jet.pt()); // here you must use the CORRECTED jet pt
    double unc = jecUnc->getUncertainty(true);
    double jetPtJECup = pat_jet.pt()*(1+unc); // shift = +1(up), or -1(down)
    double jetPtJECdown = pat_jet.pt()*(1-unc); // shift = +1(up), or -1(down)
    jet->setVariable("JECuncUp",jetPtJECup);
    jet->setVariable("JECuncDown",jetPtJECdown);

    if(isdebug)cout<<"jetPtJECup: "<<jetPtJECup<<endl;
    if(isdebug)cout<<"jetPtJECdown: "<<jetPtJECdown<<endl;

    // Btag SFs and Uncertainties: Allowed btagger string inputs are: "CSV", "JP", "cMVA" 
    fillBtagSFs(jet, pat_jet,  "CSV");
    fillBtagSFs(jet, pat_jet,   "JP");
    fillBtagSFs(jet, pat_jet, "cMVA");

    jets.push_back(jet);
  }

  sort(jets.begin(),jets.end(),SignatureObjectComparison);
  reverse(jets.begin(),jets.end());

  m_productmap["ALLJETS"] = jets;

  if(isdebug)cout<<"makeJets runs AOK"<<endl;
}

void MiniAODReader::makeElectrons()
{
  bool isdebug = false;

  const reco::Vertex &PV = vertexHandle_->front();

  vector<SignatureObject*> electrons;
  for(int i = 0; i < (int)electronHandle_->size(); i++){
    const pat::Electron& pat_electron = electronHandle_->at(i);
    SignatureObject* electron = new SignatureObject(pat_electron.px(),pat_electron.py(),pat_electron.pz(),pat_electron.energy());
    electron->setVariable("INPUTTYPE",TString("electron"));
    electron->setVariable("dxy",pat_electron.gsfTrack()->dxy(PV.position()));
    electron->setVariable("dz",pat_electron.gsfTrack()->dz(PV.position()));
    electron->setVariable("charge",pat_electron.charge());
    electron->setVariable("superClustereta",pat_electron.superCluster()->eta());
    electron->setVariable("passConversionVeto",pat_electron.passConversionVeto());
    electron->setVariable("numberOfLostHits",pat_electron.gsfTrack()->hitPattern().numberOfLostTrackerHits(reco::HitPattern::MISSING_INNER_HITS));
    electron->setVariable("sigmaIetaIeta",pat_electron.sigmaIetaIeta());
    electron->setVariable("full5x5_sigmaIetaIeta",pat_electron.full5x5_sigmaIetaIeta());
    electron->setVariable("numberOfHits",pat_electron.gsfTrack()->hitPattern().numberOfTrackerHits(reco::HitPattern::TRACK_HITS));
    electron->setVariable("isPF",(int)(pat_electron.isPF()));
    electron->setVariable("sigmaIetaIphi",pat_electron.sigmaIetaIphi());
    electron->setVariable("full5x5_sigmaIetaIphi",pat_electron.sigmaIetaIphi());
    electron->setVariable("ip3d",pat_electron.ip3d());
    electron->setVariable("dbPV3D",pat_electron.dB(pat::Electron::PV3D));
    electron->setVariable("edbPV3D",pat_electron.edB(pat::Electron::PV3D));
    if (pat_electron.edB(pat::Electron::PV3D) != 0)electron->setVariable("sigPV3D",pat_electron.dB(pat::Electron::PV3D)/pat_electron.edB(pat::Electron::PV3D));
    electron->setVariable("dbPV2D",pat_electron.dB(pat::Electron::PV2D));
    electron->setVariable("edbPV2D",pat_electron.edB(pat::Electron::PV2D));
    if (pat_electron.edB(pat::Electron::PV2D) != 0)electron->setVariable("sigPV2D",pat_electron.dB(pat::Electron::PV2D)/pat_electron.edB(pat::Electron::PV2D));
    electron->setVariable("dbBS2D",pat_electron.dB(pat::Electron::BS2D));
    electron->setVariable("edbBS2D",pat_electron.edB(pat::Electron::BS2D));
    if (pat_electron.edB(pat::Electron::BS2D) != 0)electron->setVariable("sigBS2D",pat_electron.dB(pat::Electron::BS2D)/pat_electron.edB(pat::Electron::BS2D));
    electron->setVariable("dbBS3D",pat_electron.dB(pat::Electron::BS3D));
    electron->setVariable("edbBS3D",pat_electron.edB(pat::Electron::BS3D));
    if (pat_electron.edB(pat::Electron::BS3D) != 0)electron->setVariable("sigBS3D",pat_electron.dB(pat::Electron::BS3D)/pat_electron.edB(pat::Electron::BS3D));
    // Electron Charge Measurements:
    int eleCtfTrackCharge = 0;
    if( pat_electron.closestCtfTrackRef().isNonnull() ) eleCtfTrackCharge=pat_electron.closestCtfTrackRef()->charge();
    electron->setVariable("ctfTrackCharge",eleCtfTrackCharge);
    electron->setVariable("scPixCharge",pat_electron.scPixCharge());
    electron->setVariable("gsfTrackCharge",pat_electron.gsfTrack()->charge());
    electron->setVariable("isGsfCtfScPixChargeConsistent",(int)pat_electron.isGsfCtfScPixChargeConsistent());
    electron->setVariable("isGsfScPixChargeConsistent",(int)pat_electron.isGsfScPixChargeConsistent());
    electron->setVariable("isGsfCtfChargeConsistent",(int)pat_electron.isGsfCtfChargeConsistent());
    if(isdebug){
      cout<<"   pat_electron.charge(): "<<pat_electron.charge()<<endl;
      cout<<"   isGsfScPixChargeCons.: "<<pat_electron.isGsfScPixChargeConsistent()<<" ("<<pat_electron.gsfTrack()->charge()<<":"<<pat_electron.scPixCharge()<<")"<<endl;
      cout<<"isGsfCtfScPixChargeCons.: "<<pat_electron.isGsfCtfScPixChargeConsistent()<<" ("<<pat_electron.gsfTrack()->charge()<<":"<<eleCtfTrackCharge<<":"<<pat_electron.scPixCharge()<<")"<<endl;
    cout<<"       isGsfCtfChargeCons.: "<<pat_electron.isGsfCtfChargeConsistent()<<" ("<<pat_electron.gsfTrack()->charge()<<":"<<eleCtfTrackCharge<<")"<<endl;
    }
    // See section 4.7 of electron reco & ID paper: http://iopscience.iop.org/article/10.1088/1748-0221/10/06/P06005/pdf 
    // Three methods are via the Gaussian Sum Filter track,  Kalman Filter track, and the position of the super-cluster.
    electron->setVariable("hcalOverEcal",pat_electron.hcalOverEcal());
    electron->setVariable("hadronicOverEm",pat_electron.hadronicOverEm());
    electron->setVariable("mva_Isolated",pat_electron.mva_Isolated());
    electron->setVariable("mva_e_pi",pat_electron.mva_e_pi());
    electron->setVariable("sumChargedHadronPt",pat_electron.pfIsolationVariables().sumChargedHadronPt);
    electron->setVariable("sumNeutralHadronEt",pat_electron.pfIsolationVariables().sumNeutralHadronEt);
    electron->setVariable("sumPhotonEt",pat_electron.pfIsolationVariables().sumPhotonEt);
    electron->setVariable("sumChargedParticlePt",pat_electron.pfIsolationVariables().sumChargedParticlePt);
    electron->setVariable("sumNeutralHadronEtHighThreshold",pat_electron.pfIsolationVariables().sumNeutralHadronEtHighThreshold);
    electron->setVariable("sumPhotonEtHighThreshold",pat_electron.pfIsolationVariables().sumPhotonEtHighThreshold);
    electron->setVariable("sumPUPt",pat_electron.pfIsolationVariables().sumPUPt);
    electron->setVariable("deltaEtaSuperClusterTrackAtVtx",pat_electron.deltaEtaSuperClusterTrackAtVtx());
    electron->setVariable("deltaEtaSeedClusterTrackAtCalo",pat_electron.deltaEtaSeedClusterTrackAtCalo());
    electron->setVariable("deltaEtaEleClusterTrackAtCalo",pat_electron.deltaEtaEleClusterTrackAtCalo());
    electron->setVariable("deltaPhiSuperClusterTrackAtVtx",pat_electron.deltaPhiSuperClusterTrackAtVtx());
    electron->setVariable("deltaPhiSeedClusterTrackAtCalo",pat_electron.deltaPhiSeedClusterTrackAtCalo());
    electron->setVariable("deltaPhiEleClusterTrackAtCalo",pat_electron.deltaPhiEleClusterTrackAtCalo());
    //
    // https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2015#Electrons
    // https://github.com/cms-sw/cmssw/blob/CMSSW_8_0_X/PhysicsTools/PatAlgos/python/slimming/miniAOD_tools.py
    // https://github.com/cms-sw/cmssw/blob/CMSSW_8_0_X/DataFormats/PatCandidates/interface/Electron.h
    // For cut-based IDs, the value map has the following meaning: 
    // 0: fails,
    // 1: passes electron ID only, 
    // 2: passes electron Isolation only,  
    // 3: passes electron ID and Isolation only, 
    // 4: passes conversion rejection, 
    // 5: passes conversion rejection and ID, =
    // 6: passes conversion rejection and Isolation, 
    // 7: passes the whole selection.
    // WARNING:
    //          These are not the same as the cut based IDs!
    //          See PhysicsTools/PatAlgos/python/slimming/miniAOD_tools.py  link given above.
    if(pat_electron.isElectronIDAvailable("eidLoose"))
      electron->setVariable("eidLoose",(int)(pat_electron.electronID("eidLoose"))); 
    if(pat_electron.isElectronIDAvailable("eidRobustLoose"))
      electron->setVariable("eidRobustLoose",(int)(pat_electron.electronID("eidRobustLoose")));
    if(pat_electron.isElectronIDAvailable("eidTight"))
      electron->setVariable("eidTight",(int)(pat_electron.electronID("eidTight")));
    if(pat_electron.isElectronIDAvailable("eidRobustTight"))
      electron->setVariable("eidRobustTight",(int)(pat_electron.electronID("eidRobustTight")));
    if(pat_electron.isElectronIDAvailable("eidRobustHighEnergy"))
      electron->setVariable("eidRobustHighEnergy",(int)(pat_electron.electronID("eidRobustHighEnergy")));
    //
    if(pat_electron.isElectronIDAvailable("cutBasedElectronID-Spring15-25ns-V1-standalone-loose"))
      electron->setVariable("cutBasedElectronIDloose",(int)(pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-loose")));
    if(pat_electron.isElectronIDAvailable("cutBasedElectronID-Spring15-25ns-V1-standalone-medium"))
      electron->setVariable("cutBasedElectronIDmedium",(int)(pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-medium")));
    if(pat_electron.isElectronIDAvailable("cutBasedElectronID-Spring15-25ns-V1-standalone-tight"))
      electron->setVariable("cutBasedElectronIDtight",(int)(pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-tight")));
    if(pat_electron.isElectronIDAvailable("cutBasedElectronID-Spring15-25ns-V1-standalone-veto"))
      electron->setVariable("cutBasedElectronIDveto",(int)(pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-veto"))); 
    //
    if(pat_electron.isElectronIDAvailable("mvaEleID-Spring15-25ns-Trig-V1-wp80"))
      electron->setVariable("mvaEleIDTrigwp80",(int)(pat_electron.electronID("mvaEleID-Spring15-25ns-Trig-V1-wp80")));
    if(pat_electron.isElectronIDAvailable("mvaEleID-Spring15-25ns-Trig-V1-wp90"))
      electron->setVariable("mvaEleIDTrigwp80",(int)(pat_electron.electronID("mvaEleID-Spring15-25ns-Trig-V1-wp90")));
    if(pat_electron.isElectronIDAvailable("mvaEleID-Spring15-25ns-nonTrig-V1-wp80"))
      electron->setVariable("mvaEleIDnonTrigwp80",(int)(pat_electron.electronID("mvaEleID-Spring15-25ns-nonTrig-V1-wp80")));
    if(pat_electron.isElectronIDAvailable("mvaEleID-Spring15-25ns-nonTrig-V1-wp90"))
      electron->setVariable("mvaEleIDnonTrigwp90",(int)(pat_electron.electronID("mvaEleID-Spring15-25ns-nonTrig-V1-wp90")));
    //
    if(pat_electron.isElectronIDAvailable("heepElectronID-HEEPV60"))electron->setVariable("eidHEEPV60",(int)(pat_electron.electronID("heepElectronID-HEEPV60")));

    if(isdebug) cout<<"                eidLoose: "<<pat_electron.electronID("eidLoose")<<endl;
    if(isdebug) cout<<"        mvaEleIDTrigwp80: "<<pat_electron.electronID("mvaEleID-Spring15-25ns-Trig-V1-wp80")<<endl;
    if(isdebug) cout<<"cutBasedElectronIDmedium: "<<pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-medium")<<endl;
    if(isdebug) cout<<"              eidHEEPV60: "<<pat_electron.electronID("heepElectronID-HEEPV60")<<endl;
    //
    electron->setVariable("ecalEnergy",pat_electron.ecalEnergy());
    electron->setVariable("ecalE_error",pat_electron.ecalEnergyError());
    electron->setVariable("p_in",pat_electron.trackMomentumAtVtx().R());
    electron->setVariable("trackMomentumError",pat_electron.trackMomentumError());
    if(pat_electron.ecalEnergy() > 0 && pat_electron.trackMomentumAtVtx().R() > 0) {
      electron->setVariable("1oEm1oP",fabs(1/pat_electron.ecalEnergy()-1.0/pat_electron.trackMomentumAtVtx().R()));
    }
    if(pat_electron.correctedEcalEnergy() > 0) {
      electron->setVariable("1oEm1oPcorrected", 1.0/pat_electron.correctedEcalEnergy() - pat_electron.eSuperClusterOverP()/pat_electron.correctedEcalEnergy());
    }
    //Isolation computed from PF Clusters, similar to what done in the HLT, is also available through the methods ele.ecalPFClusterIso() and ele.hcalPFClusterIso():
    electron->setVariable("EcalPFClusterIso", pat_electron.ecalPFClusterIso());
    electron->setVariable("HcalPFClusterIso", pat_electron.hcalPFClusterIso());
    electron->setVariable("TrackIso", pat_electron.dr03TkSumPt());

    //electron->setVariable("MVA",electronMVA_->mvaValue(pat_electron,false));
    setPtRelAndRatio(pat_electron,electron);
    if(fabs(pat_electron.eta()) > 1.479)setMiniIsolation(pat_electron,electron,0,0,0.08,0.015,0.015);
    else setMiniIsolation(pat_electron,electron,0,0,0,0,0);

    const auto el = electronHandle_->ptrAt(i);

    // Electron ID: MVA
    bool isPassMedium = (*eleMediumIdMapHandle_)[el];
    bool isPassTight  = (*eleTightIdMapHandle_)[el];
    float mvaValue    = (*mvaValuesMapHandle_)[el];
    int mvaCategory   = (*mvaCategoriesMapHandle_)[el];
    electron->setVariable("passMediumId",(int)(isPassMedium));
    electron->setVariable("passTightId", (int)(isPassTight));
    electron->setVariable("MVA",mvaValue);
    electron->setVariable("mvaCategory",mvaCategory);

    // Electron ID: Cut Based
    bool isPassCutBasedVeto   = (*eleVetoIdMapCutBasedHandle_)[el];
    bool isPassCutBasedLoose  = (*eleLooseIdMapCutBasedHandle_)[el];
    bool isPassCutBasedMedium = (*eleMediumIdMapCutBasedHandle_)[el];
    bool isPassCutBasedTight  = (*eleTightIdMapCutBasedHandle_)[el];
    electron->setVariable("passCutBasedVetoId",  (int)(isPassCutBasedVeto));
    electron->setVariable("passCutBasedLooseId", (int)(isPassCutBasedLoose));
    electron->setVariable("passCutBasedMediumId",(int)(isPassCutBasedMedium));
    electron->setVariable("passCutBasedTightId", (int)(isPassCutBasedTight));
    
    //cout<<"                                                  eidLoose: "<<pat_electron.electronID("eidLoose")<<endl;
    //cout<<"                                       isPassCutBasedLoose: "<<isPassCutBasedLoose<<endl;
    //cout<<"      cutBasedElectronID-Spring15-25ns-V1-standalone-loose: "<<pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-loose")<<endl;
    if( isPassCutBasedLoose != pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-loose") ) cout<<"****MISMATCH in CUT-BASED-LOOSE-ID!!"<<endl;

    // -- Let's try to reproduce cut-based-ID --
    if(isdebug) cout<<" ElectronCutBasedIDNoIsolation(pat_ele,1) = "<< ElectronCutBasedIDNoIsolation(pat_electron,PV,1)<<" >=* "<<isPassCutBasedVeto  <<" = isPassCutBasedVeto "<<endl;
    if(isdebug) cout<<" ElectronCutBasedIDNoIsolation(pat_ele,2) = "<< ElectronCutBasedIDNoIsolation(pat_electron,PV,2)<<" >=* "<<isPassCutBasedLoose <<" = isPassCutBasedLoose "<<endl;
    if(isdebug) cout<<" ElectronCutBasedIDNoIsolation(pat_ele,3) = "<< ElectronCutBasedIDNoIsolation(pat_electron,PV,3)<<" >=* "<<isPassCutBasedMedium<<" = isPassCutBasedMedium "<<endl;
    if(isdebug) cout<<" ElectronCutBasedIDNoIsolation(pat_ele,4) = "<< ElectronCutBasedIDNoIsolation(pat_electron,PV,4)<<" >=* "<<isPassCutBasedTight <<" = isPassCutBasedTight "<<endl;

    electron->setVariable("passCutBasedVetoIdNoIso",  (int)ElectronCutBasedIDNoIsolation(pat_electron,PV,1));
    electron->setVariable("passCutBasedLooseIdNoIso", (int)ElectronCutBasedIDNoIsolation(pat_electron,PV,2));
    electron->setVariable("passCutBasedMediumIdNoIso",(int)ElectronCutBasedIDNoIsolation(pat_electron,PV,3));
    electron->setVariable("passCutBasedTightIdNoIso", (int)ElectronCutBasedIDNoIsolation(pat_electron,PV,4));
    electron->setVariable("passCutBasedTrigIdIsoVL",  (int)ElectronCutBasedIDNoIsolation(pat_electron,PV,5));//this one DOES HAVE isolation!!

    // Electron ID: HEEP
    bool isPassHEEP = (*eleHEEPIdMapHandle_)[el];
    electron->setVariable("passHEEP",(int)(isPassHEEP));

    if(isdebug) cout<<"isPassCutBasedMedium: "<<isPassCutBasedMedium<<endl;
    if(isdebug) cout<<"          isPassHEEP: "<<isPassHEEP<<endl;

    electrons.push_back(electron);
  }

  sort(electrons.begin(),electrons.end(),SignatureObjectComparison);
  reverse(electrons.begin(),electrons.end());

  m_productmap["ALLELECTRONS"] = electrons;
  if(isdebug) cout<<"makeElectrons run OK"<<endl;
}

void MiniAODReader::makeMuons()
{
  const reco::Vertex &PV = vertexHandle_->front();
  vector<SignatureObject*> muons;
  for(int i = 0; i < (int)muonHandle_->size(); i++){
    const pat::Muon& pat_muon = muonHandle_->at(i);
    SignatureObject* muon = new SignatureObject(pat_muon.px(),pat_muon.py(),pat_muon.pz(),pat_muon.energy());
    muon->setVariable("INPUTTYPE",TString("muon"));
    muon->setVariable("tunePMuonBestTrack",pat_muon.tunePMuonBestTrack()->pt());
    muon->setVariable("charge",pat_muon.charge());
    muon->setVariable("trackIso",pat_muon.trackIso()); // isolationR03().sumPt; // see: https://cmssdt.cern.ch/SDT/doxygen/CMSSW_8_0_10/doc/html/d6/d13/classpat_1_1Muon.html#a23ebb00cde7eee383e170ba416db40f2
    muon->setVariable("ecalIso",pat_muon.ecalIso());
    muon->setVariable("hcalIso",pat_muon.hcalIso());
    muon->setVariable("caloIso",pat_muon.caloIso());
    muon->setVariable("numberOfValidHits",(int)pat_muon.numberOfValidHits());
    if(pat_muon.isGlobalMuon()){
      muon->setVariable("normalizedChi2",pat_muon.globalTrack()->normalizedChi2());
      muon->setVariable("numberOfValidMuonHits",pat_muon.globalTrack()->hitPattern().numberOfValidMuonHits());
      muon->setVariable("trackerLayersWithMeasurement",pat_muon.globalTrack()->hitPattern().trackerLayersWithMeasurement());
    }
    if(!pat_muon.isGlobalMuon() && !pat_muon.isTrackerMuon()){
      muon->setVariable("dxy",pat_muon.muonBestTrack()->dxy(PV.position()));
      muon->setVariable("dz",pat_muon.muonBestTrack()->dz(PV.position()));
      muon->setVariable("numberOfValidPixelHits",pat_muon.muonBestTrack()->hitPattern().numberOfValidPixelHits());
      muon->setVariable("validFraction",pat_muon.muonBestTrack()->validFraction());
      muon->setVariable("ptError",pat_muon.muonBestTrack()->ptError());
    }else{
      muon->setVariable("dxy",pat_muon.innerTrack()->dxy(PV.position()));
      muon->setVariable("dz",pat_muon.innerTrack()->dz(PV.position()));
      muon->setVariable("numberOfValidPixelHits",pat_muon.innerTrack()->hitPattern().numberOfValidPixelHits());
      muon->setVariable("validFraction",pat_muon.innerTrack()->validFraction());
      muon->setVariable("ptError",pat_muon.innerTrack()->ptError());
    }
    muon->setVariable("numberOfChambers",pat_muon.numberOfChambers());
    muon->setVariable("isLooseMuon",(int)(pat_muon.isLooseMuon()));
    muon->setVariable("isMediumMuon",(int)(pat_muon.isMediumMuon()));
    muon->setVariable("isTightMuon",(int)(pat_muon.isTightMuon(PV)));
    muon->setVariable("isSoftMuon",(int)(pat_muon.isSoftMuon(PV)));
    muon->setVariable("isHighPtMuon",(int)(pat_muon.isHighPtMuon(PV)));
    muon->setVariable("numberOfMatches",pat_muon.numberOfMatches());
    muon->setVariable("numberOfMatchedStations",pat_muon.numberOfMatchedStations());
    muon->setVariable("isGlobalMuon",(int)(pat_muon.isGlobalMuon()));
    muon->setVariable("isTrackerMuon",(int)(pat_muon.isTrackerMuon()));
    muon->setVariable("isStandAloneMuon",(int)(pat_muon.isStandAloneMuon()));
    muon->setVariable("isCaloMuon",(int)(pat_muon.isCaloMuon()));
    muon->setVariable("isPFMuon",(int)(pat_muon.isPFMuon()));
    muon->setVariable("isRPCMuon",(int)(pat_muon.isRPCMuon()));
    muon->setVariable("pfIsolationR03sumChargedHadronPt",pat_muon.pfIsolationR03().sumChargedHadronPt);
    muon->setVariable("pfIsolationR03sumChargedParticlePt",pat_muon.pfIsolationR03().sumChargedParticlePt);
    muon->setVariable("pfIsolationR03sumNeutralHadronEt",pat_muon.pfIsolationR03().sumNeutralHadronEt);
    muon->setVariable("pfIsolationR03sumPhotonEt",pat_muon.pfIsolationR03().sumPhotonEt);
    muon->setVariable("pfIsolationR03sumNeutralHadronEtHighThreshold",pat_muon.pfIsolationR03().sumNeutralHadronEtHighThreshold);
    muon->setVariable("pfIsolationR03sumPUPt",pat_muon.pfIsolationR03().sumPUPt);
    muon->setVariable("pfIsolationR03sumPhotonEtHighThreshold",pat_muon.pfIsolationR03().sumPhotonEtHighThreshold);
    muon->setVariable("pfIsolationR04sumChargedHadronPt",pat_muon.pfIsolationR04().sumChargedHadronPt);
    muon->setVariable("pfIsolationR04sumChargedParticlePt",pat_muon.pfIsolationR04().sumChargedParticlePt);
    muon->setVariable("pfIsolationR04sumNeutralHadronEt",pat_muon.pfIsolationR04().sumNeutralHadronEt);
    muon->setVariable("pfIsolationR04sumPhotonEt",pat_muon.pfIsolationR04().sumPhotonEt);
    muon->setVariable("pfIsolationR04sumNeutralHadronEtHighThreshold",pat_muon.pfIsolationR04().sumNeutralHadronEtHighThreshold);
    muon->setVariable("pfIsolationR04sumPUPt",pat_muon.pfIsolationR04().sumPUPt);
    muon->setVariable("pfIsolationR04sumPhotonEtHighThreshold",pat_muon.pfIsolationR04().sumPhotonEtHighThreshold);
    muon->setVariable("trkKink",pat_muon.combinedQuality().trkKink);
    muon->setVariable("chi2LocalPosition",pat_muon.combinedQuality().chi2LocalPosition);
    muon->setVariable("segmentCompatibility",pat_muon.segmentCompatibility());
    muon->setVariable("dbPV3D",pat_muon.dB(pat::Muon::PV3D));
    muon->setVariable("edbPV3D",pat_muon.edB(pat::Muon::PV3D));
    if (pat_muon.edB(pat::Muon::PV3D) != 0)muon->setVariable("sigPV3D",pat_muon.dB(pat::Muon::PV3D)/pat_muon.edB(pat::Muon::PV3D));
    muon->setVariable("dbPV2D",pat_muon.dB(pat::Muon::PV2D));
    muon->setVariable("edbPV2D",pat_muon.edB(pat::Muon::PV2D));
    if (pat_muon.edB(pat::Muon::PV2D) != 0)muon->setVariable("sigPV2D",pat_muon.dB(pat::Muon::PV2D)/pat_muon.edB(pat::Muon::PV2D));
    muon->setVariable("dbBS2D",pat_muon.dB(pat::Muon::BS2D));
    muon->setVariable("edbBS2D",pat_muon.edB(pat::Muon::BS2D));
    if (pat_muon.edB(pat::Muon::BS2D) != 0)muon->setVariable("sigBS2D",pat_muon.dB(pat::Muon::BS2D)/pat_muon.edB(pat::Muon::BS2D));
    muon->setVariable("dbBS3D",pat_muon.dB(pat::Muon::BS3D));
    muon->setVariable("edbBS3D",pat_muon.edB(pat::Muon::BS3D));
    if (pat_muon.edB(pat::Muon::BS3D) != 0)muon->setVariable("sigBS3D",pat_muon.dB(pat::Muon::BS3D)/pat_muon.edB(pat::Muon::BS3D));

    setPtRelAndRatio(pat_muon,muon);
    setMiniIsolation(pat_muon,muon);
    muons.push_back(muon);
  }

  sort(muons.begin(),muons.end(),SignatureObjectComparison);
  reverse(muons.begin(),muons.end());

  m_productmap["ALLMUONS"] = muons;

}

void MiniAODReader::makePhotons()
{
  vector<SignatureObject*> photons;
  for(int i = 0; i < (int)patPhotonHandle_->size(); i++){
    const pat::Photon& pat_photon = patPhotonHandle_->at(i);
    SignatureObject* photon = new SignatureObject(pat_photon.px(),pat_photon.py(),pat_photon.pz(),pat_photon.energy());
    photon->setVariable("INPUTTYPE",TString("photon"));
    photon->setVariable("trackIso",pat_photon.trackIso());
    photon->setVariable("ecalIso",pat_photon.ecalIso());
    photon->setVariable("hcalIso",pat_photon.hcalIso());
    photon->setVariable("caloIso",pat_photon.caloIso());
    //Compare:
    //https://cmssdt.cern.ch/SDT/doxygen/CMSSW_7_4_9/doc/html/d4/d47/classpat_1_1Photon.html#aafa349ac11537162dda38ecab59ad97d
    //https://cmssdt.cern.ch/SDT/doxygen/CMSSW_7_6_3_patch2/doc/html/d4/d47/classpat_1_1Photon.html#a9574a2d3f72bdb88b755f60013e66674
    //photon->setVariable("particleIso",pat_photon.particleIso());
    photon->setVariable("particleIso",pat_photon.patParticleIso());
    photon->setVariable("chargedHadronIso",pat_photon.chargedHadronIso());
    photon->setVariable("neutralHadronIso",pat_photon.neutralHadronIso());
    photon->setVariable("photonIso",pat_photon.photonIso());
    photon->setVariable("puChargedHadronIso",pat_photon.puChargedHadronIso());
    photon->setVariable("hasConversionTracks",pat_photon.hasConversionTracks());
    photon->setVariable("hasPixelSeed",pat_photon.hasPixelSeed());
    photon->setVariable("hadronicOverEm",pat_photon.hadronicOverEm());
    photon->setVariable("hadronicDepth1OverEm",pat_photon.hadronicDepth1OverEm());
    photon->setVariable("hadronicDepth2OverEm",pat_photon.hadronicDepth2OverEm());
    photon->setVariable("hadTowOverEm",pat_photon.hadTowOverEm());
    photon->setVariable("sigmaEtaEta",pat_photon.sigmaEtaEta());
    photon->setVariable("sigmaIetaIeta",pat_photon.sigmaIetaIeta());
    photon->setVariable("e3x3",pat_photon.e3x3());
    photon->setVariable("e5x5",pat_photon.e5x5());
    photon->setVariable("r9",pat_photon.r9());
    photon->setVariable("superClustereta",pat_photon.superCluster()->eta());
    photon->setVariable("full5x5_sigmaIetaIeta",pat_photon.full5x5_sigmaIetaIeta());
    
    const auto pho = patPhotonHandle_->ptrAt(i);
    bool isPassLoose  = (*loose_id_decisions)[pho];
    bool isPassMedium = (*medium_id_decisions)[pho];
    bool isPassTight  = (*tight_id_decisions)[pho];
    photon->setVariable("passLooseId", isPassLoose);
    photon->setVariable("passMediumId", isPassMedium);
    photon->setVariable("passTightId", isPassTight);
    float ChargedIsoValue = (*phoChargedIsolationHandle_)[pho];
    float NeutralHadronIsoValue = (*phoNeutralHadronIsolationHandle_)[pho];
    float PhotonIsoValue = (*phoPhotonIsolationHandle_)[pho];
    float WorstChargedIsoValue = (*phoWorstChargedIsolationHandle_)[pho];
    photon->setVariable("ChargedIsolation", ChargedIsoValue);
    photon->setVariable("NeutralHadronIsolation", NeutralHadronIsoValue);
    photon->setVariable("PhotonIsolation", PhotonIsoValue);
    photon->setVariable("WorstChargedIsolation", WorstChargedIsoValue);

    setPtRelAndRatio(pat_photon,photon);
    setMiniIsolation(pat_photon,photon, 0, 0.0,0.08,0.015,0.015);

    photons.push_back(photon);
  }

  sort(photons.begin(),photons.end(),SignatureObjectComparison);
  reverse(photons.begin(),photons.end());

  m_productmap["ALLPHOTONS"] = photons;

}

void MiniAODReader::makeTaus()
{
  const reco::Vertex &PV = vertexHandle_->front();
  vector<SignatureObject*> taus;
  for(int i = 0; i < (int)tauHandle_->size(); i++){
    const pat::Tau& pat_tau = tauHandle_->at(i);
    //if(!pat_tau.isPFTau())continue;
    SignatureObject* tau = new SignatureObject(pat_tau.px(),pat_tau.py(),pat_tau.pz(),pat_tau.energy());
    tau->setVariable("INPUTTYPE",TString("tau"));
    tau->setVariable("charge",pat_tau.charge());
    tau->setVariable("isPFTau",pat_tau.isPFTau());
    //tau->setVariable("leadPFChargedHadrCandsignedSipt",pat_tau.leadPFChargedHadrCandsignedSipt());
    //tau->setVariable("isolationPFChargedHadrCandsPtSum",pat_tau.isolationPFChargedHadrCandsPtSum());
    //tau->setVariable("isolationPFGammaCandsEtSum",pat_tau.isolationPFGammaCandsEtSum());
    //tau->setVariable("maximumHCALPFClusterEt",pat_tau.maximumHCALPFClusterEt());
    tau->setVariable("dz", ( pat_tau.leadChargedHadrCand()->vz()-PV.z() ) - ( (pat_tau.leadChargedHadrCand()->vx()-PV.x() ) * pat_tau.leadChargedHadrCand()->px() + (pat_tau.leadChargedHadrCand()->vy()-PV.y())* pat_tau.leadChargedHadrCand()->py() ) / pat_tau.leadChargedHadrCand()->pt() * pat_tau.leadChargedHadrCand()->pz() / pat_tau.leadChargedHadrCand()->pt() );
    //tau->setVariable("dxy_Sig",pat_tau.dxy_Sig());
    //tau->setVariable("leadPFCandpt",pat_tau.leadPFCand()->pt());
    //tau->setVariable("leadPFCandpdgId",pat_tau.leadPFCand()->pdgId());
    tau->setVariable("leadChargedHadrCandvx", pat_tau.leadChargedHadrCand()->vx());
    tau->setVariable("leadChargedHadrCandvy", pat_tau.leadChargedHadrCand()->vy());
    tau->setVariable("leadChargedHadrCandvz", pat_tau.leadChargedHadrCand()->vz());
    tau->setVariable("leadChargedHadrCandpt", pat_tau.leadChargedHadrCand()->pt());

    // Variables from https://github.com/cms-sw/cmssw/blob/CMSSW_7_4_X/PhysicsTools/PatAlgos/python/tools/tauTools.py as of 2015-07-22
    // Updated to: https://github.com/cms-sw/cmssw/blob/CMSSW_8_0_X/PhysicsTools/PatAlgos/python/tools/tauTools.py on 2016-05-10

    tau->setVariable("againstElectronMVA6category", int(pat_tau.tauID("againstElectronMVA6category")));

    std::vector<const char*> variables;
    variables.push_back("chargedIsoPtSum");
    variables.push_back("neutralIsoPtSum");
    variables.push_back("puCorrPtSum");
    variables.push_back("neutralIsoPtSumWeight");
    variables.push_back("footprintCorrection");
    variables.push_back("photonPtSumOutsideSignalCone");
    //
    variables.push_back("chargedIsoPtSumdR03");
    variables.push_back("neutralIsoPtSumdR03");
    variables.push_back("neutralIsoPtSumWeightdR03");
    variables.push_back("footprintCorrectiondR03");
    variables.push_back("photonPtSumOutsideSignalConedR03");
    //
    variables.push_back("byCombinedIsolationDeltaBetaCorrRaw3Hits");
    variables.push_back("byIsolationMVArun2v1DBoldDMwLTraw");
    variables.push_back("byIsolationMVArun2v1DBnewDMwLTraw");
    variables.push_back("byIsolationMVArun2v1PWoldDMwLTraw");
    variables.push_back("byIsolationMVArun2v1PWnewDMwLTraw");
    variables.push_back("byIsolationMVArun2v1DBdR03oldDMwLTraw");
    variables.push_back("byIsolationMVArun2v1PWdR03oldDMwLTraw");
    variables.push_back("againstElectronMVA6Raw");
    
    for(const auto &variable : variables) {
      tau->setVariable(variable, pat_tau.tauID(variable));
    }
    
    //https://twiki.cern.ch/twiki/bin/view/CMS/TauIDRecommendation13TeV?rev=11
    std::vector<const char*> discriminators;
    discriminators.push_back("decayModeFindingNewDMs");
    discriminators.push_back("decayModeFinding");
    //
    discriminators.push_back("byPhotonPtSumOutsideSignalCone");
    //
    discriminators.push_back("byLooseCombinedIsolationDeltaBetaCorr3Hits");
    discriminators.push_back("byMediumCombinedIsolationDeltaBetaCorr3Hits");
    discriminators.push_back("byTightCombinedIsolationDeltaBetaCorr3Hits");
    //
    //discriminators.push_back("byLooseCombinedIsolationDeltaBetaCorr3HitsdR03"); //not present in Spring2016miniAODv1 ?
    //discriminators.push_back("byMediumCombinedIsolationDeltaBetaCorr3HitsdR03");
    //discriminators.push_back("byTightCombinedIsolationDeltaBetaCorr3HitsdR03");
    //
    discriminators.push_back("byVLooseIsolationMVArun2v1DBoldDMwLT");
    discriminators.push_back("byLooseIsolationMVArun2v1DBoldDMwLT");
    discriminators.push_back("byMediumIsolationMVArun2v1DBoldDMwLT");
    discriminators.push_back("byTightIsolationMVArun2v1DBoldDMwLT");
    discriminators.push_back("byVTightIsolationMVArun2v1DBoldDMwLT");
    discriminators.push_back("byVVTightIsolationMVArun2v1DBoldDMwLT");
    //
    discriminators.push_back("byVLooseIsolationMVArun2v1DBnewDMwLT");
    discriminators.push_back("byLooseIsolationMVArun2v1DBnewDMwLT");
    discriminators.push_back("byMediumIsolationMVArun2v1DBnewDMwLT");
    discriminators.push_back("byTightIsolationMVArun2v1DBnewDMwLT");
    discriminators.push_back("byVTightIsolationMVArun2v1DBnewDMwLT");
    discriminators.push_back("byVVTightIsolationMVArun2v1DBnewDMwLT");
    //
    discriminators.push_back("byVLooseIsolationMVArun2v1PWoldDMwLT");
    discriminators.push_back("byLooseIsolationMVArun2v1PWoldDMwLT");
    discriminators.push_back("byMediumIsolationMVArun2v1PWoldDMwLT");
    discriminators.push_back("byTightIsolationMVArun2v1PWoldDMwLT");
    discriminators.push_back("byVTightIsolationMVArun2v1PWoldDMwLT");
    discriminators.push_back("byVVTightIsolationMVArun2v1PWoldDMwLT");
    //
    discriminators.push_back("byVLooseIsolationMVArun2v1PWnewDMwLT");
    discriminators.push_back("byLooseIsolationMVArun2v1PWnewDMwLT");
    discriminators.push_back("byMediumIsolationMVArun2v1PWnewDMwLT");
    discriminators.push_back("byTightIsolationMVArun2v1PWnewDMwLT");
    discriminators.push_back("byVTightIsolationMVArun2v1PWnewDMwLT");
    discriminators.push_back("byVVTightIsolationMVArun2v1PWnewDMwLT");
    //
    discriminators.push_back("byVLooseIsolationMVArun2v1DBdR03oldDMwLT");
    discriminators.push_back("byLooseIsolationMVArun2v1DBdR03oldDMwLT");
    discriminators.push_back("byMediumIsolationMVArun2v1DBdR03oldDMwLT");
    discriminators.push_back("byTightIsolationMVArun2v1DBdR03oldDMwLT");
    discriminators.push_back("byVTightIsolationMVArun2v1DBdR03oldDMwLT");
    discriminators.push_back("byVVTightIsolationMVArun2v1DBdR03oldDMwLT");
    //
    discriminators.push_back("byVLooseIsolationMVArun2v1PWdR03oldDMwLT");
    discriminators.push_back("byLooseIsolationMVArun2v1PWdR03oldDMwLT");
    discriminators.push_back("byMediumIsolationMVArun2v1PWdR03oldDMwLT");
    discriminators.push_back("byTightIsolationMVArun2v1PWdR03oldDMwLT");
    discriminators.push_back("byVTightIsolationMVArun2v1PWdR03oldDMwLT");
    discriminators.push_back("byVVTightIsolationMVArun2v1PWdR03oldDMwLT");
    //
    discriminators.push_back("againstElectronVLooseMVA6");
    discriminators.push_back("againstElectronLooseMVA6");
    discriminators.push_back("againstElectronMediumMVA6");
    discriminators.push_back("againstElectronTightMVA6");
    discriminators.push_back("againstElectronVTightMVA6");
    //
    discriminators.push_back("againstMuonLoose3");
    discriminators.push_back("againstMuonTight3");
    //
	
    for(const auto &discriminator : discriminators) {
      if(!(pat_tau.tauID(discriminator) == 0 || pat_tau.tauID(discriminator) == 1)) {
	cerr << discriminator << " has value " << pat_tau.tauID(discriminator) << endl;
	exit(1);
      }
      //cout<<"Tau "<<discriminator<<" bool: "<<bool(pat_tau.tauID(discriminator))<<endl;
      //tau->setVariable(discriminator, bool(pat_tau.tauID(discriminator))); //this is removed in favor of int for better handling in EA.
      tau->setVariable(discriminator, int(pat_tau.tauID(discriminator)));
    }
    
    setPtRelAndRatio(pat_tau,tau);
    setMiniIsolation(pat_tau,tau,0.5,0.01,0.01,0.015,0.015);
    
    taus.push_back(tau);
  }

  sort(taus.begin(),taus.end(),SignatureObjectComparison);
  reverse(taus.begin(),taus.end());

  m_productmap["ALLTAUS"] = taus;

}

void MiniAODReader::makeTracks()
{
  vector<SignatureObject*> tracks;
  for(int i = 0; i < (int)lostTrackHandle_->size(); i++){
    const pat::PackedCandidate pc = lostTrackHandle_->at(i);
    if(pc.charge() == 0)continue;
    const reco::Track& pat_track = pc.pseudoTrack();
    SignatureObject* track = new SignatureObject(pat_track.px(),pat_track.py(),pat_track.pz(),pat_track.p());
    makeSigObjTrack(track,pc);
    track->setVariable("INPUTTYPE",TString("losttrack"));
    setIsolationVariablesForPackedCandidates(track,pc);
    tracks.push_back(track);
  }

  for(int i = 0; i < (int)pfCandidateHandle_->size(); i++){
    const pat::PackedCandidate pc = pfCandidateHandle_->at(i);
    if(pc.charge() == 0)continue;
    const reco::Track& pat_track = pc.pseudoTrack();
    SignatureObject* track = new SignatureObject(pat_track.px(),pat_track.py(),pat_track.pz(),pat_track.p());
    makeSigObjTrack(track,pc);
    track->setVariable("INPUTTYPE",TString("pftrack"));
    setIsolationVariablesForPackedCandidates(track,pc);
    tracks.push_back(track);
  }

  sort(tracks.begin(),tracks.end(),SignatureObjectComparison);
  reverse(tracks.begin(),tracks.end());

  m_productmap["ALLTRACKS"] = tracks;

}

void MiniAODReader::makePFCands()
{
  vector<SignatureObject*> pfcands;
  for(int i = 0; i < (int)pfCandidateHandle_->size(); i++){
    const pat::PackedCandidate pc = pfCandidateHandle_->at(i);
    if( pc.pt()<pfCandMinPt_ ) continue;
    SignatureObject* pfcand = new SignatureObject(pc.px(),pc.py(),pc.pz(),pc.p());
    const reco::VertexRef& pfcandVertexRef = pc.vertexRef();// returns for each packed candidate the associated PV.
    unsigned int pfcandVertexIndex = pfcandVertexRef.index();// index of the associated vertex
    //if(pc.fromPV()==pc.fromPV(0)) cout<<"INCONSISTENCY WARNING::  pc.fromPV()!=pc.fromPV(0)"<<endl;
    pfcand->setVariable("charge", pc.charge());
    pfcand->setVariable("pdgid",  pc.pdgId());//11, 13, 22 for ele/mu/gamma, 211 for charged hadrons, 130 for neutral hadrons, 1 and 2 for hadronic and em particles in HF
    pfcand->setVariable("fromPV", pc.fromPV());
    pfcand->setVariable("fromPVassoc", pc.fromPV(pfcandVertexIndex));
    pfcand->setVariable("PVindex", (int)pfcandVertexIndex);
    pfcand->setVariable("dxy",    pc.dxy());
    pfcand->setVariable("dz",     pc.dz());
    pfcand->setVariable("INPUTTYPE",TString("pfcand"));
    pfcands.push_back(pfcand);
  }

  sort(pfcands.begin(),pfcands.end(),SignatureObjectComparison);
  reverse(pfcands.begin(),pfcands.end());

  m_productmap["ALLPFCANDS"] = pfcands;
}

void MiniAODReader::makeVertices()
{
  vector<SignatureObject*> vertices;
  for(int i = 0; i < (int)vertexHandle_->size(); i++){
    const reco::Vertex& pat_vertex = vertexHandle_->at(i);
    SignatureObject* vertex = new SignatureObject(0,0,0,0);
    vertex->setVariable("x",pat_vertex.x());
    vertex->setVariable("y",pat_vertex.y());
    vertex->setVariable("z",pat_vertex.z());
    vertex->setVariable("isValid",pat_vertex.isValid());
    vertex->setVariable("isFake",pat_vertex.isFake());
    vertex->setVariable("tracksSize",int(pat_vertex.tracksSize()));
    vertex->setVariable("normalizedChi2",pat_vertex.normalizedChi2());
    vertex->setVariable("chi2",pat_vertex.chi2());
    vertex->setVariable("ndof",pat_vertex.ndof());
    vertex->setVariable("rho",pat_vertex.position().rho());
    vertex->setVariable("INPUTTYPE",TString("vertex"));
    vertices.push_back(vertex);
  }

  m_productmap["ALLVERTICES"] = vertices;
}

void MiniAODReader::makeBeamSpot()
{
  vector<SignatureObject*> beamspots;
  const reco::BeamSpot& pat_beamspot = (*beamspotHandle_);
  SignatureObject* beamspot = new SignatureObject(0,0,0,0);
  beamspot->setVariable("x0",pat_beamspot.x0());
  beamspot->setVariable("y0",pat_beamspot.y0());
  beamspot->setVariable("z0",pat_beamspot.z0());
  beamspot->setVariable("x0Error",pat_beamspot.x0Error());
  beamspot->setVariable("y0Error",pat_beamspot.y0Error());
  beamspot->setVariable("z0Error",pat_beamspot.z0Error());
  beamspot->setVariable("INPUTTYPE",TString("beamspot"));
  beamspots.push_back(beamspot);

  m_productmap["ALLBEAMSPOTS"] = beamspots;

}

void MiniAODReader::makeTriggers(const edm::Event& event, const edm::EventSetup& eventsetup)
{
  bool debugTrig=false;
  vector<SignatureObject*> triggers;
  const edm::TriggerNames &names = event.triggerNames(*triggerHandle_);
  for(int i = 0; i < (int)triggerHandle_->size(); i++){
    string trigName = names.triggerName(i);
    int prescale = triggerPrescalesHandle_->getPrescaleForIndex(i);
    SignatureObject* trigger = new SignatureObject(0,0,0,0);
    trigger->setVariable("triggerName",TString(trigName));
    trigger->setVariable("PRESCALE",prescale);
    trigger->setVariable("wasrun",triggerHandle_->wasrun(i));
    trigger->setVariable("accept",triggerHandle_->accept(i));
    trigger->setVariable("INPUTTYPE",TString("trigger"));
    //if( trigName.find("HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Mu50_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_TkMu50_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_IsoMu22_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_IsoTkMu22_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_DZ_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Ele27_WPTight_Gsf_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ_v") == std::string::npos ) continue;//debugging
    //if( trigName.find("HLT_Mu30_TkMu11_v") == std::string::npos ) continue;//debugging
    //
    if(debugTrig) std::cout<<"Trigger name/decision: "<<trigName<<" / "<<triggerHandle_->accept(i)<<std::endl;
    triggers.push_back(trigger);
  }

  m_productmap["ALLTRIGGERS"] = triggers;

  //trigger objects
  // Templates from: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2015#Trigger
  vector<SignatureObject*> triggerobjects;
  bool debugTrigObj=false;
  bool debugTrigObj2=false;
  //
  if( triggerObjectHLTnames.size()>0 || triggerObjectFilterLabels.size()>0 ){
    // Loop over all TriggerObjects
    for (pat::TriggerObjectStandAlone obj : *triggerObjectsHandle_) {
      obj.unpackPathNames(names);
      std::vector<std::string> pathNamesAll  = obj.pathNames(false);
      //
      // Trig objects search loop over associated filter labels - used for mixed flavor multilepton triggers
      for( unsigned ihlt = 0, hltnamesize = triggerObjectFilterLabels.size(); ihlt<hltnamesize; ++ihlt){//input
	for (unsigned h = 0; h < obj.filterLabels().size(); ++h){//Loop over HLT filter labels associated with the trigger object
	  std::string currentFilterName     = obj.filterLabels()[h];
	  std::string currentCollectionName = obj.collection();
	  std::string muonCollectionName    = "hltL3MuonCandidates::HLT";
	  std::string muonCollectionName2   = "hltHighPtTkMuonCands::HLT";
	  std::string muonCollectionName3   = "hltGlbTrkMuonCands::HLT";
	  std::string eleCollectionName     = "hltEgammaCandidates::HLT";
	  std::string hltname               = triggerObjectFilterLabels[ihlt];
	  //
	  //if( currentFilterName.compare(hltname) == 0  && (currentFilterName.compare("hltMu23TrkIsoVVLEle12CaloIdLTrackIdLIsoVLMuonlegL3IsoFiltered23") == 0 ||
	  //    currentFilterName.compare("hltMu23TrkIsoVVLEle12CaloIdLTrackIdLIsoVLElectronlegTrackIsoFilter")==0) ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && (currentFilterName.compare("hltMu23TrkIsoVVLEle8CaloIdLTrackIdLIsoVLMuonlegL3IsoFiltered23") == 0 ||
	  //    currentFilterName.compare("hltMu23TrkIsoVVLEle8CaloIdLTrackIdLIsoVLElectronlegTrackIsoFilter")==0) ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && (currentFilterName.compare("hltMu8TrkIsoVVLEle23CaloIdLTrackIdLIsoVLMuonlegL3IsoFiltered8") == 0 ||
	  //						   currentFilterName.compare("hltMu8TrkIsoVVLEle23CaloIdLTrackIdLIsoVLElectronlegTrackIsoFilter")==0) ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltL3fL1sMu22Or25L1f0L2f10QL3Filtered50Q") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltL3fL1sMu25f0TkFiltered50Q") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltL3crIsoL1sMu20L1f0L2f10QL3f22QL3trkIsoFiltered0p09") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltL3fL1sMu20L1f0Tkf22QL3trkIsoFiltered0p09") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltDiMuonGlb17Glb8RelTrkIsoFiltered0p4") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltDiMuonGlb17Glb8RelTrkIsoFiltered0p4DzFiltered0p2") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltDiMuonGlb17Trk8RelTrkIsoFiltered0p4") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltDiMuonGlb17Trk8RelTrkIsoFiltered0p4DzFiltered0p2") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && currentFilterName.compare("hltEle27WPTightGsfTrackIsoFilter") == 0 ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && ( currentFilterName.compare("hltEle23Ele12CaloIdLTrackIdLIsoVLDZFilter") == 0 ||
	  //			  			    currentFilterName.compare("hltEle23Ele12CaloIdLTrackIdLIsoVLTrackIsoLeg1Filter") == 0  ||
	  //						    currentFilterName.compare("hltEle23Ele12CaloIdLTrackIdLIsoVLTrackIsoLeg2Filter") == 0 ) ){//debugging
	  //if( currentFilterName.compare(hltname) == 0  && ( currentFilterName.compare("hltDiMuonGlb30Trk11DzFiltered0p2") == 0 || currentFilterName.compare("hltDiMuonGlbFiltered30TrkFiltered11") == 0 ) ){//debugging
	  //
	  if( currentFilterName.compare(hltname) == 0  &&  
	      ( currentCollectionName.compare(muonCollectionName)  == 0 || currentCollectionName.compare(eleCollectionName)   == 0 ||
		currentCollectionName.compare(muonCollectionName2) == 0 || currentCollectionName.compare(muonCollectionName3) == 0  )
	      ){ 
            if(debugTrigObj) std::cout << "Trigger object:  pt " << obj.pt() << ", eta " << obj.eta() << ", phi " << obj.phi() << std::endl;
            if(debugTrigObj) std::cout << "   Collection: " << obj.collection() << std::endl;
            if(debugTrigObj) std::cout<<"Trig object fires trigger: "<<currentFilterName<<std::endl;
            if(debugTrigObj) std::cout<<"TrigObj HLT path name input: "<<hltname<<std::endl;
            if(debugTrigObj) std::cout << std::endl;
            SignatureObject* triggerobject = new SignatureObject(obj.px(),obj.py(),obj.pz(),obj.p());
            triggerobject->setVariable("INPUTTYPE",TString("triggerobject"));
            triggerobject->setVariable("HLTPATH",TString(hltname));
            triggerobjects.push_back(triggerobject);
          }
        }
      }
      //
      /*
      // Trig objects search loop over associated HLT path names
      for( unsigned ihlt = 0, hltnamesize = triggerObjectHLTnames.size(); ihlt<hltnamesize; ++ihlt){//input
	for (unsigned h = 0, n = pathNamesAll.size(); h < n; ++h) {//Loop over HLT path names associated with the trigger object
	  bool isBoth = obj.hasPathName( pathNamesAll[h], true, true );
	  bool isLF   = obj.hasPathName( pathNamesAll[h], true, false );
          string currentPathName=pathNamesAll[h];
          string hltname=triggerObjectHLTnames[ihlt];
          if( currentPathName.find(hltname) != std::string::npos && (isBoth || isLF) ){
            if(debugTrigObj2) std::cout <<"Trigger object:  pt " << obj.pt() << ", eta " << obj.eta() << ", phi " << obj.phi() << std::endl;
            if(debugTrigObj2) std::cout <<"   Collection: " << obj.collection() << std::endl;
            if(debugTrigObj2) std::cout<<"Trig object fires trigger: "<<currentPathName<<std::endl;
            if(debugTrigObj2) std::cout<<"TrigObj HLT path name input: "<<hltname<<std::endl;
            if(debugTrigObj2) std::cout << std::endl;
            SignatureObject* triggerobject = new SignatureObject(obj.px(),obj.py(),obj.pz(),obj.p());
            triggerobject->setVariable("INPUTTYPE",TString("triggerobject"));
            triggerobject->setVariable("HLTPATH",TString(hltname));
            triggerobjects.push_back(triggerobject);
          }
        }
      }
      */
    }
    if(debugTrigObj) std::cout << std::endl;
  }
  
  sort(triggerobjects.begin(),triggerobjects.end(),SignatureObjectComparison);
  reverse(triggerobjects.begin(),triggerobjects.end());
  
  m_productmap["ALLTRIGGEROBJECTS"] = triggerobjects;
      
}

void MiniAODReader::makeFilters(const edm::Event& event, const edm::EventSetup& eventsetup) {
	vector<SignatureObject*> filters;
	const edm::TriggerNames &names = event.triggerNames(*filterHandle_);
	SignatureObject* filter;
	for(int i = 0; i < (int)filterHandle_->size(); i++){
		string trigName = names.triggerName(i);
		filter = new SignatureObject(0,0,0,0);
		filter->setVariable("filterName", TString(trigName));
		filter->setVariable("wasrun", filterHandle_->wasrun(i));
		filter->setVariable("accept", filterHandle_->accept(i));
		filter->setVariable("INPUTTYPE", TString("filter"));
		filters.push_back(filter);
	}
	
    // https://twiki.cern.ch/twiki/bin/view/CMS/HCALNoiseFilterRecipe
    if(event.getByToken(hcalToken_,hcalHandle_)) {
        edm::Handle<bool> hNoiseResult;
        event.getByToken(HBHENoiseFilterResultToken_, hNoiseResult);

        filter = new SignatureObject(0,0,0,0);
        filter->setVariable("filterName", TString("HBHENoiseFilter"));
        filter->setVariable("wasrun", true);
        filter->setVariable("accept", *hNoiseResult);
        filter->setVariable("INPUTTYPE", TString("filter"));
        filters.push_back(filter);

        edm::Handle<bool> hIsoNoiseResult;
        event.getByToken(HBHEIsoNoiseFilterResultToken_, hIsoNoiseResult);

        filter = new SignatureObject(0,0,0,0);
        filter->setVariable("filterName", TString("HBHEIsoNoiseFilter"));
        filter->setVariable("wasrun", true);
        filter->setVariable("accept", *hIsoNoiseResult);
        filter->setVariable("INPUTTYPE", TString("filter"));
                filters.push_back(filter);
    }
	
	m_productmap["ALLFILTERS"] = filters;
}

void MiniAODReader::makeGenParticles()
{
  map<reco::GenParticleRef,int> motherMap;

  vector<SignatureObject*> genParticles;
  //for(int i = 0; i < (int)packedGenHandle_->size();i++){
  for(int i = 0; i < (int)genParticleHandle_->size();i++){
    //const pat::PackedGenParticle& reco_genpart = packedGenHandle_->at(i);
    const reco::GenParticle& reco_genpart = genParticleHandle_->at(i);
    reco::GenParticleRef ref(genParticleHandle_,i);
    //cout<<"  "<<i<<" "<<reco_genpart.pdgId()<<" "<<reco_genpart.status()<<" "<<reco_genpart.numberOfMothers()<<endl;
    SignatureObject* part = new SignatureObject(reco_genpart.px(),reco_genpart.py(),reco_genpart.pz(),reco_genpart.energy());
    motherMap[ref] = i;
    part->setVariable("index",i);
    part->setVariable("INPUTTYPE",TString("mc"));
    part->setVariable("collisionId",reco_genpart.collisionId());
    part->setVariable("charge",reco_genpart.charge());
    part->setVariable("vx",reco_genpart.vx());
    part->setVariable("vy",reco_genpart.vy());
    part->setVariable("vz",reco_genpart.vz());
    part->setVariable("pdgId",reco_genpart.pdgId());
    part->setVariable("status",reco_genpart.status());
    part->setVariable("numberOfMothers",(int)reco_genpart.numberOfMothers());
    if(reco_genpart.numberOfMothers() > 0){
      part->setVariable("motherpdgId",reco_genpart.mother(0)->pdgId());
      part->setVariable("motherstatus",reco_genpart.mother(0)->status());
      if(motherMap.find(reco_genpart.motherRef()) != motherMap.end()){
        part->setVariable("motherIndex",motherMap[reco_genpart.motherRef()]);
      }
    }
    part->setVariable("isPromptFinalState",reco_genpart.isPromptFinalState());
    part->setVariable("isDirectPromptTauDecayProductFinalState",reco_genpart.isDirectPromptTauDecayProductFinalState());
    //part->setVariable("isPrompt",reco_genpart.isPrompt());
    //part->setVariable("isDecayedLeptonHadron",reco_genpart.isDecayedLeptonHadron());
    //part->setVariable("isTauDecayProduct",reco_genpart.isTauDecayProduct());
    //part->setVariable("isPromptTauDecayProduct",reco_genpart.isPromptTauDecayProduct());
    //part->setVariable("isDirectPromptTauDecayProduct",reco_genpart.isDirectPromptTauDecayProduct());
    //part->setVariable("isDirectHadronDecayProduct",reco_genpart.isDirectHadronDecayProduct());
    part->setVariable("isLastCopy",reco_genpart.isLastCopy());
    genParticles.push_back(part);
  }
  m_productmap["ALLMC"] = genParticles;
}

void MiniAODReader::makeMET(TString inputTag) {
	assert(metHandle_->size() == 1);

	bool isdebug=false;
	
	vector<SignatureObject*> mets;
	
	// Take care about MET itself
	// https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2015#ETmiss
	const pat::MET& pat_met = metHandle_->at(0);
	SignatureObject* met = new SignatureObject(pat_met.px(),pat_met.py(),0,pat_met.pt());
	//met->setVariable("corEx",pat_met.corEx());
	//met->setVariable("corEy",pat_met.corEy());
	//met->setVariable("corSumEt",pat_met.corSumEt());
	met->setVariable("mEtSig",pat_met.mEtSig());
	met->setVariable("sumEt",pat_met.sumEt());
	met->setVariable("significance",pat_met.significance());
	met->setVariable("e_longitudinal",pat_met.e_longitudinal());
        met->setVariable("uncorrectedPt",pat_met.uncorPt());//   RAW PFMET
        met->setVariable("uncorrectedPhi",pat_met.uncorPhi());// RAW PFMET
	met->setVariable("isCaloMET",pat_met.isCaloMET());
	met->setVariable("isPFMET",pat_met.isPFMET());
	met->setVariable("isRecoMET",pat_met.isRecoMET());
	//
	if(pat_met.isCaloMET()){
	  met->setVariable("caloMETPt",pat_met.caloMETPt());
	  met->setVariable("caloMETPhi",pat_met.caloMETPhi());
	  met->setVariable("caloMETSumEt",pat_met.caloMETSumEt());
	}else if(isdebug) cout<<"WARNING: Missing Calo met from "<<inputTag<<endl;
	//
	if(pat_met.isPFMET()){
	  met->setVariable("NeutralEMFraction",pat_met.NeutralEMFraction());
	  met->setVariable("NeutralHadEtFraction",pat_met.NeutralHadEtFraction());
	  met->setVariable("ChargedEMEtFraction",pat_met.ChargedEMEtFraction());
	  met->setVariable("ChargedHadEtFraction",pat_met.ChargedHadEtFraction());
	}else if(isdebug) cout<<"WARNING: Missing PF met from "<<inputTag<<endl;
	//
	met->setVariable("INPUTTYPE",TString("met"));
	met->setVariable("INPUTTAG", inputTag);
	mets.push_back(met);
	
	// Obtain and store a few variants based on a few uncertainties: 
	//    See DataFormats/PatCandidates/interface/MET.h and https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2015#ETmiss
	static const std::map<pat::MET::METCorrectionLevel, TString> levels = {
		  {pat::MET::Raw, "Raw"}
		, {pat::MET::Type1, "Type1"}
	};
	static const std::map<pat::MET::METUncertainty, TString> uncertainties = {
		  {pat::MET::JetResUp, "JetResUp"}
		, {pat::MET::JetResDown, "JetResDown"}
		, {pat::MET::JetEnUp, "JetEnUp"}
		, {pat::MET::JetEnDown, "JetEnDown"}
		, {pat::MET::MuonEnUp, "MuonEnUp"}
		, {pat::MET::MuonEnDown, "MuonEnDown"}
		, {pat::MET::ElectronEnUp, "ElectronEnUp"}
		, {pat::MET::ElectronEnDown, "ElectronEnDown"}
		, {pat::MET::TauEnUp, "TauEnUp"}
		, {pat::MET::TauEnDown, "TauEnDown"}
		, {pat::MET::UnclusteredEnUp, "UnclusteredEnUp"}
		, {pat::MET::UnclusteredEnDown, "UnclusteredEnDown"}
		, {pat::MET::PhotonEnUp, "PhotonEnUp"}
		, {pat::MET::PhotonEnDown, "PhotonEnDown"}
		, {pat::MET::NoShift, "NoShift"}
		/* // The following are not really uncertainties (causing exceptions) -- just book-keeping inside DataFormats/PatCandidates/src/MET.cc
		, {pat::MET::METUncertaintySize, "METUncertaintySize"}
		, {pat::MET::JetResUpSmear, "JetResUpSmear"}
		, {pat::MET::JetResDownSmear, "JetResDownSmear"}
		, {pat::MET::METFullUncertaintySize, "METFullUncertaintySize"}
		*/
	};
	
	for(auto &level : levels) {
		for(auto &uncertainty : uncertainties) {
			met = new SignatureObject(pat_met.shiftedPx(uncertainty.first, level.first), pat_met.shiftedPy(uncertainty.first, level.first), 0, pat_met.shiftedPt(uncertainty.first, level.first));
			met->setVariable("level", level.second);
			met->setVariable("uncertainty", uncertainty.second);
			met->setVariable("INPUTTYPE", TString("met"));
			met->setVariable("INPUTTAG", inputTag);
			mets.push_back(met);
			if(isdebug){
			  TVector2 metvec(pat_met.shiftedPx(uncertainty.first, level.first), pat_met.shiftedPy(uncertainty.first, level.first));
			  cout<<inputTag<<"- "<<level.second<<"/"<<uncertainty.second<<" : "<<metvec.Mod()<<endl;;
			}
		}
	}
	
	m_productmap["ALLMET"].insert(m_productmap["ALLMET"].end(), mets.begin(), mets.end());

	if(isdebug) cout<<"Run MakeMET AOK: "<<inputTag<<" MET: "<<pat_met.pt()<<endl;
}


void MiniAODReader::makePileupInfo()
{
  vector<SignatureObject*> puverts;
  if(pileupHandle_->size() < 1)return;
  setVariable("PU_NumInteractions",pileupHandle_->at(0).getPU_NumInteractions());
  //setVariable("BunchCrossing",pileupHandle_->at(0).getBunchCrossing());
  //setVariable("BunchSpacing",pileupHandle_->at(0).getBunchSpacing());
  setVariable("TrueNumInteractions",pileupHandle_->at(0).getTrueNumInteractions());
  for(int i = 0; i < (int)pileupHandle_->size(); i++){
    SignatureObject* puv = new SignatureObject(0,0,0,0);
    puv->setVariable("INPUTTYPE",TString("PUVERTEX"));
    puv->setVariable("BunchCrossing",pileupHandle_->at(i).getBunchCrossing());
    vector<float> zpos =   pileupHandle_->at(i).getPU_zpositions();
    vector<float> sumpt_low =   pileupHandle_->at(i).getPU_sumpT_lowpT();
    vector<float> sumpt_high =   pileupHandle_->at(i).getPU_sumpT_highpT();
    vector<float> instLumi = pileupHandle_->at(i).getPU_instLumi();
    vector<int> ntrks_lowpT = pileupHandle_->at(i).getPU_ntrks_lowpT();
    vector<int> ntrks_highpT = pileupHandle_->at(i).getPU_ntrks_highpT();
    vector<edm::EventID> eventIDs = pileupHandle_->at(i).getPU_EventID();
    if(zpos.size() > 0)puv->setVariable("z",zpos[0]);
    if(sumpt_low.size() > 0)puv->setVariable("sumpT_lowpT",sumpt_low[0]);
    if(sumpt_high.size() > 0)puv->setVariable("sumpT_highpT",sumpt_high[0]);
    if(instLumi.size() > 0)puv->setVariable("instLumi",instLumi[0]);
    if(ntrks_lowpT.size() > 0)puv->setVariable("ntrks_lowpT",ntrks_lowpT[0]);
    if(ntrks_highpT.size() > 0)puv->setVariable("ntrks_highpT",ntrks_highpT[0]);
    if(eventIDs.size() > 0){
      puv->setVariable("run",int(eventIDs[0].run()));
      puv->setVariable("luminosityBlock",int(eventIDs[0].luminosityBlock()));
      puv->setVariable("event",int(eventIDs[0].event()));
    }
    puverts.push_back(puv);    
  }
  m_productmap["ALLPUVERTS"] = puverts;

}

void MiniAODReader::setIsolationVariablesForPackedCandidates(SignatureObject* sigObj,pat::PackedCandidate pc)
{
  double chargedHad = 0;
  double neutralHad = 0;
  double neutralPho = 0;
  double pileup = 0;
  for (unsigned int i = 0, n = pfCandidateHandle_->size(); i < n; ++i) {
    const pat::PackedCandidate &pf = (*pfCandidateHandle_)[i];
    if(deltaR(pf,pc)<1e-6 && pf.charge() == pc.charge() && fabs(pf.pt()-pc.pt()) < 1e-6 && pf.pdgId() == pc.pdgId())continue;
    //cout<<i<<" "<<deltaR(pf,pc)<<" "<<pf.charge()<<" "<<pf.pt()<<" "<<pf.fromPV()<<" "<<pf.pdgId()<<" "<<endl;
    if (deltaR(pf,pc) < 0.3) {
      if (pf.charge() == 0) {
	if (pf.pt() > 0.5){
	  if(pf.pdgId() == 22) neutralPho += pf.pt();
	  else neutralHad += pf.pt();
	}
      } else if (pf.fromPV() >= 2) {
	if(abs(pf.pdgId()) == 211)chargedHad += pf.pt();
      } else {
	if (pf.pt() > 0.5) pileup += pf.pt();
      }
    }
  }
  sigObj->setVariable("chargedHadronIsoFromPF",chargedHad);
  sigObj->setVariable("neutralHadronIsoFromPF",neutralHad);
  sigObj->setVariable("neutralPhotonIsoFromPF",neutralPho);
  sigObj->setVariable("betaIsoFromPF",pileup);
}

void MiniAODReader::makeGenInfo()
{
  lhef::HEPEUP genInfo = genInfoHandle_->hepeup();
  setVariable("SUBPROCESSID",genInfo.IDPRUP);
  setVariable("XWGTUP",genInfo.XWGTUP);
  setVariable("PDFW1",genInfo.XPDWUP.first);
  setVariable("PDFW2",genInfo.XPDWUP.second);
  setVariable("SCALUP",genInfo.SCALUP);
}

void MiniAODReader::makeGenEventInfoProduct()
{
  //GenEventInfoProduct
  setVariable("genEventInfo_weight",             genEventInfoProductHandle_->weight());
  setVariable("genEventInfo_weightProduct",      genEventInfoProductHandle_->weightProduct());
  setVariable("genEventInfo_signalProcessID",    (int)genEventInfoProductHandle_->signalProcessID());
  setVariable("genEventInfo_qScale",             genEventInfoProductHandle_->qScale());
  setVariable("genEventInfo_alphaQCD",           genEventInfoProductHandle_->alphaQCD());
  setVariable("genEventInfo_alphaQED",           genEventInfoProductHandle_->alphaQED());
  setVariable("genEventInfo_nMEPartons",         genEventInfoProductHandle_->nMEPartons());
  setVariable("genEventInfo_nMEPartonsFiltered", genEventInfoProductHandle_->nMEPartonsFiltered());
  setVariable("genEventInfo_Q",                  genEventInfoProductHandle_->pdf()->scalePDF);
  setVariable("genEventInfo_id1",                genEventInfoProductHandle_->pdf()->id.first);
  setVariable("genEventInfo_x1",                 genEventInfoProductHandle_->pdf()->x.first);
  setVariable("genEventInfo_pdf1",               genEventInfoProductHandle_->pdf()->xPDF.first);
  setVariable("genEventInfo_id2",                genEventInfoProductHandle_->pdf()->id.second);
  setVariable("genEventInfo_x2",                 genEventInfoProductHandle_->pdf()->x.second);
  setVariable("genEventInfo_pdf2",               genEventInfoProductHandle_->pdf()->xPDF.second);
}

void MiniAODReader::makePDFsInternal(){
  bool isdebug = false;
  //
  // In general, does not work for pure Pythia8 samples!
  // Reading & storing weights stored in the LHE event content:
  //   https://twiki.cern.ch/twiki/bin/viewauth/CMS/LHEReaderCMSSW#How_to_use_weights
  //
  vector<SignatureObject*> InternalPDFWeights;
  for (unsigned int i=0; i < genInfoHandle_->weights().size() ; i++){
    double internalweight =  genInfoHandle_->weights()[i].wgt/genInfoHandle_->originalXWGTUP();
    SignatureObject* interalpdfweight = new SignatureObject(0,0,0,0);//empty sigobject.
    interalpdfweight->setVariable("IdNumber",genInfoHandle_->weights()[i].id);
    interalpdfweight->setVariable("PDFweight",internalweight);
    InternalPDFWeights.push_back(interalpdfweight);
    //
    if(isdebug) cout<<"WeightNo:("<<i<<") WeightID:("<< genInfoHandle_->weights()[i].id<<") "<< internalweight <<endl;
    //
    // IMPORTANT NOTE:
    // First 9 weights are renormalization (mu_R) and the factorization (mu_F) scale variations.
    // Other weights are various PDF set weights, already normalized wrt the set used during the MC generation.
    // A list of these various sets are provided here (slide 4), but need to be verified for each sample: 
    //    https://indico.cern.ch/event/377812/session/2/contribution/11/attachments/753599/1033778/PDF4LHC_April_2015.pdf
    // ---------------------------------------------------
    // <weight id="1"> mur=1   muf=1   </weight> nominal
    // <weight id="2"> mur=1   muf=2   </weight> 
    // <weight id="3"> mur=1   muf=0.5 </weight> 
    // <weight id="4"> mur=2   muf=1   </weight> 
    // <weight id="5"> mur=2   muf=2   </weight> 
    // <weight id="6"> mur=2   muf=0.5 </weight> 
    // <weight id="7"> mur=0.5 muf=1   </weight> 
    // <weight id="8"> mur=0.5 muf=2   </weight> 
    // <weight id="9"> mur=0.5 muf=0.5 </weight> 
    // ---------------------------------------------------
  }

  m_productmap["INTERNALPDFWEIGHTS"] = InternalPDFWeights;
}

void MiniAODReader::makePDFs(){
  bool isdebug = false;
  //
  float  Q    = genEventInfoProductHandle_->pdf()->scalePDF;
  int    id1  = genEventInfoProductHandle_->pdf()->id.first;
  double x1   = genEventInfoProductHandle_->pdf()->x.first;
  double pdf1 = genEventInfoProductHandle_->pdf()->xPDF.first;
  int    id2  = genEventInfoProductHandle_->pdf()->id.second;
  double x2   = genEventInfoProductHandle_->pdf()->x.second;
  double pdf2 = genEventInfoProductHandle_->pdf()->xPDF.second;
  double weig = genEventInfoProductHandle_->weight();
  if(isdebug) cout<<"   Q: "<< Q    <<endl;
  if(isdebug) cout<<" id1: "<< id1  <<endl;
  if(isdebug) cout<<"  x1: "<< x1   <<endl;
  if(isdebug) cout<<"pdf1: "<< pdf1 <<endl;
  if(isdebug) cout<<" id2: "<< id2  <<endl;
  if(isdebug) cout<<"  x2: "<< x2   <<endl;
  if(isdebug) cout<<"pdf2: "<< pdf2 <<endl;
  if(isdebug) cout<<"weig: "<< weig <<endl;
  //https://hypernews.cern.ch/HyperNews/CMS/get/generators/1939/1/1/1/1/1/1/1/1/1.html
  //A bit weird that this is happening in pythia8 LQ samples, but it solves the id=21 problem.
  if( id1==21 )  id1=0;
  if( id2==21 )  id2=0;

  //Loop over PDF sets:
  PDFSetWeights.clear();
  for(unsigned int ipdf=1; ipdf<=PDFSetNames.size(); ++ipdf){
    vector<double> singlePDFSetWeights;
    singlePDFSetWeights.clear();
    //
    LHAPDF::usePDFMember(ipdf,0);
    const double xpdf1 = LHAPDF::xfx(1, x1, (double)(Q), id1);
    const double xpdf2 = LHAPDF::xfx(1, x2, (double)(Q), id2);
    double w0 = xpdf1 * xpdf2;
    if(isdebug) cout<<PDFSetNames.at(ipdf-1)<<endl;
    if(isdebug) cout<<"xpdf1: "<< xpdf1 <<endl;
    if(isdebug) cout<<"xpdf2: "<< xpdf2 <<endl;
    if(isdebug) cout<<"   w0: "<< w0    <<endl;
    //
    //variation over central value
    if(pdf2*pdf1/w0==1.0) cout<<"variation over central value is ONE "<<endl;
    if(isdebug) cout<<"variation over central value: "<<pdf2*pdf1/w0<<endl;
    singlePDFSetWeights.push_back(pdf2*pdf1/w0);
    //Loop over members of a given PDF set:
    for(int i=1; i <= LHAPDF::numberPDF(ipdf); ++i){
      LHAPDF::usePDFMember(ipdf,i);
      const double xpdf1_new = LHAPDF::xfx(1, x1, Q, id1);
      const double xpdf2_new = LHAPDF::xfx(1, x2, Q, id2);
      double weight = xpdf1_new * xpdf2_new / w0;
      if(isdebug) cout<<" xpdf1_new: "<< xpdf1_new <<endl;
      if(isdebug) cout<<" xpdf2_new: "<< xpdf2_new <<endl;
      if(isdebug) cout<<"PDF weight: "<< weight    <<endl;
      singlePDFSetWeights.push_back(weight);
    }
    PDFSetWeights.push_back(singlePDFSetWeights);
  }

  //produces weight vectors for each PDF set.
  vector<SignatureObject*> PDFWeights;
  //
  vector<vector<double> >::iterator pdfset_iter;
  vector<double>::iterator pdfweight_iter;
  unsigned int ipdf=0;
  for(pdfset_iter = PDFSetWeights.begin();  pdfset_iter != PDFSetWeights.end();  pdfset_iter++) {
    for(pdfweight_iter = pdfset_iter->begin(); pdfweight_iter != pdfset_iter->end(); pdfweight_iter++) {
      SignatureObject* pdfweight = new SignatureObject(0,0,0,0);//empty sigobject.
      pdfweight->setVariable("PDFName",PDFSetNames.at(ipdf));
      pdfweight->setVariable("PDFweight",(*pdfweight_iter));
      PDFWeights.push_back(pdfweight);
    }
    ipdf++;
  }

  m_productmap["EXTERNALPDFWEIGHTS"] = PDFWeights;
}

double MiniAODReader::linearRadialMoment(pat::Jet jet)
{
  vector<reco::CandidatePtr> constituents(jet.daughterPtrVector());
  double lrm = 0;
  LorentzVector jetpt = jet.p4();
  for(int i = 0; i < (int) constituents.size(); i++){
    const pat::PackedCandidate &cand = dynamic_cast<const pat::PackedCandidate &>(*constituents[i]);
    LorentzVector contstp4 = cand.p4();
    float deltar = reco::deltaR<LorentzVector,LorentzVector>(jetpt,contstp4);
    lrm += deltar * contstp4.pt()/jetpt.pt();
  }
  return lrm;
}

void MiniAODReader::makeSigObjTrack(SignatureObject* track, pat::PackedCandidate pc)
{
  const reco::Track& pat_track = pc.pseudoTrack();
  track->setVariable("charge",pc.charge());
  track->setVariable("fromPV",pc.fromPV());
  track->setVariable("dxy",pc.dxy());
  track->setVariable("dz",pc.dz());
  track->setVariable("lostInnerHits",pc.lostInnerHits());
  track->setVariable("numberOfHits",pc.numberOfHits());
  track->setVariable("numberOfPixelHits",pc.numberOfPixelHits());
  track->setVariable("trackerExpectedHitsInner",pat_track.hitPattern().numberOfHits(reco::HitPattern::TRACK_HITS));
  track->setVariable("normalizedChi2",pat_track.normalizedChi2());
  track->setVariable("trackHighPurity",pc.trackHighPurity());
  setPtRelAndRatio(pc,track);
  setMiniIsolation(pc,track);
  return;
}

void MiniAODReader::setMiniIsolation(const reco::Candidate& pc, SignatureObject* obj, double ptThresh, double vetoConeNeutralHadron,double vetoConePhoton, double vetoConeChargedHadron, double vetoConePU)
{
  double cone = 0.2;
  if(pc.pt() > 200){
    cone = 0.05;
  }else if(pc.pt() > 50){
    cone = 10.0 / pc.pt();
  }
  double chargedHad = 0;
  double neutralHad = 0;
  double neutralPho = 0;
  double pileup = 0;

  //cout<<cone<<" "<<pc.pt()<<endl;

  for (unsigned int i = 0, n = pfCandidateHandle_->size(); i < n; ++i) {
    const pat::PackedCandidate &pf = (*pfCandidateHandle_)[i];
    double dr = deltaR(pf,pc);
    if(dr > cone)continue;
    //cout<<i<<" "<<deltaR(pf,pc)<<" "<<pf.charge()<<" "<<pf.pt()<<" "<<pf.fromPV()<<" "<<pf.pdgId()<<" "<<endl;
    if (pf.charge() == 0) {
      if (pf.pt() > ptThresh){
	if(pf.pdgId() == 22){
	  if(dr < vetoConePhoton)continue;
	  neutralPho += pf.pt();
	}else{
	  if(dr < vetoConeNeutralHadron)continue;
	  neutralHad += pf.pt();
	}
      }
    } else if (pf.fromPV() >= 2) {
      if(abs(pf.pdgId()) == 211){
	if(dr < vetoConeChargedHadron) continue;
	chargedHad += pf.pt();
      }
    } else {
      if (pf.pt() > ptThresh && dr > vetoConePU) pileup += pf.pt();
    }
  }

  obj->setVariable("chargedHadronMiniIso",chargedHad);
  obj->setVariable("neutralHadronMiniIso",neutralHad);
  obj->setVariable("photonMiniIso",neutralPho);
  obj->setVariable("betaMiniIso",pileup);
  obj->setVariable("miniIsoCone",cone);

}

void MiniAODReader::setPtRelAndRatio(const reco::Candidate& pc, SignatureObject* obj) {
	double minR = jetR_;
	double ptRelv1 = 0;
	double ptRelv2 = 0;
	double ptRatiov1 = 1;
	double ptRatiov2 = 1;
	int index = -1;
	for(int i = 0; i < (int)jetHandle_->size(); i++){
		double dR = deltaR(pc, jetHandle_->at(i));
		if(dR < minR) {
			minR = dR;
			index = i;
		}
	}
	if(index >= 0) {
		const pat::Jet& pat_jet = jetHandle_->at(index);
		// We need to make this jet "lepton-aware", i.e. jet = (rawJet * L1 - lepton) * L2L3res + lepton = correctedJet + lepton * (1 - L2L3res)
		// We start with a L3Absolute-corrected jet. The factor to get to L1FastJet correction level is
		double L3toL1 = pat_jet.jecFactor("L1FastJet"); // the inverse of this will be the L2L3 factor needed for the above formula
		LorentzVector jetv1 = pat_jet.p4();
		LorentzVector jetv2 = jetv1 + pc.p4() * (1 - 1./L3toL1);
		
		ptRatiov1 = pc.pt() / jetv1.pt();
		LorentzVector axis = jetv1 - pc.p4();
		TVector3 a(axis.x(), axis.y(), axis.z());      
		TLorentzVector v(pc.p4().px(), pc.p4().py(), pc.p4().pz(), pc.p4().e());
		ptRelv1 = v.Perp(a);
		
		ptRatiov2 = pc.pt() / jetv2.pt();
		axis = jetv2 - pc.p4();
		a = TVector3(axis.x(), axis.y(), axis.z());      
		v = TLorentzVector(pc.p4().px(), pc.p4().py(), pc.p4().pz(), pc.p4().e());
		ptRelv2 = v.Perp(a);
	}
	obj->setVariable("ptRelv1", ptRelv1);
	obj->setVariable("ptRatiov1", ptRatiov1);
	obj->setVariable("ptRel", ptRelv2);
	obj->setVariable("ptRatio", ptRatiov2);
}

void MiniAODReader::initializeBtagSFs() {
  if( isData_ ) cout<<"\nINFO: Running on DATA, all Btag SFs and uncertainties will be set to unity. \n"<<endl;
  if( isData_ ) return; // Do nothing if running on DATA.
  //
  // Btag Scale Factors and Uncertainties - initialization:
  CSVv2calib  = BTagCalibration("csvv2",  edm::FileInPath("RutgersAODReader/BaseAODReader/data/CSVv2.csv").fullPath()  );
  cMVAv2calib = BTagCalibration("cmvav2", edm::FileInPath("RutgersAODReader/BaseAODReader/data/cMVAv2.csv").fullPath() );
  JPcalib     = BTagCalibration("jp",     edm::FileInPath("RutgersAODReader/BaseAODReader/data/JP.csv").fullPath()     );

  opl = BTagEntry::OP_LOOSE;  opV.push_back(opl);
  opm = BTagEntry::OP_MEDIUM; opV.push_back(opm);
  opt = BTagEntry::OP_TIGHT;  opV.push_back(opt);
  sysTypeV = {"central","up","down"};
  
  CSVv2.clear();
  cMVAv2.clear();
  JP.clear();
  for(unsigned int iOP=0; iOP<opV.size(); iOP++){// 3 working points:  L M T
    for(unsigned int iST=0; iST<sysTypeV.size(); iST++){// 3 syst values:  C U D
      //
      // Each btag SF vector (CSVv2, cMVAv2, JP) will have 18 entries, ordered as follows:
      // ------------------------------------------------------------------------------------------------------------------------
      //  LC light/bc, LU light/bc, LD light/bc, MC light/bc, MU light/bc, MD light/bc, TC light/bc, TU light/bc, TD light/bc
      //         0  1         2  3         4  5         6  7         8  9       10  11       12  14       14  15       16  17
      // Key:
      // LC -> Loose WP - Central value
      // LU -> Loose WP - Up value
      // LD -> Loose WP - Down value
      // MC -> Medium WP - Central value, etc..
      // ------------------------------------------------------------------------------------------------------------------------
      CSVv2_LIGHT = BTagCalibrationReader(opV.at(iOP),sysTypeV.at(iST));
      CSVv2_LIGHT.load(CSVv2calib,BTagEntry::FLAV_UDSG);
      CSVv2_BC =  BTagCalibrationReader(opV.at(iOP),sysTypeV.at(iST));
      CSVv2_BC.load(CSVv2calib,BTagEntry::FLAV_B);
      CSVv2.push_back(CSVv2_LIGHT); 
      CSVv2.push_back(CSVv2_BC);

      cMVAv2_LIGHT = BTagCalibrationReader(opV.at(iOP),sysTypeV.at(iST));
      cMVAv2_LIGHT.load(cMVAv2calib,BTagEntry::FLAV_UDSG);
      cMVAv2_BC =  BTagCalibrationReader(opV.at(iOP),sysTypeV.at(iST));
      cMVAv2_BC.load(cMVAv2calib,BTagEntry::FLAV_B);
      cMVAv2.push_back(cMVAv2_LIGHT);  
      cMVAv2.push_back(cMVAv2_BC);

      JP_LIGHT = BTagCalibrationReader(opV.at(iOP),sysTypeV.at(iST));
      JP_LIGHT.load(JPcalib,BTagEntry::FLAV_UDSG);
      JP_BC =  BTagCalibrationReader(opV.at(iOP),sysTypeV.at(iST));
      JP_BC.load(JPcalib,BTagEntry::FLAV_B);
      JP.push_back(JP_LIGHT);  
      JP.push_back(JP_BC);
      /*
      CSVv2_LIGHT  = BTagCalibrationReader(&CSVv2calib,opV.at(iOP),"incl",sysTypeV.at(iST));
      CSVv2_BC     = BTagCalibrationReader(&CSVv2calib,opV.at(iOP),"mujets",sysTypeV.at(iST));
      CSVv2.push_back(CSVv2_LIGHT); 
      CSVv2.push_back(CSVv2_BC);
      //
      cMVAv2_LIGHT = BTagCalibrationReader(&cMVAv2calib,opV.at(iOP),"incl",sysTypeV.at(iST));
      cMVAv2_BC    = BTagCalibrationReader(&cMVAv2calib,opV.at(iOP),"ttbar",sysTypeV.at(iST));
      cMVAv2.push_back(cMVAv2_LIGHT);  
      cMVAv2.push_back(cMVAv2_BC);
      //
      JP_LIGHT = BTagCalibrationReader(&JPcalib,opV.at(iOP),"incl",sysTypeV.at(iST));
      JP_BC    = BTagCalibrationReader(&JPcalib,opV.at(iOP),"mujets",sysTypeV.at(iST));
      JP.push_back(JP_LIGHT);  
      JP.push_back(JP_BC);
      */
    }
  }
}

bool MiniAODReader::ElectronCutBasedIDNoIsolation( const pat::Electron& ele, const reco::Vertex& PV, int IDno){
  // ID definitions follow from:
  //   https://twiki.cern.ch/twiki/bin/view/CMS/CutBasedElectronIdentificationRun2#Spring15_selection_25ns
  //   https://twiki.cern.ch/twiki/bin/view/CMSPublic/EgammaPublicData#Glossary_of_Variables
  //   https://cmssdt.cern.ch/SDT/doxygen/CMSSW_8_0_7/doc/html/db/da4/CutBasedElectronID_8cc_source.html
  // -------------
  float *cutIDB;
  float *cutIDE;
  // -------------
  // Note: "Conversion veto" is applied for all selections below.
  float cutIDvetoB[8]    = { 0.0114, 0.0152,  0.216,  0.181,  0.207,   0.0564, 0.472,  2 }; // cut-based-veto-ID
  float cutIDvetoE[8]    = { 0.0352, 0.0113,  0.237,  0.116,  0.174,   0.222,  0.921,  3 };
  float cutIDlooseB[8]   = { 0.0103, 0.0105,  0.115,  0.104,  0.102,   0.0261, 0.41,   2 }; // cut-based-loose-ID
  float cutIDlooseE[8]   = { 0.0301, 0.00814, 0.182,  0.0897, 0.126,   0.118,  0.822,  1 };
  float cutIDmediumB[8]  = { 0.0101, 0.0103,  0.0336, 0.0876, 0.0174,  0.0118, 0.373,  2 }; // cut-based-medium-ID
  float cutIDmediumE[8]  = { 0.0283, 0.00733, 0.114,  0.0678, 0.0898,  0.0739, 0.602,  1 };
  float cutIDtightB[8]   = { 0.0101, 0.00926, 0.0336, 0.0597, 0.012,   0.0111, 0.0466, 2 }; // cut-based-tight-ID
  float cutIDtightE[8]   = { 0.0279, 0.00724, 0.0918, 0.0615, 0.00999, 0.0351, 0.417,  1 };
  //.....................  |trig online.............................| |Not specified..........|
  float cutIDtrigOnB[8]  = { 0.013,  0.01,    0.07,   0.13,   999999,  999999, 999999, 999999 }; // cut-based-trigOnline  <-- These are the tighter of  Ele*_CaloIdL_TrackIdL_IsoVL  and  Ele*_CaloIdL_GsfTrkIdVL
  float cutIDtrigOnE[8]  = { 0.035,  9999,    9999,   0.10,   999999,  999999, 999999, 999999 }; //                       <-- 
  //                                                                                                                 
  // ------------- 
  // IDno=1 veto  // IDno=2 loose  // IDno=3 medium  // IDno=4 tight
  if( IDno==1 ) cutIDB = cutIDvetoB;
  if( IDno==1 ) cutIDE = cutIDvetoE;
  if( IDno==2 ) cutIDB = cutIDlooseB;
  if( IDno==2 ) cutIDE = cutIDlooseE;
  if( IDno==3 ) cutIDB = cutIDmediumB;
  if( IDno==3 ) cutIDE = cutIDmediumE;
  if( IDno==4 ) cutIDB = cutIDtightB;
  if( IDno==4 ) cutIDE = cutIDtightE;
  if( IDno==5 ) cutIDB = cutIDtrigOnB;
  if( IDno==5 ) cutIDE = cutIDtrigOnE;
  // -------------
  if( ele.pt()>10 && fabs(ele.superCluster()->eta())>=2.5 ) return false; // ignore electrons with pt<10 GeV, |SCeta|>2.5
  // -------------
  if( fabs(ele.superCluster()->eta())<=1.479 ){
    if( ele.full5x5_sigmaIetaIeta()                               >= cutIDB[0] ) return false;
    if( fabs(ele.deltaEtaSuperClusterTrackAtVtx())                >= cutIDB[1] ) return false;
    if( fabs(ele.deltaPhiSuperClusterTrackAtVtx())                >= cutIDB[2] ) return false;
    if( ele.hadronicOverEm()                                      >= cutIDB[3] ) return false;
    if( fabs(1/ele.ecalEnergy()-1.0/ele.trackMomentumAtVtx().R()) >= cutIDB[4] ) return false;
    if( fabs(ele.gsfTrack()->dxy(PV.position()))                  >= cutIDB[5] ) return false;
    if( fabs(ele.gsfTrack()->dz(PV.position()))                   >= cutIDB[6] ) return false;
    if( ele.gsfTrack()->hitPattern().numberOfLostTrackerHits(reco::HitPattern::MISSING_INNER_HITS) > (int)(cutIDB[7]) ) return false; // ">" is intentional
    if( ele.passConversionVeto()                                  == false     ) return false;
    //
  } else{
    if( ele.full5x5_sigmaIetaIeta()                               >= cutIDE[0] ) return false;
    if( fabs(ele.deltaEtaSuperClusterTrackAtVtx())                >= cutIDE[1] ) return false;
    if( fabs(ele.deltaPhiSuperClusterTrackAtVtx())                >= cutIDE[2] ) return false;
    if( ele.hadronicOverEm()                                      >= cutIDE[3] ) return false;
    if( fabs(1/ele.ecalEnergy()-1.0/ele.trackMomentumAtVtx().R()) >= cutIDE[4] ) return false;
    if( fabs(ele.gsfTrack()->dxy(PV.position()))                  >= cutIDE[5] ) return false;
    if( fabs(ele.gsfTrack()->dz(PV.position()))                   >= cutIDE[6] ) return false;
    if( ele.gsfTrack()->hitPattern().numberOfLostTrackerHits(reco::HitPattern::MISSING_INNER_HITS) > (int)(cutIDE[7]) ) return false; // ">" is intentional
    if( ele.passConversionVeto()                                  == false     ) return false;
  }
  //
  if( IDno==5 ){
    if( ele.ecalPFClusterIso()/ele.pt() >= 0.50 ) return false;
    if( ele.hcalPFClusterIso()/ele.pt() >= 0.30 ) return false;
    if( ele.dr03TkSumPt()/ele.pt()      >= 0.20 ) return false;
  }
  //
  return true;
}

void MiniAODReader::fillBtagSFs( SignatureObject* objjet, const pat::Jet& patjet, string btagger ){
  //
  // Allowed btagger string inputs are: "CSV", "JP", "cMVA
  if( btagger != "CSV" && btagger != "JP" && btagger != "cMVA" ){
    cout<< "\nWARNING: invalid btagger input is provided, no btag SFs will be computed/stored! \n"<<endl;
    return; 
  }

  bool isdebug = false;

  // Set all values to unity for DATA.
  if( isData_ ){
    for( unsigned int iwp=0; iwp<=2; iwp++){
      double SF  = 1;
      double SFu = 1;
      double SFd = 1;
      string WPlabel="_WPL";
      if(iwp==1) WPlabel="_WPM";
      if(iwp==2) WPlabel="_WPT";
      objjet->setVariable(btagger+WPlabel+"_SFc",SF); 
      objjet->setVariable(btagger+WPlabel+"_SFu",SFu);
      objjet->setVariable(btagger+WPlabel+"_SFd",SFd);
    }
    return;
  }

  // ------------------------------------------------------------------------------------------------------------------------
  //  The CMSSW b-tagger module "BTagCalibrationReader" will return a SF value of 0 for jets with |eta|>2.4 or pt<20.
  // ------------------------------------------------------------------------------------------------------------------------

  // Get patjet variables: 
  int   btagjetflav    = patjet.hadronFlavour();
  float btagjeteta     = patjet.eta();
  float lightbtagjetpt = patjet.pt(); 
  float bcbtagjetpt    = patjet.pt(); 

  //cout<<"Print Jet info Flav/Eta/Pt : "<<btagjetflav<<"/"<<btagjeteta<<"/"<<lightbtagjetpt<<endl;

  // Set tagger dependent proxy SF vector, and pt boundaries:
  //    https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation76X
  vector<BTagCalibrationReader> proxy;
  float minlightpt = 20;  //dummy values
  float maxlightpt = 1000;//
  float minbcpt    = 30;  //
  float maxbcpt    = 670; //
  if(      btagger == "cMVA" ){ proxy = cMVAv2; minlightpt=20; maxlightpt=1000; minbcpt=30; maxbcpt=320; }
  else if( btagger == "CSV"  ){ proxy = CSVv2;  minlightpt=20; maxlightpt=1000; minbcpt=30; maxbcpt=670; }
  else if( btagger == "JP"   ){ proxy = JP;     minlightpt=20; maxlightpt=1000; minbcpt=30; maxbcpt=670; }

  // Fix pt boundaries: 
  // 20 GeV is the currently supported lowest pt threshold for btagging (see above Twiki)
  double UncFactor=1;
  if(      lightbtagjetpt>maxlightpt                      ){ lightbtagjetpt = maxlightpt*0.9999; UncFactor=2; }
  else if( lightbtagjetpt<minlightpt && lightbtagjetpt>20 ){ lightbtagjetpt = minlightpt*1.0001; UncFactor=2; }
  //
  if(      bcbtagjetpt>maxbcpt                            ){    bcbtagjetpt = maxbcpt*0.9999;    UncFactor=2; }
  else if( bcbtagjetpt<minbcpt       &&    bcbtagjetpt>20 ){    bcbtagjetpt = minbcpt*1.0001;    UncFactor=2; }

  // Read and store SF values and uncertainties:
  // ------------------------------------------------------------------------------------------------------------------------
  // btag SF vector structure: 18 members (0-17) for a given tagger, 3 vectors in total.
  //           LOOSE WORKING POINT           |         MEDIUM WORKING POINT           |          TIGHT WORKING POINT
  //  LC light/bc, LU light/bc, LD light/bc, | MC light/bc, MU light/bc, MD light/bc, | TC light/bc, TU light/bc, TD light/bc
  //         0  1         2  3         4  5  |        6  7         8  9       10  11  |      12  14       14  15       16  17
  // ------------------------------------------------------------------------------------------------------------------------
  for( unsigned int iwp=0; iwp<=2; iwp++){ 
    // iwp = 0:loose, 1:medium, 2:tight
    double SF  = -1;
    double SFu = -1;
    double SFd = -1;

    // ------------------------------------------------------------------------------------------------------------------------
    // Jet flavour: the jet flavour in b-tag studies in Run2 is determined according to the so called hadron-based definition.
    //   See: https://twiki.cern.ch/twiki/bin/view/CMS/BtagRecommendation#Useful_Tools
    // ------------------------------------------------------------------------------------------------------------------------

    // if reco jet has light-hadron flavour
    if( btagjetflav == 0 ){
      unsigned int ptr = iwp*6;
      SF  = proxy.at(ptr).eval(BTagEntry::FLAV_UDSG,  btagjeteta, lightbtagjetpt); ptr=ptr+2;
      SFu = proxy.at(ptr).eval(BTagEntry::FLAV_UDSG,  btagjeteta, lightbtagjetpt); ptr=ptr+2;
      SFd = proxy.at(ptr).eval(BTagEntry::FLAV_UDSG,  btagjeteta, lightbtagjetpt);
    }
    // if reco jet has c-hadron flavor
    if( btagjetflav == 4 ){ 
      unsigned int ptr = iwp*6+1;
      SF  = proxy.at(ptr).eval(BTagEntry::FLAV_C,     btagjeteta,    bcbtagjetpt); ptr=ptr+2;
      SFu = proxy.at(ptr).eval(BTagEntry::FLAV_C,     btagjeteta,    bcbtagjetpt); ptr=ptr+2;
      SFd = proxy.at(ptr).eval(BTagEntry::FLAV_C,     btagjeteta,    bcbtagjetpt);
    }
    // if reco jet has b-hadron flavor
    if( btagjetflav == 5 ){
      unsigned int ptr = iwp*6+1;
      SF  = proxy.at(ptr).eval(BTagEntry::FLAV_B,     btagjeteta,    bcbtagjetpt); ptr=ptr+2;
      SFu = proxy.at(ptr).eval(BTagEntry::FLAV_B,     btagjeteta,    bcbtagjetpt); ptr=ptr+2;
      SFd = proxy.at(ptr).eval(BTagEntry::FLAV_B,     btagjeteta,    bcbtagjetpt);
    }
    // inflate the uncertainty in up and down values, if needed:
    SFu = UncFactor*( SFu - SF ) + SF;
    SFd = UncFactor*( SFd - SF ) + SF;

    // Set variables:    
    string WPlabel="_WPL";
    if(iwp==1) WPlabel="_WPM";
    if(iwp==2) WPlabel="_WPT";
    //
    if(isdebug) cout<<"Jet Btag info :: Flav/Pt/Eta "<<btagger+WPlabel<<" SF/SFup/SFdown : "<<btagjetflav<<"/"<<lightbtagjetpt<<"/"<<btagjeteta<<"/ ";
    if(isdebug) cout<<SF<<"/"<<SFu<<"/"<<SFd<<endl;

    objjet->setVariable(btagger+WPlabel+"_SFc",SF); //central value
    objjet->setVariable(btagger+WPlabel+"_SFu",SFu);//central + 1sigma (UP)
    objjet->setVariable(btagger+WPlabel+"_SFd",SFd);//central - 1sigma (DOWN)
  }

}


DEFINE_FWK_MODULE(MiniAODReader);
