#include "FWCore/Framework/interface/EventSetup.h"
#include "RutgersAODReader/BaseAODReader/interface/BaseAODReader.h"
#include <TClonesArray.h>
#include <TObjArray.h>
#include <Compression.h>
#include <TTree.h>
#include <TFile.h>
#include "RutgersIAF/EventAnalyzer/interface/SignatureObject.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableCombined.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableInRange.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableMT.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableMass.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableN.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableOS.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableOSSF.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableObjectVariableExtreme.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableObjectWeightPtTF1.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariablePairMass.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableReversed.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableSmearMET.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableSumPT.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableTF1.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableTH1.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableThreshold.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableTriggerWeight.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableValue.h"
#include "RutgersIAF/EventAnalyzer/interface/EventVariableValueInList.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectComparisonDeltaR.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectComparisonElectron.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectComparisonMatchDeltaRCharge.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectComparisonSkimRecoTracks.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableCombined.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableElectronTotalIso.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableInRange.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableMethod.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableMuonTotalIso.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableRelIso.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableReversed.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableTauTotalIso.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableValue.h"
#include "RutgersIAF/EventAnalyzer/interface/ObjectVariableValueInList.h"
#include "RutgersIAF/EventAnalyzer/interface/Signature.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureTH1F_CountNoWeight.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureTH1F_CountWeight.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureTH1F_EventVariable.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureTH1F_N.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureTH1F_ObjectVariable.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureTH2F_EventVariableVsEventVariable.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureTH2F_EventVariableVsObjectVariable.h"
#include "RutgersIAF/EventAnalyzer/interface/SignatureTH2F_ObjectVariableVsObjectVariable.h"


using namespace std;
using namespace edm;

BaseAODReader::BaseAODReader(const ParameterSet& pset) : 
  BaseTreeReader(0),
  BaseHandler(TString(pset.getUntrackedParameter<string>("outFilename")),this),
  BaseTreeWriter(this,TString(pset.getUntrackedParameter<string>("treeName"))),hltPrescale_(pset,consumesCollector(),*this)
{
  //TClass* cl = TClass::GetClass("SignatureObject");
  //cout<<"1 inheritance: "<<cl->GetName()<<" "<<cl->InheritsFrom(TObject::Class())<<" "<<cl->GetCheckSum()<<" "<<cl->GetNmethods()<<endl;

  writeAllEvents_ = true;
  writeAllObjects_ = false;

  setWriter(this);

  if(pset.exists("writeAllEvents"))writeAllEvents_ = pset.getParameter<bool>("writeAllEvents");
  if(pset.exists("writeAllObjects"))writeAllObjects_ = pset.getParameter<bool>("writeAllObjects");

  jetLabel_ = pset.getParameter<InputTag>("patJetsInputTag");
  jetToken_ = consumes<edm::View<pat::Jet> >(jetLabel_);

  electronLabel_ = pset.getParameter<InputTag>("patElectronsInputTag");
  electronToken_ = consumes<edm::View<pat::Electron> >(electronLabel_);

  muonLabel_ = pset.getParameter<InputTag>("patMuonsInputTag");
  muonToken_ = consumes<edm::View<pat::Muon> >(muonLabel_);

  tauLabel_ = pset.getParameter<InputTag>("patTausInputTag");
  tauToken_ = consumes<edm::View<pat::Tau> >(tauLabel_);

  photonLabel_ = pset.getParameter<InputTag>("patPhotonsInputTag");
  photonToken_ = consumes<edm::View<pat::Photon> >(photonLabel_);

  phoLooseIdLabel_ = pset.getParameter<edm::InputTag>("phoLooseIdMap");
  phoLooseIdMapToken_ = consumes<edm::ValueMap<bool> >(phoLooseIdLabel_);
  
  phoMediumIdLabel_ = pset.getParameter<edm::InputTag>("phoMediumIdMap");
  phoMediumIdMapToken_ = consumes<edm::ValueMap<bool> >(phoMediumIdLabel_);
  
  phoTightIdLabel_ = pset.getParameter<edm::InputTag>("phoTightIdMap");
  phoTightIdMapToken_ = consumes<edm::ValueMap<bool> >(phoTightIdLabel_);
  
  phoChargedIsolationLabel_ = pset.getParameter<edm::InputTag>("phoChargedIsolationMap");
  phoChargedIsolationMapToken_ = consumes<edm::ValueMap<float> >(phoChargedIsolationLabel_);
  
  phoNeutralHadronIsolationLabel_ = pset.getParameter<edm::InputTag>("phoNeutralHadronIsolationMap");
  phoNeutralHadronIsolationMapToken_ = consumes<edm::ValueMap<float> >(phoNeutralHadronIsolationLabel_);
  
  phoPhotonIsolationLabel_ = pset.getParameter<edm::InputTag>("phoPhotonIsolationMap");
  phoPhotonIsolationMapToken_ = consumes<edm::ValueMap<float> >(phoPhotonIsolationLabel_);
  
  phoWorstChargedIsolationLabel_ = pset.getParameter<edm::InputTag>("phoWorstChargedIsolationMap");
  phoWorstChargedIsolationMapToken_ = consumes<edm::ValueMap<float> >(phoWorstChargedIsolationLabel_);

  if(pset.exists("patMETInputTag")) {
    metLabel_ = pset.getParameter<InputTag>("patMETInputTag");
    metToken_ = consumes<edm::View<pat::MET> >(metLabel_);
  }

  trackLabel_ = pset.getParameter<InputTag>("trackInputTag");
  trackToken_ = consumes<edm::View<reco::Track> >(trackLabel_);

  vertexLabel_ = pset.getParameter<InputTag>("vtxInputTag");
  vertexToken_ = consumes<edm::View<reco::Vertex> >(vertexLabel_);

  beamspotLabel_ = pset.getParameter<InputTag>("beamSpotInputTag");
  beamspotToken_ = consumes<reco::BeamSpot>(beamspotLabel_);

  triggerLabel_ = pset.getParameter<InputTag>("triggerInputTag");
  triggerToken_ = consumes<edm::TriggerResults>(triggerLabel_);

  genParticleLabel_ = pset.getParameter<InputTag>("genParticleInputTag");
  genParticleToken_ = consumes<reco::GenParticleCollection>(genParticleLabel_);

  triggerEventLabel_ = pset.getParameter<InputTag>("triggerEventInputTag");
  triggerEventToken_ = consumes<trigger::TriggerEvent>(triggerEventLabel_);

  processName_ = pset.getParameter<std::string>("processName");

  if(pset.exists("setupList")){
    VParameterSet cutList = pset.getParameter<VParameterSet>("setupList");
    processParameters(cutList);
  }

  LHERunInfoProductLabel_ = pset.getParameter<InputTag>("genInfoTag");
  LHERunInfoProductToken_ = consumes<LHERunInfoProduct,edm::InRun>(LHERunInfoProductLabel_);// Token not used, but necessary for CMSSW >=76X
  
  printLHERunInfo_ = pset.getParameter<bool>("printLHERunInfo");

  getJECUncFromDB_ = pset.getParameter<bool>("getJECUncFromDB");
  JECUncFileName_  = pset.getParameter<edm::FileInPath>("JECUncFileName");

  getJERSFfromGT_  = pset.getParameter<bool>("getJERSFfromGT");
  JERSFfileName_   = pset.getParameter<edm::FileInPath>("JERSFfileName");

  // Eleceton ID: MVA
  eleMediumIdMapLabel_   = pset.getParameter<InputTag>("eleMediumIdMap");
  eleTightIdMapLabel_    = pset.getParameter<InputTag>("eleTightIdMap");
  mvaValuesMapLabel_     = pset.getParameter<InputTag>("mvaValuesMap");
  mvaCategoriesMapLabel_ = pset.getParameter<InputTag>("mvaCategoriesMap");
  eleMediumIdMapToken_   = consumes<edm::ValueMap<bool> >(eleMediumIdMapLabel_);
  eleTightIdMapToken_    = consumes<edm::ValueMap<bool> >(eleTightIdMapLabel_);
  mvaValuesMapToken_     = consumes<edm::ValueMap<float> >(mvaValuesMapLabel_);
  mvaCategoriesMapToken_ = consumes<edm::ValueMap<int> >(mvaCategoriesMapLabel_);

  // Electron ID: Cut Based
  eleVetoIdMapCutBasedLabel_   = pset.getParameter<InputTag>("eleVetoIdMapCutBased");
  eleLooseIdMapCutBasedLabel_  = pset.getParameter<InputTag>("eleLooseIdMapCutBased");
  eleMediumIdMapCutBasedLabel_ = pset.getParameter<InputTag>("eleMediumIdMapCutBased");
  eleTightIdMapCutBasedLabel_  = pset.getParameter<InputTag>("eleTightIdMapCutBased");
  eleVetoIdMapCutBasedToken_   = consumes<edm::ValueMap<bool> >(eleVetoIdMapCutBasedLabel_);
  eleLooseIdMapCutBasedToken_  = consumes<edm::ValueMap<bool> >(eleLooseIdMapCutBasedLabel_);
  eleMediumIdMapCutBasedToken_ = consumes<edm::ValueMap<bool> >(eleMediumIdMapCutBasedLabel_);
  eleTightIdMapCutBasedToken_  = consumes<edm::ValueMap<bool> >(eleTightIdMapCutBasedLabel_);

  // Electron ID: HEEP
  eleHEEPIdMapLabel_ = pset.getParameter<InputTag>("eleHEEPIdMap");
  eleHEEPIdMapToken_ = consumes<edm::ValueMap<bool> >(eleHEEPIdMapLabel_);

  initTree();
}

BaseAODReader::~BaseAODReader()
{

  delete m_array;

}

void BaseAODReader::beginJob()
{
  initSignatures();
}

void BaseAODReader::endJob()
{
  finishSignatures();
}

void BaseAODReader::beginRun(edm::Run const& run, edm::EventSetup const& eventsetup)
{
  if(printLHERunInfo_){
    if( run.getByLabel( LHERunInfoProductLabel_, LHERunInfoProductHandle_ ) ){
      typedef std::vector<LHERunInfoProduct::Header>::const_iterator headers_const_iterator;
      LHERunInfoProduct myLHERunInfoProduct = *(LHERunInfoProductHandle_.product());
      cout<<"\n\nPrint LHERunInfoProduct - begin"<<endl;
      for (headers_const_iterator iter=myLHERunInfoProduct.headers_begin(); iter!=myLHERunInfoProduct.headers_end(); iter++){
	std::cout << iter->tag() << std::endl;
	std::vector<std::string> lines = iter->lines();
	for (unsigned int iLine = 0; iLine<lines.size(); iLine++) {
	  std::cout << lines.at(iLine);
	}
      }
      cout<<"Print LHERunInfoProduct - end\n\n"<<endl;
    } else{
      cout<<"\n\nLHERunInfoProduct is not available!\n\n"<<endl;
    }
  }
  
  // Example taken from:
  // https://github.com/cms-sw/cmssw/blob/CMSSW_7_6_X/PhysicsTools/PatUtils/plugins/ShiftedPFCandidateProducerForNoPileUpPFMEt.cc#L12
  // https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookJetEnergyCorrections#JetCorUncertainties?rev=129
  if(getJECUncFromDB_){
    cout<<"\n\nJEC uncertainties are taken from the database.\n\n"<<endl;
    eventsetup.get<JetCorrectionsRecord>().get("AK4PFchs",JetCorParColl);
    JetCorrectorParameters const & JetCorPar = (*JetCorParColl)["Uncertainty"];
    jecUnc = new JetCorrectionUncertainty(JetCorPar);
  }else{
    cout<<"\n\nJEC uncertainties are taken from file:\n"<<JECUncFileName_<<"\n\n"<<endl;
    jetCorrParameters_ = new JetCorrectorParameters(JECUncFileName_.fullPath().data(),"");
    jecUnc = new JetCorrectionUncertainty(*jetCorrParameters_);
  }
  
  // Example taken from:
  // https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookJetEnergyResolution#CMSSW_7_6_X
  if(getJERSFfromGT_){
    cout<<"\n\nJER scale factors and uncertainties are taken from the GT.\n\n"<<endl;
    res_sf = JME::JetResolutionScaleFactor::get(eventsetup, "AK4PFchs_pt");
  }else{
    cout<<"\n\nJER scale factors and uncertainties are taken from file:\n"<<JERSFfileName_<<"\n\n"<<endl;
    res_sf = JME::JetResolutionScaleFactor(JERSFfileName_.fullPath().data());
  }
  
  //getOutFile()->SetCompressionSettings(ROOT::CompressionSettings(ROOT::kLZMA,5));
  //cout<<getOutFile()->GetCompressionAlgorithm()<<" "<<getOutFile()->GetCompressionLevel()<<" "<<getOutFile()->GetCompressionSettings()<<endl;
  bool changed(true);
  if(hltConfig_.init(run,eventsetup,processName_,changed)){
    /*
    hltConfig_.dump("ProcessName");
    hltConfig_.dump("GlobalTag");
    hltConfig_.dump("TableName");
    hltConfig_.dump("Streams");
    hltConfig_.dump("Datasets");
    hltConfig_.dump("PrescaleTable");
    hltConfig_.dump("ProcessPSet");
    */
  }
  hltPrescale_.init(run,eventsetup,processName_,changed);
}

void BaseAODReader::processParameters(const VParameterSet vpset)
{
  for(int i = 0; i < (int)vpset.size(); i++){
    ParameterSet thingToDo = vpset[i];
    if(!thingToDo.exists("type"))continue;
    TString type = thingToDo.getUntrackedParameter<string>("type");
    if(type.Contains("EventVariable")){
      processEventVariable(thingToDo);
    }else if(type.Contains("ObjectVariable")){
      processObjectVariable(thingToDo);
    }else if(type.Contains("ObjectComparison")){
      processObjectComparison(thingToDo);
    }else if(type.Contains("SignatureTH")){
      processSignatureTH(thingToDo);
    }else if(type.Contains("Product")){
      processProduct(thingToDo);
    }else if(type == "Signature"){
    processSignature(thingToDo);
    }else{
      cerr << "---Don't know what to do with type: "<<type<<endl;
      cerr << "---Contact developers or add to processing code "<<endl;
    }
  }
}

void BaseAODReader::analyze(const Event& event, const EventSetup& eventsetup)
{
  m_currentEntry++;
  clearProducts();
  resetVariables();
  /*
  p_variable_map_double_values->clear(); 
  p_variable_map_long_values->clear(); 
  p_variable_map_int_values->clear(); 
  p_variable_map_TString_values->clear(); 
  p_variable_map_bool_values->clear(); 

  p_variable_map_double_keys->clear(); 
  p_variable_map_long_keys->clear(); 
  p_variable_map_int_keys->clear(); 
  p_variable_map_TString_keys->clear(); 
  p_variable_map_bool_keys->clear(); 
  */

  event_ = (long)event.id().event();
  run_ = (int)event.id().run();
  lumi_ = (int)event.luminosityBlock();

  makeProducts(event,eventsetup);

  m_products = m_productmap;

  prepareEvent();
  setVariable("EVENT",event_);
  setVariable("RUN",run_);
  setVariable("LUMI",lumi_);

  analyzeEvent();

  bool writeEvent = false;
  bool isSet = getVariable("WRITEEVENT",writeEvent);
  writeEvent &= isSet;

  if(writeAllEvents_ || writeEvent)fillTree();

}

void BaseAODReader::finish()
{
  getOutFile()->cd();
  m_tree->Write("", TObject::kOverwrite);
}

void BaseAODReader::makeProducts(const Event& event, const EventSetup& eventsetup)
{
  if(event.getByToken(jetToken_,jetHandle_))
    makeJets();

  if(event.getByToken(electronToken_,electronHandle_))
    makeElectrons();

  if(event.getByToken(muonToken_,muonHandle_))
    makeMuons();

  if(event.getByToken(photonToken_,photonHandle_) 
     && event.getByToken(phoLooseIdMapToken_ ,loose_id_decisions) 
     && event.getByToken(phoMediumIdMapToken_ ,medium_id_decisions) 
     && event.getByToken(phoTightIdMapToken_ ,tight_id_decisions) 
     && event.getByToken(phoChargedIsolationMapToken_ ,phoChargedIsolationHandle_) 
     && event.getByToken(phoNeutralHadronIsolationMapToken_ ,phoNeutralHadronIsolationHandle_) 
     && event.getByToken(phoPhotonIsolationMapToken_ ,phoPhotonIsolationHandle_) 
     && event.getByToken(phoWorstChargedIsolationMapToken_ ,phoWorstChargedIsolationHandle_)
     )
    makePhotons();

  if(event.getByToken(tauToken_,tauHandle_))
    makeTaus();

  if(event.getByToken(metToken_,metHandle_))
    makeMET();

  if(event.getByToken(trackToken_,trackHandle_))
    makeTracks();

  if(event.getByToken(vertexToken_,vertexHandle_))
    makeVertices();

  if(event.getByToken(triggerToken_, triggerHandle_) && event.getByToken(triggerEventToken_, triggerEventHandle_)) 
    makeTriggers(event,eventsetup);
    //event.getByToken(triggerEventToken_, triggerEventHandle_))

  if(event.getByToken(beamspotToken_, beamspotHandle_))
    makeBeamSpot();

  if(event.getByToken(genParticleToken_, genParticleHandle_))
    makeGenParticles();
}

void BaseAODReader::makeProducts()
{
  /* needed to satisfy inheritance*/
}

void BaseAODReader::fillTree()
{
  m_variable_map_double_values.clear(); 
  m_variable_map_long_values.clear(); 
  m_variable_map_int_values.clear(); 
  m_variable_map_TString_values.clear(); 
  m_variable_map_bool_values.clear(); 

  m_variable_map_double_keys.clear(); 
  m_variable_map_long_keys.clear(); 
  m_variable_map_int_keys.clear(); 
  m_variable_map_TString_keys.clear(); 
  m_variable_map_bool_keys.clear(); 

  m_array->Delete();
  map<TString,vector<SignatureObject*> > products = m_products;
  map<TString,vector<SignatureObject*> >::const_iterator iter;
  map<SignatureObjectFlat*,map<TString,SignatureObject*> > oldAssociateList;
  map<SignatureObject*,SignatureObjectFlat*> oldToNew;
  map<SignatureObjectFlat*,SignatureObject*> newToOld;
  int i = 0;
  for(iter = products.begin(); iter != products.end(); iter++){
    TString productname = (*iter).first;
    vector<SignatureObject*> v = (*iter).second;
    for(int j = 0; j < (int)v.size();j++){
      bool check = false;
      bool isset = v[j]->getVariable("WRITEOBJECT",check);
      if((!isset || !check) && !writeAllObjects_)continue;
      //cout<<i<<endl;
      v[j]->setVariable("PRODUCTNAME",productname);
      m_array->AddLast(new SignatureObjectFlat(*v[j]));
      //new((*m_clonesarray)[i]) SignatureObjectFlat(*v[j]);
      //cout<<" "<<i<<endl;
      //oldToNew[v[j]] = (SignatureObjectFlat*)m_clonesarray->At(i);
      //newToOld[(SignatureObjectFlat*)m_clonesarray->At(i)] = v[j];
      //oldAssociateList[(SignatureObjectFlat*)m_clonesarray->At(i)] = v[j]->getAssociates();
      i++;
    }
  }

  for(map<TString,vector<double> >::const_iterator iter = m_variable_map_double.begin(); iter != m_variable_map_double.end(); iter++){
    m_variable_map_double_keys.push_back(string((*iter).first));
    m_variable_map_double_values.push_back(((*iter).second)[0]);
  }

  for(map<TString,vector<int> >::const_iterator iter = m_variable_map_int.begin(); iter != m_variable_map_int.end(); iter++){
    m_variable_map_int_keys.push_back(string((*iter).first));
    m_variable_map_int_values.push_back(((*iter).second)[0]);
  }

  for(map<TString,vector<long> >::const_iterator iter = m_variable_map_long.begin(); iter != m_variable_map_long.end(); iter++){
    m_variable_map_long_keys.push_back(string((*iter).first));
    m_variable_map_long_values.push_back(((*iter).second)[0]);
  }

  for(map<TString,vector<TString> >::const_iterator iter = m_variable_map_TString.begin(); iter != m_variable_map_TString.end(); iter++){
    m_variable_map_TString_keys.push_back(string((*iter).first));
    m_variable_map_TString_values.push_back(string(((*iter).second)[0]));
  }

  for(map<TString,bool>::const_iterator iter = m_variable_map_bool.begin(); iter != m_variable_map_bool.end(); iter++){
    m_variable_map_bool_keys.push_back(string((*iter).first));
    m_variable_map_bool_values.push_back(((*iter).second));
  }


  //Fix associations
  /*
  int n = m_clonesarray->GetEntries();

  for(int j = 0; j < n; j++){
    SignatureObjectFlat* newObject = (SignatureObjectFlat*)m_clonesarray->At(j);
    map<SignatureObjectFlat*,SignatureObject*>::iterator iterNew = newToOld.find(newObject);
    if(iterNew == newToOld.end())continue;
    map<TString,SignatureObject*> oldAssociates = oldAssociateList[newObject];
    map<TString,SignatureObject*> newAssociates;
    map<TString,SignatureObject*>::iterator associateIter;
    for(associateIter = oldAssociates.begin(); associateIter != oldAssociates.end(); associateIter++){
      TString associateName = (*associateIter).first;
      SignatureObject* oldAssociate = (*associateIter).second;
      map<SignatureObject*,SignatureObjectFlat*>::const_iterator iterOld = oldToNew.find(oldAssociate);
      if(iterOld == oldToNew.end())continue;
      newObject->addAssociate(string(associateName),(*iterOld).second);
    }
  }
  */  
  m_tree->Fill();
}

void BaseAODReader::initTree()
{

  //TClass* cl = TClass::GetClass("SignatureObject");
  //cout<<"inheritance: "<<cl->InheritsFrom(TObject::Class())<<endl;
  //m_clonesarray = new TClonesArray("SignatureObjectFlat",1000);
  m_array = new TObjArray();
  m_tree->Branch("event", &event_,"event/L");
  m_tree->Branch("run", &run_, "run/I");
  m_tree->Branch("lumi", &lumi_, "lumi/I");
  m_tree->Branch("SignatureObjectFlats",&m_array,256000);

  p_variable_map_double_keys = &m_variable_map_double_keys;
  p_variable_map_int_keys = &m_variable_map_int_keys;
  p_variable_map_long_keys = &m_variable_map_long_keys;
  p_variable_map_TString_keys = &m_variable_map_TString_keys;
  p_variable_map_bool_keys = &m_variable_map_bool_keys;
  p_variable_map_double_values = &m_variable_map_double_values;
  p_variable_map_int_values = &m_variable_map_int_values;
  p_variable_map_long_values = &m_variable_map_long_values;
  p_variable_map_TString_values = &m_variable_map_TString_values;
  p_variable_map_bool_values = &m_variable_map_bool_values;

  m_tree->Branch("VariableDoubleValues",&p_variable_map_double_values);
  m_tree->Branch("VariableIntValues",&p_variable_map_int_values);
  m_tree->Branch("VariableLongValues",&p_variable_map_long_values);
  m_tree->Branch("VariableTStringValues",&p_variable_map_TString_values);
  m_tree->Branch("VariableBoolValues",&p_variable_map_bool_values);
  m_tree->Branch("VariableDoubleKeys",&p_variable_map_double_keys);
  m_tree->Branch("VariableIntKeys",&p_variable_map_int_keys);
  m_tree->Branch("VariableLongKeys",&p_variable_map_long_keys);
  m_tree->Branch("VariableTStringKeys",&p_variable_map_TString_keys);
  m_tree->Branch("VariableBoolKeys",&p_variable_map_bool_keys);
}

void BaseAODReader::makeJets()
{

  vector<SignatureObject*> jets;
  for(int i = 0; i < (int)jetHandle_->size(); i++){
    const pat::Jet& pat_jet = jetHandle_->at(i);
    SignatureObject* jet = new SignatureObject(pat_jet.px(),pat_jet.py(),pat_jet.pz(),pat_jet.energy());
    jet->setVariable("WRITEOBJECT",true);
    jets.push_back(jet);
  }

  sort(jets.begin(),jets.end(),SignatureObjectComparison);
  reverse(jets.begin(),jets.end());

  m_productmap["ALLJETS"] = jets;


}

void BaseAODReader::makeElectrons()
{
  const reco::Vertex &PV = vertexHandle_->front();

  vector<SignatureObject*> electrons;
  for(int i = 0; i < (int)electronHandle_->size(); i++){
    const pat::Electron& pat_electron = electronHandle_->at(i);
    SignatureObject* electron = new SignatureObject(pat_electron.px(),pat_electron.py(),pat_electron.pz(),pat_electron.energy());
    electron->setVariable("INPUTTYPE",TString("electron"));
    electron->setVariable("dxy",pat_electron.gsfTrack()->dxy(PV.position()));
    electron->setVariable("dz",pat_electron.gsfTrack()->dz(PV.position()));
    electron->setVariable("charge",pat_electron.charge());
    electron->setVariable("superClustereta",pat_electron.superCluster()->eta());
    electron->setVariable("passConversionVeto",pat_electron.passConversionVeto());
    electron->setVariable("numberOfLostHits",pat_electron.gsfTrack()->hitPattern().numberOfLostTrackerHits(reco::HitPattern::MISSING_INNER_HITS));
    electron->setVariable("sigmaIetaIeta",pat_electron.sigmaIetaIeta());
    electron->setVariable("full5x5_sigmaIetaIeta",pat_electron.full5x5_sigmaIetaIeta());
    electron->setVariable("numberOfHits",pat_electron.gsfTrack()->hitPattern().numberOfTrackerHits(reco::HitPattern::TRACK_HITS));
    electron->setVariable("isPF",pat_electron.isPF());
    electron->setVariable("sigmaIetaIphi",pat_electron.sigmaIetaIphi());
    electron->setVariable("full5x5_sigmaIetaIphi",pat_electron.sigmaIetaIphi());
    electron->setVariable("ip3d",pat_electron.ip3d());
    electron->setVariable("scPixCharge",pat_electron.scPixCharge());
    electron->setVariable("isGsfCtfScPixChargeConsistent",pat_electron.isGsfCtfScPixChargeConsistent());
    electron->setVariable("isGsfScPixChargeConsistent",pat_electron.isGsfScPixChargeConsistent());
    electron->setVariable("isGsfCtfChargeConsistent",pat_electron.isGsfCtfChargeConsistent());
    electron->setVariable("hcalOverEcal",pat_electron.hcalOverEcal());
    electron->setVariable("hadronicOverEm",pat_electron.hadronicOverEm());
    electron->setVariable("mva_Isolated",pat_electron.mva_Isolated());
    electron->setVariable("mva_e_pi",pat_electron.mva_e_pi());
    electron->setVariable("sumChargedHadronPt",pat_electron.pfIsolationVariables().sumChargedHadronPt);
    electron->setVariable("sumNeutralHadronEt",pat_electron.pfIsolationVariables().sumNeutralHadronEt);
    electron->setVariable("sumPhotonEt",pat_electron.pfIsolationVariables().sumPhotonEt);
    electron->setVariable("sumChargedParticlePt",pat_electron.pfIsolationVariables().sumChargedParticlePt);
    electron->setVariable("sumNeutralHadronEtHighThreshold",pat_electron.pfIsolationVariables().sumNeutralHadronEtHighThreshold);
    electron->setVariable("sumPhotonEtHighThreshold",pat_electron.pfIsolationVariables().sumPhotonEtHighThreshold);
    electron->setVariable("sumPUPt",pat_electron.pfIsolationVariables().sumPUPt);
    electron->setVariable("deltaEtaSuperClusterTrackAtVtx",pat_electron.deltaEtaSuperClusterTrackAtVtx());
    electron->setVariable("deltaEtaSeedClusterTrackAtCalo",pat_electron.deltaEtaSeedClusterTrackAtCalo());
    electron->setVariable("deltaEtaEleClusterTrackAtCalo",pat_electron.deltaEtaEleClusterTrackAtCalo());
    electron->setVariable("deltaPhiSuperClusterTrackAtVtx",pat_electron.deltaPhiSuperClusterTrackAtVtx());
    electron->setVariable("deltaPhiSeedClusterTrackAtCalo",pat_electron.deltaPhiSeedClusterTrackAtCalo());
    electron->setVariable("deltaPhiEleClusterTrackAtCalo",pat_electron.deltaPhiEleClusterTrackAtCalo());
    if(pat_electron.isElectronIDAvailable("eidLoose"))electron->setVariable("eidLoose",pat_electron.electronID("eidLoose"));
    if(pat_electron.isElectronIDAvailable("eidRobustLoose"))electron->setVariable("eidRobustLoose",pat_electron.electronID("eidRobustLoose"));
    if(pat_electron.isElectronIDAvailable("eidTight"))electron->setVariable("eidTight",pat_electron.electronID("eidTight"));
    if(pat_electron.isElectronIDAvailable("eidRobustTight"))electron->setVariable("eidRobustTight",pat_electron.electronID("eidRobustTight"));
    if(pat_electron.isElectronIDAvailable("eidRobustHighEnergy"))electron->setVariable("eidRobustHighEnergy",pat_electron.electronID("eidRobustHighEnergy"));

    if(pat_electron.isElectronIDAvailable("eidLoose"))
      electron->setVariable("eidLoose",(int)(pat_electron.electronID("eidLoose"))); 
    if(pat_electron.isElectronIDAvailable("eidRobustLoose"))
      electron->setVariable("eidRobustLoose",(int)(pat_electron.electronID("eidRobustLoose")));
    if(pat_electron.isElectronIDAvailable("eidTight"))
      electron->setVariable("eidTight",(int)(pat_electron.electronID("eidTight")));
    if(pat_electron.isElectronIDAvailable("eidRobustTight"))
      electron->setVariable("eidRobustTight",(int)(pat_electron.electronID("eidRobustTight")));
    if(pat_electron.isElectronIDAvailable("eidRobustHighEnergy"))
      electron->setVariable("eidRobustHighEnergy",(int)(pat_electron.electronID("eidRobustHighEnergy")));
    //
    if(pat_electron.isElectronIDAvailable("cutBasedElectronID-Spring15-25ns-V1-standalone-loose"))
      electron->setVariable("cutBasedElectronIDloose",(int)(pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-loose")));
    if(pat_electron.isElectronIDAvailable("cutBasedElectronID-Spring15-25ns-V1-standalone-medium"))
      electron->setVariable("cutBasedElectronIDmedium",(int)(pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-medium")));
    if(pat_electron.isElectronIDAvailable("cutBasedElectronID-Spring15-25ns-V1-standalone-tight"))
      electron->setVariable("cutBasedElectronIDtight",(int)(pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-tight")));
    if(pat_electron.isElectronIDAvailable("cutBasedElectronID-Spring15-25ns-V1-standalone-veto"))
      electron->setVariable("cutBasedElectronIDveto",(int)(pat_electron.electronID("cutBasedElectronID-Spring15-25ns-V1-standalone-veto"))); 
    //
    if(pat_electron.isElectronIDAvailable("mvaEleID-Spring15-25ns-Trig-V1-wp80"))
      electron->setVariable("mvaEleIDTrigwp80",(int)(pat_electron.electronID("mvaEleID-Spring15-25ns-Trig-V1-wp80")));
    if(pat_electron.isElectronIDAvailable("mvaEleID-Spring15-25ns-Trig-V1-wp90"))
      electron->setVariable("mvaEleIDTrigwp80",(int)(pat_electron.electronID("mvaEleID-Spring15-25ns-Trig-V1-wp90")));
    if(pat_electron.isElectronIDAvailable("mvaEleID-Spring15-25ns-nonTrig-V1-wp80"))
      electron->setVariable("mvaEleIDnonTrigwp80",(int)(pat_electron.electronID("mvaEleID-Spring15-25ns-nonTrig-V1-wp80")));
    if(pat_electron.isElectronIDAvailable("mvaEleID-Spring15-25ns-nonTrig-V1-wp90"))
      electron->setVariable("mvaEleIDnonTrigwp90",(int)(pat_electron.electronID("mvaEleID-Spring15-25ns-nonTrig-V1-wp90")));
    //
    if(pat_electron.isElectronIDAvailable("heepElectronID-HEEPV60"))electron->setVariable("eidHEEPV60",(int)(pat_electron.electronID("heepElectronID-HEEPV60")));


    electron->setVariable("ecalEnergy",pat_electron.ecalEnergy());
    electron->setVariable("p_in",pat_electron.trackMomentumAtVtx().R());
    if(pat_electron.ecalEnergy() > 0 && pat_electron.trackMomentumAtVtx().R() > 0)
      electron->setVariable("1oEm1oP",fabs(1/pat_electron.ecalEnergy()-1.0/pat_electron.trackMomentumAtVtx().R()));    

    electrons.push_back(electron);
  }

  sort(electrons.begin(),electrons.end(),SignatureObjectComparison);
  reverse(electrons.begin(),electrons.end());

  m_productmap["ALLELECTRONS"] = electrons;
}

void BaseAODReader::makeMuons()
{
  const reco::Vertex &PV = vertexHandle_->front();
  vector<SignatureObject*> muons;
  for(int i = 0; i < (int)muonHandle_->size(); i++){
    const pat::Muon& pat_muon = muonHandle_->at(i);
    SignatureObject* muon = new SignatureObject(pat_muon.px(),pat_muon.py(),pat_muon.pz(),pat_muon.energy());
    muon->setVariable("INPUTTYPE",TString("muon"));
    muon->setVariable("charge",pat_muon.charge());
    muon->setVariable("trackIso",pat_muon.trackIso());
    muon->setVariable("ecalIso",pat_muon.ecalIso());
    muon->setVariable("hcalIso",pat_muon.hcalIso());
    muon->setVariable("caloIso",pat_muon.caloIso());
    muon->setVariable("numberOfValidHits",(int)pat_muon.numberOfValidHits());
    if(pat_muon.isGlobalMuon()){
      muon->setVariable("normalizedChi2",pat_muon.globalTrack()->normalizedChi2());
      muon->setVariable("numberOfValidMuonHits",pat_muon.globalTrack()->hitPattern().numberOfValidMuonHits());
      muon->setVariable("trackerLayersWithMeasurement",pat_muon.globalTrack()->hitPattern().trackerLayersWithMeasurement());
    }
    if(!pat_muon.isGlobalMuon() && !pat_muon.isTrackerMuon()){
      muon->setVariable("dxy",pat_muon.muonBestTrack()->dxy(PV.position()));
      muon->setVariable("dz",pat_muon.muonBestTrack()->dz(PV.position()));
      muon->setVariable("numberOfValidPixelHits",pat_muon.muonBestTrack()->hitPattern().numberOfValidPixelHits());
      muon->setVariable("validFraction",pat_muon.muonBestTrack()->validFraction());
      muon->setVariable("ptError",pat_muon.muonBestTrack()->ptError());
    }else{
      muon->setVariable("dxy",pat_muon.innerTrack()->dxy(PV.position()));
      muon->setVariable("dz",pat_muon.innerTrack()->dz(PV.position()));
      muon->setVariable("numberOfValidPixelHits",pat_muon.innerTrack()->hitPattern().numberOfValidPixelHits());
      muon->setVariable("validFraction",pat_muon.innerTrack()->validFraction());
      muon->setVariable("ptError",pat_muon.innerTrack()->ptError());
    }
    muon->setVariable("numberOfChambers",pat_muon.numberOfChambers());
    muon->setVariable("isLooseMuon",pat_muon.isLooseMuon());
    muon->setVariable("isMediumMuon",pat_muon.isMediumMuon());
    muon->setVariable("isTightMuon",pat_muon.isTightMuon(PV));
    muon->setVariable("isSoftMuon",pat_muon.isSoftMuon(PV));
    muon->setVariable("isHighPtMuon",pat_muon.isHighPtMuon(PV));
    muon->setVariable("numberOfMatches",pat_muon.numberOfMatches());
    muon->setVariable("numberOfMatchedStations",pat_muon.numberOfMatchedStations());
    muon->setVariable("isGlobalMuon",pat_muon.isGlobalMuon());
    muon->setVariable("isTrackerMuon",pat_muon.isTrackerMuon());
    muon->setVariable("isStandAloneMuon",pat_muon.isStandAloneMuon());
    muon->setVariable("isCaloMuon",pat_muon.isCaloMuon());
    muon->setVariable("isPFMuon",pat_muon.isPFMuon());
    muon->setVariable("isRPCMuon",pat_muon.isRPCMuon());
    muon->setVariable("pfIsolationR03sumChargedHadronPt",pat_muon.pfIsolationR03().sumChargedHadronPt);
    muon->setVariable("pfIsolationR03sumChargedParticlePt",pat_muon.pfIsolationR03().sumChargedParticlePt);
    muon->setVariable("pfIsolationR03sumNeutralHadronEt",pat_muon.pfIsolationR03().sumNeutralHadronEt);
    muon->setVariable("pfIsolationR03sumPhotonEt",pat_muon.pfIsolationR03().sumPhotonEt);
    muon->setVariable("pfIsolationR03sumNeutralHadronEtHighThreshold",pat_muon.pfIsolationR03().sumNeutralHadronEtHighThreshold);
    muon->setVariable("pfIsolationR03sumPUPt",pat_muon.pfIsolationR03().sumPUPt);
    muon->setVariable("pfIsolationR03sumPhotonEtHighThreshold",pat_muon.pfIsolationR03().sumPhotonEtHighThreshold);
    muon->setVariable("pfIsolationR04sumChargedHadronPt",pat_muon.pfIsolationR04().sumChargedHadronPt);
    muon->setVariable("pfIsolationR04sumChargedParticlePt",pat_muon.pfIsolationR04().sumChargedParticlePt);
    muon->setVariable("pfIsolationR04sumNeutralHadronEt",pat_muon.pfIsolationR04().sumNeutralHadronEt);
    muon->setVariable("pfIsolationR04sumPhotonEt",pat_muon.pfIsolationR04().sumPhotonEt);
    muon->setVariable("pfIsolationR04sumNeutralHadronEtHighThreshold",pat_muon.pfIsolationR04().sumNeutralHadronEtHighThreshold);
    muon->setVariable("pfIsolationR04sumPUPt",pat_muon.pfIsolationR04().sumPUPt);
    muon->setVariable("pfIsolationR04sumPhotonEtHighThreshold",pat_muon.pfIsolationR04().sumPhotonEtHighThreshold);
    muon->setVariable("trkKink",pat_muon.combinedQuality().trkKink);
    muon->setVariable("chi2LocalPosition",pat_muon.combinedQuality().chi2LocalPosition);
    muon->setVariable("segmentCompatibility",pat_muon.segmentCompatibility());
    muon->setVariable("dbPV3D",pat_muon.dB(pat::Muon::PV3D));
    muon->setVariable("edbPV3D",pat_muon.edB(pat::Muon::PV3D));
    if (pat_muon.edB(pat::Muon::PV3D) != 0)muon->setVariable("sigPV3D",pat_muon.dB(pat::Muon::PV3D)/pat_muon.edB(pat::Muon::PV3D));
    muon->setVariable("dbPV2D",pat_muon.dB(pat::Muon::PV2D));
    muon->setVariable("edbPV2D",pat_muon.edB(pat::Muon::PV2D));
    if (pat_muon.edB(pat::Muon::PV2D) != 0)muon->setVariable("sigPV2D",pat_muon.dB(pat::Muon::PV2D)/pat_muon.edB(pat::Muon::PV2D));
    muon->setVariable("dbBS2D",pat_muon.dB(pat::Muon::BS2D));
    muon->setVariable("edbBS2D",pat_muon.edB(pat::Muon::BS2D));
    if (pat_muon.edB(pat::Muon::BS2D) != 0)muon->setVariable("sigBS2D",pat_muon.dB(pat::Muon::BS2D)/pat_muon.edB(pat::Muon::BS2D));
    muon->setVariable("dbBS3D",pat_muon.dB(pat::Muon::BS3D));
    muon->setVariable("edbBS3D",pat_muon.edB(pat::Muon::BS3D));
    if (pat_muon.edB(pat::Muon::BS3D) != 0)muon->setVariable("sigBS3D",pat_muon.dB(pat::Muon::BS3D)/pat_muon.edB(pat::Muon::BS3D));

    muons.push_back(muon);
  }

  sort(muons.begin(),muons.end(),SignatureObjectComparison);
  reverse(muons.begin(),muons.end());

  m_productmap["ALLMUONS"] = muons;
}

void BaseAODReader::makePhotons()
{
  vector<SignatureObject*> photons;
  for(int i = 0; i < (int)photonHandle_->size(); i++){
    const pat::Photon& pat_photon = photonHandle_->at(i);
    SignatureObject* photon = new SignatureObject(pat_photon.px(),pat_photon.py(),pat_photon.pz(),pat_photon.energy());
    photon->setVariable("INPUTTYPE",TString("photon"));
    photon->setVariable("chargedHadronIso",pat_photon.chargedHadronIso());
    photon->setVariable("chargedHadronIsoWrongVtx",pat_photon.chargedHadronIsoWrongVtx());
    photon->setVariable("neutralHadronIso",pat_photon.neutralHadronIso());
    photon->setVariable("photonIso",pat_photon.photonIso());
    photon->setVariable("sumPUPt",pat_photon.sumPUPt());
    photon->setVariable("hasConversionTracks",pat_photon.hasConversionTracks());
    photon->setVariable("hasPixelSeed",pat_photon.hasPixelSeed());
    photon->setVariable("hadronicOverEm",pat_photon.hadronicOverEm());
    photon->setVariable("hadronicDepth1OverEm",pat_photon.hadronicDepth1OverEm());
    photon->setVariable("hadronicDepth2OverEm",pat_photon.hadronicDepth2OverEm());
    photon->setVariable("hadTowOverEm",pat_photon.hadTowOverEm());
    photon->setVariable("sigmaEtaEta",pat_photon.sigmaEtaEta());
    photon->setVariable("sigmaIetaIeta",pat_photon.sigmaIetaIeta());
    photon->setVariable("e3x3",pat_photon.e3x3());
    photon->setVariable("e5x5",pat_photon.e5x5());
    photon->setVariable("r9",pat_photon.r9());
    photon->setVariable("superClustereta",pat_photon.superCluster()->eta());
    photon->setVariable("full5x5_sigmaIetaIeta",pat_photon.full5x5_sigmaIetaIeta());
    photon->setVariable("passElectronVeto",pat_photon.passElectronVeto());
    photon->setVariable("hasPixelSeed",pat_photon.hasPixelSeed());

    /*    
    const auto pho = photonHandle_->ptrAt(i);
     if( pho->pt() < 15 ) 
      continue;
    cout<<" "<<i<<" a "<<endl;
    cout<<(*loose_id_decisions).idSize()<<endl;
    cout<<" "<<i<<" b"<<endl;
    bool isPassLoose  = (*loose_id_decisions)[pho];
    bool isPassMedium = (*medium_id_decisions)[pho];
    cout<<" "<<i<<" c"<<endl;
    bool isPassTight  = (*tight_id_decisions)[pho];
    cout<<" "<<i<<" d"<<endl;
    float ChargedIsoValue = (*phoChargedIsolationHandle_)[pho];
    float NeutralHadronIsoValue = (*phoNeutralHadronIsolationHandle_)[pho];
    float PhotonIsoValue = (*phoPhotonIsolationHandle_)[pho];
    float WorstChargedIsoValue = (*phoWorstChargedIsolationHandle_)[pho];
    photon->setVariable("passLooseId", isPassLoose);
    photon->setVariable("passMediumId", isPassMedium);
    photon->setVariable("passTightId", isPassTight);
    photon->setVariable("ChargedIsolation", ChargedIsoValue);
    photon->setVariable("NeutralHadronIsolation", NeutralHadronIsoValue);
    photon->setVariable("PhotonIsolation", PhotonIsoValue);
    photon->setVariable("WorstChargedIsolation", WorstChargedIsoValue);
    */
    photons.push_back(photon);
  }

  sort(photons.begin(),photons.end(),SignatureObjectComparison);
  reverse(photons.begin(),photons.end());

  m_productmap["ALLPHOTONS"] = photons;
}

void BaseAODReader::makeTaus()
{
  vector<SignatureObject*> taus;
  for(int i = 0; i < (int)tauHandle_->size(); i++){
    const pat::Tau& pat_tau = tauHandle_->at(i);
    if(!pat_tau.isPFTau())continue;
    SignatureObject* tau = new SignatureObject(pat_tau.px(),pat_tau.py(),pat_tau.pz(),pat_tau.energy());
    tau->setVariable("INPUTTYPE",TString("tau"));
    tau->setVariable("charge",pat_tau.charge());
    tau->setVariable("isPFTau",pat_tau.isPFTau());
    tau->setVariable("leadPFChargedHadrCandsignedSipt",pat_tau.leadPFChargedHadrCandsignedSipt());
    tau->setVariable("isolationPFChargedHadrCandsPtSum",pat_tau.isolationPFChargedHadrCandsPtSum());
    tau->setVariable("isolationPFGammaCandsEtSum",pat_tau.isolationPFGammaCandsEtSum());
    tau->setVariable("maximumHCALPFClusterEt",pat_tau.maximumHCALPFClusterEt());
    tau->setVariable("dxy",pat_tau.dxy());
    tau->setVariable("dxy_Sig",pat_tau.dxy_Sig());
    tau->setVariable("leadPFCandpt",pat_tau.leadPFCand()->pt());
    tau->setVariable("leadPFCandpdgId",pat_tau.leadPFCand()->pdgId());
    tau->setVariable("byLooseCombinedIsolationDeltaBetaCorr3Hits",pat_tau.tauID("byLooseCombinedIsolationDeltaBetaCorr3Hits"));
    tau->setVariable("byMediumCombinedIsolationDeltaBetaCorr3Hits",pat_tau.tauID("byMediumCombinedIsolationDeltaBetaCorr3Hits"));
    tau->setVariable("byTightCombinedIsolationDeltaBetaCorr3Hits",pat_tau.tauID("byTightCombinedIsolationDeltaBetaCorr3Hits"));
    tau->setVariable("againstElectronLooseMVA3",pat_tau.tauID("againstElectronLooseMVA3"));
    tau->setVariable("againstElectronMediumMVA3",pat_tau.tauID("againstElectronMediumMVA3"));
    tau->setVariable("againstElectronTightMVA3",pat_tau.tauID("againstElectronTightMVA3"));
    tau->setVariable("againstElectronVTightMVA3",pat_tau.tauID("againstElectronVTightMVA3"));
    tau->setVariable("againstMuonLoose3",pat_tau.tauID("againstMuonLoose3"));
    tau->setVariable("againstMuonMedium3",pat_tau.tauID("againstMuonMedium3"));
    tau->setVariable("againstMuonTight3",pat_tau.tauID("againstMuonTight3"));
    taus.push_back(tau);
  }

  sort(taus.begin(),taus.end(),SignatureObjectComparison);
  reverse(taus.begin(),taus.end());

  m_productmap["ALLTAUS"] = taus;
}

void BaseAODReader::makeTracks()
{
  vector<SignatureObject*> tracks;
  for(int i = 0; i < (int)trackHandle_->size(); i++){
    const reco::Track& pat_track = trackHandle_->at(i);
    SignatureObject* track = new SignatureObject(pat_track.px(),pat_track.py(),pat_track.pz(),pat_track.p());
    track->setVariable("WRITEOBJECT",true);
    tracks.push_back(track);
  }

  sort(tracks.begin(),tracks.end(),SignatureObjectComparison);
  reverse(tracks.begin(),tracks.end());

  m_productmap["ALLTRACKS"] = tracks;

}

void BaseAODReader::makeVertices()
{
  vector<SignatureObject*> vertices;
  for(int i = 0; i < (int)vertexHandle_->size(); i++){
    const reco::Vertex& pat_vertex = vertexHandle_->at(i);
    SignatureObject* vertex = new SignatureObject(0,0,0,0);
    vertex->setVariable("VX",pat_vertex.x());
    vertex->setVariable("VY",pat_vertex.y());
    vertex->setVariable("VZ",pat_vertex.z());
    vertex->setVariable("WRITEOBJECT",true);
    vertices.push_back(vertex);
  }

  m_productmap["ALLVERTICES"] = vertices;
}

void BaseAODReader::makeBeamSpot()
{
  vector<SignatureObject*> beamspots;
  const reco::BeamSpot& pat_beamspot = (*beamspotHandle_);
  SignatureObject* beamspot = new SignatureObject(0,0,0,0);
  beamspot->setVariable("X0",pat_beamspot.x0());
  beamspot->setVariable("Y0",pat_beamspot.y0());
  beamspot->setVariable("Z0",pat_beamspot.z0());
  beamspot->setVariable("WRITEOBJECT",true);
  beamspots.push_back(beamspot);

  m_productmap["ALLBEAMSPOTS"] = beamspots;

}

void BaseAODReader::makeTriggers(const edm::Event& event, const edm::EventSetup& eventsetup)
{
  //cout<<"WARNING:  Trigger prescales are not correctly computed by BaseAODReader::makeTriggers, all are set to -1."<<endl;
  vector<SignatureObject*> triggers;
  for(int i = 0; i < (int)hltConfig_.size(); i++){
    string trigName = hltConfig_.triggerName(i);
    //const pair<int,int> prescales = hltPrescale_.prescaleValues(event,eventsetup,trigName);//outdated for CMSSW 76X
    int prescale = hltPrescale_.prescaleValue(event,eventsetup,trigName);
    const unsigned int triggerIndex = hltConfig_.triggerIndex(trigName);
    SignatureObject* trigger = new SignatureObject(0,0,0,0);
    trigger->setVariable("INPUTTYPE",TString("trigger"));
    trigger->setVariable("TRIGGERNAME",TString(trigName));
    //trigger->setVariable("L1PRESCALE",prescales.first);
    //trigger->setVariable("HLTPRESCALE",prescales.second);
    //trigger->setVariable("PRESCALE",prescales.first > 0 ? prescales.first*prescales.second : prescales.second);
    trigger->setVariable("PRESCALE",prescale);
    //trigger->setVariable("PRESCALE",-1);
    //trigger->setVariable("HLTPRESCALE",-1);
    //trigger->setVariable("PRESCALE",-1);
    trigger->setVariable("WASRUN",triggerHandle_->wasrun(triggerIndex));
    trigger->setVariable("ACCEPT",triggerHandle_->accept(triggerIndex));
    trigger->setVariable("ERROR",triggerHandle_->accept(triggerIndex));
    trigger->setVariable("WRITEOBJECT",true);
    triggers.push_back(trigger);
  }

  m_productmap["ALLTRIGGERS"] = triggers;
}

void BaseAODReader::makeGenParticles()
{
  vector<SignatureObject*> genParticles;
  for(int i = 0; i < (int)genParticleHandle_->size(); i++){
    const reco::GenParticle& reco_genpart = genParticleHandle_->at(i);
    SignatureObject* part = new SignatureObject(reco_genpart.px(),reco_genpart.py(),reco_genpart.pz(),reco_genpart.energy());
    part->setVariable("INPUTTYPE",TString("mc"));
    part->setVariable("collisionId",reco_genpart.collisionId());
    part->setVariable("charge",reco_genpart.charge());
    part->setVariable("vx",reco_genpart.vx());
    part->setVariable("vy",reco_genpart.vy());
    part->setVariable("vz",reco_genpart.vz());
    part->setVariable("pdgId",reco_genpart.pdgId());
    part->setVariable("status",reco_genpart.status());
    part->setVariable("numberOfMothers",(int)reco_genpart.numberOfMothers());
    if(reco_genpart.numberOfMothers() > 0){
      part->setVariable("motherpdgId",reco_genpart.mother(0)->pdgId());
      part->setVariable("motherstatus",reco_genpart.mother(0)->status());
    }
    genParticles.push_back(part);
  }
  m_productmap["ALLMC"] = genParticles;
}

void BaseAODReader::makeMET()
{
  vector<SignatureObject*> mets;
  for(int i = 0; i < (int)metHandle_->size(); i++){
    const pat::MET& pat_met = metHandle_->at(i);
    SignatureObject* met = new SignatureObject(pat_met.px(),pat_met.py(),0,pat_met.pt());
    met->setVariable("WRITEOBJECT",true);
    mets.push_back(met);
  }
  m_productmap["ALLMET"] = mets;
}


void BaseAODReader::processProduct(ParameterSet pset)
{
  TString type = pset.getUntrackedParameter<string>("type");
  if(!type.Contains("Product"))return;

  TString productName = pset.getUntrackedParameter<string>("productName");
  TString sourceName = pset.getUntrackedParameter<string>("sourceName");

  vector<string> cuts = pset.getParameter<vector<string> >("cutList");

  addProduct(productName,sourceName);

  for(int i = 0; i < (int)cuts.size(); i++){
    addProductCut(productName,cuts[i]);
  }

}


void BaseAODReader::processSignatureTH(ParameterSet pset)
{
  TString type = pset.getUntrackedParameter<string>("type");
  if(!type.Contains("SignatureTH"))return;

  int nBinsX = 1;
  int nBinsY = 0;
  //int nBinsZ = 0;
  double minX = 0;
  double minY = 0;
  //double minZ = 0;
  double maxX = 1;
  double maxY = 0;
  //double maxZ = 0;

  if(pset.exists("nBinsX"))nBinsX = pset.getParameter<int>("nBinsX");
  if(pset.exists("minX")) minX = pset.getParameter<double>("minX");
  if(pset.exists("maxX")) maxX = pset.getParameter<double>("maxX");

  if(pset.exists("nBinsY"))nBinsY = pset.getParameter<int>("nBinsY");
  if(pset.exists("minY")) minY = pset.getParameter<double>("minY");
  if(pset.exists("maxY")) maxY = pset.getParameter<double>("maxY");
  /*
  if(pset.exists("nBinsZ"))nBinsZ = pset.getParameter<int>("nBinsZ");
  if(pset.exists("minZ")) minZ = pset.getParameter<double>("minZ");
  if(pset.exists("maxZ")) maxZ = pset.getParameter<double>("maxZ");
  */
  string name = pset.getParameter<string>("name");
  string regexp = "";
  if(pset.exists("regexp"))regexp = pset.getParameter<string>("regexp");
  string title = "";
  if(pset.exists("title"))title = pset.getParameter<string>("title");

  if(type == "SignatureTH1F_CountNoWeight"){
    addHistogram(new SignatureTH1F_CountNoWeight(name.c_str()),regexp);
  }else if(type == "SignatureTH1F_CountWeight"){
    addHistogram(new SignatureTH1F_CountWeight(name.c_str()),regexp);
  }else if(type == "SignatureTH1F_EventVariable"){
    string varname=pset.getParameter<string>("variableName");
    string vartype=pset.getParameter<string>("variableType");
    if(vartype == "double"){
      addHistogram(new SignatureTH1F_EventVariable<double>(name.c_str(),varname,title.c_str(),nBinsX,minX,maxX),regexp);
    }else if(vartype == "int"){
      addHistogram(new SignatureTH1F_EventVariable<int>(name.c_str(),varname,title.c_str(),nBinsX,minX,maxX),regexp);
    }else{

    }
  }else if(type == "SignatureTH1F_N"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    if(productnames.size() == 0)return;
    SignatureTH1F_N* h = new SignatureTH1F_N(name.c_str(),productnames[0].c_str(),title.c_str(),nBinsX,minX,maxX);
    if(productnames.size() > 1){
      for(int i = 1; i < (int)productnames.size(); i++){
	h->addProduct(productnames[i]);
      }
    }
    addHistogram(h,regexp);
  }else if(type == "SignatureTH1F_ObjectVariable"){
    string varname=pset.getParameter<string>("variableName");
    string vartype=pset.getParameter<string>("variableType");
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    if(productnames.size() == 0)return;

    if(vartype == "double"){
      SignatureTH1F_ObjectVariable<double>* h = new SignatureTH1F_ObjectVariable<double>(name.c_str(),varname,productnames[0],title.c_str(),nBinsX,minX,maxX);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  h->addProduct(productnames[i]);
	}
      }
      addHistogram(h,regexp);
    }else if(vartype == "int"){
      SignatureTH1F_ObjectVariable<int>* h = new SignatureTH1F_ObjectVariable<int>(name.c_str(),varname,productnames[0],title.c_str(),nBinsX,minX,maxX);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  h->addProduct(productnames[i]);
	}
      }
      addHistogram(h,regexp);
    }else{

    }

  }else if(type == "SignatureTH2F_EventVariableVsEventVariable"){
    string varnameX=pset.getParameter<string>("variableNameX");
    string vartypeX=pset.getParameter<string>("variableTypeX");
    string varnameY=pset.getParameter<string>("variableNameY");
    string vartypeY=pset.getParameter<string>("variableTypeY");
    if(vartypeX == "double" && vartypeX == "double"){
      addHistogram(new SignatureTH2F_EventVariableVsEventVariable<double,double>(varnameX,varnameY,name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY),regexp);
    }else if(vartypeX == "double" && vartypeY == "int"){
      addHistogram(new SignatureTH2F_EventVariableVsEventVariable<double,int>(varnameX,varnameY,name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY),regexp);
    }else if(vartypeX == "int" && vartypeY == "double"){
      addHistogram(new SignatureTH2F_EventVariableVsEventVariable<int,double>(varnameX,varnameY,name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY),regexp);
    }else if(vartypeX == "int" && vartypeY == "int"){
      addHistogram(new SignatureTH2F_EventVariableVsEventVariable<int,int>(varnameX,varnameY,name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY),regexp);
    }else{

    }
  }else if(type == "SignatureTH2F_EventVariableVsObjectVariable"){
    string varnameX=pset.getParameter<string>("variableNameX");
    string vartypeX=pset.getParameter<string>("variableTypeX");
    string varnameY=pset.getParameter<string>("variableNameY");
    string vartypeY=pset.getParameter<string>("variableTypeY");
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    if(productnames.size() == 0)return;

    if(vartypeX == "double" && vartypeX == "double"){
      SignatureTH2F_EventVariableVsObjectVariable<double,double>* h = new SignatureTH2F_EventVariableVsObjectVariable<double,double>(varnameX,varnameY,productnames[0],name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  h->addProduct(productnames[i]);
	}
      }
    addHistogram(h,regexp);
    }else if(vartypeX == "double" && vartypeY == "int"){
       SignatureTH2F_EventVariableVsObjectVariable<double,int>* h = new SignatureTH2F_EventVariableVsObjectVariable<double,int>(varnameX,varnameY,productnames[0],name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY);
       if(productnames.size() > 1){
	 for(int i = 1; i < (int)productnames.size(); i++){
	   h->addProduct(productnames[i]);
	 }
       }
       addHistogram(h,regexp);
    }else if(vartypeX == "int" && vartypeY == "double"){
       SignatureTH2F_EventVariableVsObjectVariable<int,double>* h = new SignatureTH2F_EventVariableVsObjectVariable<int,double>(varnameX,varnameY,productnames[0],name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY);
       if(productnames.size() > 1){
	 for(int i = 1; i < (int)productnames.size(); i++){
	   h->addProduct(productnames[i]);
	 }
       }
       addHistogram(h,regexp);
    }else if(vartypeX == "int" && vartypeY == "int"){
      SignatureTH2F_EventVariableVsObjectVariable<int,int>* h = new SignatureTH2F_EventVariableVsObjectVariable<int,int>(varnameX,varnameY,productnames[0],name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  h->addProduct(productnames[i]);
	}
      }
      addHistogram(h,regexp);
    }else{
      
    }


  }else if(type == "SignatureTH2F_ObjectVariableVsObjectVariable"){
    string varnameX=pset.getParameter<string>("variableNameX");
    string vartypeX=pset.getParameter<string>("variableTypeX");
    string varnameY=pset.getParameter<string>("variableNameY");
    string vartypeY=pset.getParameter<string>("variableTypeY");
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    if(productnames.size() == 0)return;

    if(vartypeX == "double" && vartypeX == "double"){
      SignatureTH2F_ObjectVariableVsObjectVariable<double,double>* h = new SignatureTH2F_ObjectVariableVsObjectVariable<double,double>(varnameX,varnameY,productnames[0],name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  h->addProduct(productnames[i]);
	}
      }
      addHistogram(h,regexp);
    }else if(vartypeX == "double" && vartypeY == "int"){
      SignatureTH2F_ObjectVariableVsObjectVariable<double,int>* h = new SignatureTH2F_ObjectVariableVsObjectVariable<double,int>(varnameX,varnameY,productnames[0],name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  h->addProduct(productnames[i]);
	}
      }
      addHistogram(h,regexp);
    }else if(vartypeX == "int" && vartypeY == "double"){
      SignatureTH2F_ObjectVariableVsObjectVariable<int,double>* h = new SignatureTH2F_ObjectVariableVsObjectVariable<int,double>(varnameX,varnameY,productnames[0],name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  h->addProduct(productnames[i]);
	}
      }
      addHistogram(h,regexp);
    }else if(vartypeX == "int" && vartypeY == "int"){
      SignatureTH2F_ObjectVariableVsObjectVariable<int,int>* h = new SignatureTH2F_ObjectVariableVsObjectVariable<int,int>(varnameX,varnameY,productnames[0],name.c_str(),title.c_str(),nBinsX,minX,maxX,nBinsY,minY,maxY);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  h->addProduct(productnames[i]);
	}
      }
      addHistogram(h,regexp);
    }else{
      
    }

  }else{
      cerr << "---Don't know what to do with type: "<<type<<endl;
      cerr << "---Contact developers or add to processing code "<<endl;
  }

}

void BaseAODReader::processEventVariable(ParameterSet pset)
{
  TString type = pset.getUntrackedParameter<string>("type");
  if(!type.Contains("EventVariable"))return;
  string name = pset.getParameter<string>("name");

  if(type == "EventVariableCombined"){
    bool doAnd = false;
    if(pset.exists("doAnd"))doAnd = pset.getParameter<bool>("doAnd");
    vector<string> cutList = pset.getParameter<vector<string> >("cutList");
    if(cutList.size() < 2)return;
    EventVariableCombined* var = new EventVariableCombined(cutList[0],cutList[1],doAnd,name.c_str());
    if(cutList.size() > 2){
      for(int i = 2; i < (int)cutList.size(); i++){
	var->addVariable(cutList[i]);
      }
    }
    addEventVariable(name,var);
  }else if(type == "EventVariableInRange"){
    string varType = pset.getParameter<string>("variableType");
    string varName = pset.getParameter<string>("variableName");
    if(varType == "int"){
      int low = pset.getParameter<int>("low");
      int high = pset.getParameter<int>("high");
      EventVariableInRange<int>* var = new EventVariableInRange<int>(varName,low,high,name);
      addEventVariable(name,var);
    }else if(varType == "double"){
      double low = pset.getParameter<double>("low");
      double high = pset.getParameter<double>("high");
      EventVariableInRange<double>* var = new EventVariableInRange<double>(varName,low,high,name);
      addEventVariable(name,var);
    }else{

    }
  }else if(type == "EventVariableMT"){
    string prefix = "";
    double mZ = pset.getParameter<double>("mZ");
    string productEl = "goodElectrons";
    string productMu = "goodMuons";
    string productTau = "goodTaus";
    string productMET = "MET";
    if(pset.exists("electronProduct"))productEl = pset.getParameter<string>("electronProduct");
    if(pset.exists("muonProduct"))productMu = pset.getParameter<string>("muonProduct");
    if(pset.exists("tauProduct"))productTau = pset.getParameter<string>("tauProduct");
    if(pset.exists("metProduct"))productMET = pset.getParameter<string>("metProduct");
    if(pset.exists("prefix"))prefix = pset.getParameter<string>("prefix");

    EventVariableMT* var = new EventVariableMT(name,mZ,productEl,productMu,productTau,productMET);
    addEventVariable(name,var);
  }else if(type == "EventVariableMass"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    if(productnames.size() == 0)return;
    EventVariableMass* var = new EventVariableMass(name,productnames[0]);
    if(productnames.size() > 1){
      for(int i = 1; i < (int)productnames.size(); i++){
	var->addProduct(productnames[i]);
      }
    }
    addEventVariable(name,var);
  }else if(type == "EventVariableN"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    if(productnames.size() == 0)return;
    EventVariableN* var = new EventVariableN(name,productnames[0]);
    if(productnames.size() > 1){
      for(int i = 1; i < (int)productnames.size(); i++){
	var->addProduct(productnames[i]);
      }
    }
    addEventVariable(name,var);
  }else if(type == "EventVariableOS"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    string prefix = "";
    if(pset.exists("prefix"))prefix = pset.getParameter<string>("prefix");
    if(productnames.size() == 0)return;
    EventVariableOS* var = new EventVariableOS(name,productnames[0],prefix);
    if(productnames.size() > 1){
      for(int i = 1; i < (int)productnames.size(); i++){
	var->addProduct(productnames[i]);
      }
    }
    addEventVariable(name,var);
  }else if(type == "EventVariableOSSF"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    double zmass = 91;
    double width = 15;
    if(pset.exists("mZ"))zmass = pset.getParameter<double>("mZ");
    if(pset.exists("width"))width = pset.getParameter<double>("width");
    string prefix = "";
    if(pset.exists("prefix"))prefix = pset.getParameter<string>("prefix");
    if(productnames.size() == 0)return;
    EventVariableOSSF* var = new EventVariableOSSF(name,productnames[0],prefix,zmass,width);
    if(productnames.size() > 1){
      for(int i = 1; i < (int)productnames.size(); i++){
	var->addProduct(productnames[i]);
      }
    }
    addEventVariable(name,var);
  }else if(type == "EventVariableObjectVariableExtreme"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    string varname = pset.getParameter<string>("variableName");
    string vartype = pset.getParameter<string>("variableType");
    if(productnames.size() == 0)return;
    if(vartype == "int"){
      EventVariableObjectVariableExtreme<int>* var = new EventVariableObjectVariableExtreme<int>(varname,productnames[0],name);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  var->addProduct(productnames[i]);
	}
      }
    }else if(vartype == "double"){
      EventVariableObjectVariableExtreme<double>* var = new EventVariableObjectVariableExtreme<double>(varname,productnames[0],name);
      if(productnames.size() > 1){
	for(int i = 1; i < (int)productnames.size(); i++){
	  var->addProduct(productnames[i]);
	}
      }
      addEventVariable(name,var);
    }
  }else if(type == "EventVariablePairMass"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    double peak = 0;
    double width = 10;
    if(pset.exists("peak"))peak = pset.getParameter<double>("peak");
    if(pset.exists("width"))width = pset.getParameter<double>("width");
    string prefix = "";
    if(pset.exists("prefix"))prefix = pset.getParameter<string>("prefix");
    if(productnames.size() == 0)return;
    EventVariablePairMass* var = new EventVariablePairMass(name,productnames[0],prefix,peak,width);
    if(productnames.size() > 1){
      for(int i = 1; i < (int)productnames.size(); i++){
	var->addProduct(productnames[i]);
      }
    }
    addEventVariable(name,var);
  }else if(type == "EventVariableReversed"){
    string cutname = pset.getParameter<string>("cutName");
    addEventVariable(name,new EventVariableReversed(cutname,name.c_str()));
  }else if(type == "EventVariableSmearMET"){
    int seed = 123456;
    if(pset.exists("seed"))seed = pset.getParameter<int>("seed");
    string productname = pset.getParameter<string>("productName");
    string htname = "HT";
    string nvname = "NRECOVERTICES";
    if(pset.exists("HTvarname"))htname = pset.getParameter<string>("HTvarname");
    if(pset.exists("NVvarname"))nvname = pset.getParameter<string>("NVvarname");
    double mc_sigma0 = 2.68;
    if(pset.exists("mc_sigma0"))mc_sigma0 = pset.getParameter<double>("mc_sigma0");
    double mc_sigmaHT = 4.14;
    if(pset.exists("mc_sigmaHT"))mc_sigmaHT = pset.getParameter<double>("mc_sigmaHT");
    double mc_sigmaNV = 3.48;
    if(pset.exists("mc_sigmaNV"))mc_sigmaNV = pset.getParameter<double>("mc_sigmaNV");
    double data_sigma0 = 2.68;
    if(pset.exists("data_sigma0"))data_sigma0 = pset.getParameter<double>("data_sigma0");
    double data_sigmaHT = 5.10;
    if(pset.exists("data_sigmaHT"))data_sigmaHT = pset.getParameter<double>("data_sigmaHT");
    double data_sigmaNV = 3.48;
    if(pset.exists("data_sigmaNV"))data_sigmaNV = pset.getParameter<double>("data_sigmaNV");
    EventVariableSmearMET* var = new EventVariableSmearMET(name,productname,htname,nvname,mc_sigma0,mc_sigmaHT,mc_sigmaNV,data_sigma0,data_sigmaHT,data_sigmaNV);
    var->setSeed(seed);
    addEventVariable(name,var);
  }else if(type == "EventVariableSumPT"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    if(productnames.size() == 0)return;
    EventVariableSumPT* var = new EventVariableSumPT(name,productnames[0]);
    if(productnames.size() > 1){
      for(int i = 1; i < (int)productnames.size(); i++){
	var->addProduct(productnames[i]);
      }
    }
    addEventVariable(name,var);
  }else if(type == "EventVariableThreshold"){
    vector<string> productnames = pset.getParameter<vector<string> >("productNames");
    if(productnames.size() == 0)return;
    vector<double> thresholds = pset.getParameter<vector<double> >("thresholds");
    EventVariableThreshold* var = new EventVariableThreshold(name,productnames[0]);
    if(productnames.size() > 1){
      for(int i = 1; i < (int)productnames.size(); i++){
	var->addProduct(productnames[i]);
      }
    }
    for(int i = 0; i < (int)thresholds.size(); i++){
      var->addThreshold(thresholds[i]);
    }
    addEventVariable(name,var);
  }else if(type == "EventVariableTriggerWeight"){
    addEventVariable(name,new EventVariableTriggerWeight(name));
  }else if(type == "EventVariableValue"){
    string varname = pset.getParameter<string>("variableName");
    string vartype = pset.getParameter<string>("variableType");
    if(vartype == "double"){
      double value = pset.getParameter<double>("value");
      EventVariableValue<double>* var = new EventVariableValue<double>(varname,value,name);
      addEventVariable(name,var);
    }else if(vartype=="int"){
      int value = pset.getParameter<int>("value");
      EventVariableValue<int>* var = new EventVariableValue<int>(varname,value,name);
      addEventVariable(name,var);
    }else if(vartype=="long"){
      return;
    }else if(vartype=="TString"){
      string value = pset.getParameter<string>("value");
      EventVariableValue<TString>* var = new EventVariableValue<TString>(varname,value,name);
      addEventVariable(name,var);
    }else if(vartype=="bool"){
      bool value = pset.getParameter<bool>("value");
      EventVariableValue<bool>* var = new EventVariableValue<bool>(varname,value,name);
      addEventVariable(name,var);
    }else{

    }

  }else if(type == "EventVariableValueInList"){
    string varname = pset.getParameter<string>("variableName");
    string vartype = pset.getParameter<string>("variableType");
    if(vartype == "double"){
      vector<double> values = pset.getParameter<vector<double> >("values");
      if(values.size() == 0)return;
      EventVariableValueInList<double>* var = new EventVariableValueInList<double>(varname,values[0],name);
      for(int i = 1; i < (int)values.size(); i++){
	var->addValue(values[i]);
      }
      addEventVariable(name,var);
    }else if(vartype=="int"){
      vector<int> values = pset.getParameter<vector<int> >("values");
      if(values.size() == 0)return;
      EventVariableValueInList<int>* var = new EventVariableValueInList<int>(varname,values[0],name);
      for(int i = 1; i < (int)values.size(); i++){
	var->addValue(values[i]);
      }
      addEventVariable(name,var);
    }else if(vartype=="long"){
      return;
    }else if(vartype=="TString"){
      vector<string> values = pset.getParameter<vector<string> >("values");
      if(values.size() == 0)return;
      EventVariableValueInList<TString>* var = new EventVariableValueInList<TString>(varname,values[0],name);
      for(int i = 1; i < (int)values.size(); i++){
	var->addValue(values[i]);
      }
      addEventVariable(name,var);
    }else if(vartype=="bool"){
      return;
    }else{

    }

  }else{

  }

}

void BaseAODReader::processObjectVariable(ParameterSet pset)
{
  TString type = pset.getUntrackedParameter<string>("type");
  if(!type.Contains("ObjectVariable"))return;
  string name = pset.getParameter<string>("name");

  if(type == "ObjectVariableCombined"){
    bool doAnd = false;
    if(pset.exists("doAnd"))doAnd = pset.getParameter<bool>("doAnd");
    vector<string> cutList = pset.getParameter<vector<string> >("cutList");
    if(cutList.size() < 2)return;
    ObjectVariableCombined* var = new ObjectVariableCombined(cutList[0],cutList[1],doAnd,name.c_str());
    if(cutList.size() > 2){
      for(int i = 2; i < (int)cutList.size(); i++){
	var->addVariable(cutList[i]);
      }
    }
    addObjectVariable(name,var);
  }else if(type == "ObjectVariableElectronTotalIso"){
    addObjectVariable(name,new ObjectVariableElectronTotalIso(name));
  }else if(type == "ObjectVariableInRange"){
    string varType = pset.getParameter<string>("variableType");
    string varName = pset.getParameter<string>("variableName");
    if(varType == "int"){
      int low = pset.getParameter<int>("low");
      int high = pset.getParameter<int>("high");
      ObjectVariableInRange<int>* var = new ObjectVariableInRange<int>(varName,low,high,name);
      addObjectVariable(name,var);
    }else if(varType == "double"){
      double low = pset.getParameter<double>("low");
      double high = pset.getParameter<double>("high");
      ObjectVariableInRange<double>* var = new ObjectVariableInRange<double>(varName,low,high,name);
      addObjectVariable(name,var);
    }else{

    }
  }else if(type == "ObjectVariableMuonTotalIso"){
    addObjectVariable(name, new ObjectVariableMuonTotalIso(name));
  }else if(type == "ObjectVariableRelIso"){
    string varName = "TOTALISO";
    if(pset.exists("variableName"))pset.getParameter<string>("variableName");
    addObjectVariable(name, new ObjectVariableRelIso(name,varName));
  }else if(type == "ObjectVariableReversed"){
    string cutname = pset.getParameter<string>("cutName");
    addObjectVariable(name,new ObjectVariableReversed(cutname,name.c_str()));
  }else if(type == "ObjectVariableTauTotalIso"){
    addObjectVariable(name,new ObjectVariableTauTotalIso(name));
  }else if(type == "ObjectVariableValue"){
    string varname = pset.getParameter<string>("variableName");
    string vartype = pset.getParameter<string>("variableType");
    if(vartype == "double"){
      double value = pset.getParameter<double>("value");
      ObjectVariableValue<double>* var = new ObjectVariableValue<double>(varname,value,name);
      addObjectVariable(name,var);
    }else if(vartype=="int"){
      int value = pset.getParameter<int>("value");
      ObjectVariableValue<int>* var = new ObjectVariableValue<int>(varname,value,name);
      addObjectVariable(name,var);
    }else if(vartype=="long"){
      return;
    }else if(vartype=="TString"){
      string value = pset.getParameter<string>("value");
      ObjectVariableValue<TString>* var = new ObjectVariableValue<TString>(varname,value,name);
      addObjectVariable(name,var);
    }else if(vartype=="bool"){
      bool value = pset.getParameter<bool>("value");
      ObjectVariableValue<bool>* var = new ObjectVariableValue<bool>(varname,value,name);
      addObjectVariable(name,var);
    }else{

    }

  }else if(type == "ObjectVariableValueInList"){
    string varname = pset.getParameter<string>("variableName");
    string vartype = pset.getParameter<string>("variableType");
    if(vartype == "double"){
      vector<double> values = pset.getParameter<vector<double> >("values");
      if(values.size() == 0)return;
      ObjectVariableValueInList<double>* var = new ObjectVariableValueInList<double>(varname,values[0],name);
      for(int i = 1; i < (int)values.size(); i++){
	var->addValue(values[i]);
      }
      addObjectVariable(name,var);
    }else if(vartype=="int"){
      vector<int> values = pset.getParameter<vector<int> >("values");
      if(values.size() == 0)return;
      ObjectVariableValueInList<int>* var = new ObjectVariableValueInList<int>(varname,values[0],name);
      for(int i = 1; i < (int)values.size(); i++){
	var->addValue(values[i]);
      }
      addObjectVariable(name,var);
    }else if(vartype=="long"){
      return;
    }else if(vartype=="TString"){
      vector<string> values = pset.getParameter<vector<string> >("values");
      if(values.size() == 0)return;
      ObjectVariableValueInList<TString>* var = new ObjectVariableValueInList<TString>(varname,values[0],name);
      for(int i = 1; i < (int)values.size(); i++){
	var->addValue(values[i]);
      }
      addObjectVariable(name,var);
    }else if(vartype=="bool"){
      return;
    }else{

    }
  }else if(type == "ObjectVariableMethod"){
    string methodname = pset.getParameter<string>("methodName");
    if(methodname == "Pt"){
      addObjectVariable(name, new ObjectVariableMethod(name, &SignatureObject::Pt));
    }else if(methodname == "Eta"){
      addObjectVariable(name, new ObjectVariableMethod(name, &SignatureObject::Eta));
    }else if(methodname == "Phi"){
      addObjectVariable(name, new ObjectVariableMethod(name, &SignatureObject::Phi));
    }else{

    }
  }else{

  }
}

void BaseAODReader::processObjectComparison(ParameterSet pset)
{
  TString type = pset.getUntrackedParameter<string>("type");
  if(!type.Contains("ObjectComparison"))return;
  string name = "ObjectComparison";
  if(pset.exists("name"))name = pset.getParameter<string>("name");
  bool doAnd = true;
  if(pset.exists("doAnd"))doAnd = pset.getParameter<bool>("doAnd");
  string product1 = pset.getParameter<string>("product1");
  string product2 = "";
  if(pset.exists("product2"))product2 = pset.getParameter<string>("product2");


  if(type == "ObjectComparisonDeltaR"){
    double deltaR = pset.getParameter<double>("deltaR");
    if(product2 == ""){
      addProductSelfComparison(product1,new ObjectComparisonDeltaR(deltaR,name),doAnd);
    }else{
      addProductComparison(product1,product2,new ObjectComparisonDeltaR(deltaR,name),doAnd);
    }
  }else if(type == "ObjectComparisonElectron"){
    double deltaR = pset.getParameter<double>("deltaR");
    if(product2 == ""){
      addProductSelfComparison(product1,new ObjectComparisonElectron(deltaR,name),doAnd);
    }else{
      addProductComparison(product1,product2,new ObjectComparisonElectron(deltaR,name),doAnd);
    }
  }else if(type == "ObjectComparisonSkimRecoTracks"){
    double deltaR = pset.getParameter<double>("deltaR");
    if(product2 == ""){
      addProductSelfComparison(product1,new ObjectComparisonSkimRecoTracks(deltaR,name),doAnd);
    }else{
      addProductComparison(product1,product2,new ObjectComparisonSkimRecoTracks(deltaR,name),doAnd);
    }
  }else if(type == "ObjectComparisonMatchDeltaRCharge"){
    double deltaR = pset.getParameter<double>("deltaR");
    if(product2 == ""){
      addProductSelfComparison(product1,new ObjectComparisonMatchDeltaRCharge(deltaR,name),doAnd);
    }else{
      addProductComparison(product1,product2,new ObjectComparisonMatchDeltaRCharge(deltaR,name),doAnd);
    }

  }else{

  }

}

void BaseAODReader::processSignature(ParameterSet pset)
{
  TString type = pset.getUntrackedParameter<string>("type");
  if(type != "Signature")return;

  string name = pset.getParameter<string>("name");
  vector<string> cutList = pset.getParameter<vector<string> >("cutList");

  Signature* sig = new Signature(name);
  for(int i = 0; i < (int)cutList.size(); i++)sig->addCut(cutList[i]);

  addSignature(sig);

}

DEFINE_FWK_MODULE(BaseAODReader);
